package com.gitlab.bogdan776.springstore.usecase.auth;

import com.gitlab.bogdan776.springstore.common.constant.enums.Country;
import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.dto.request.SignUpRequest;
import com.gitlab.bogdan776.springstore.dto.response.UserProfileResponse;
import com.gitlab.bogdan776.springstore.entity.user.UserInfo;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.mapper.UserMapper;
import com.gitlab.bogdan776.springstore.service.UserService;
import com.gitlab.bogdan776.springstore.usecase.internal.SendConfirmEmailLetterUseCase;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;

import java.util.Optional;
import java.util.UUID;

class DefaultSignUpTest {

    private final UserService userService = Mockito.mock(UserService.class);
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    private final SendConfirmEmailLetterUseCase sendConfirmEmailLetter = Mockito.mock(SendConfirmEmailLetterUseCase.class);
    private final UUID userID = UUID.fromString("ac3c1884-e348-47f9-9f1c-61912c4185d0");

    private final SignUpUseCase signUp = new DefaultSignUp(userService, userMapper, sendConfirmEmailLetter);

    @Test
    void shouldBeNotNull_whenUserIsCorrectlyCreated() {
        SignUpRequest request = createRequest();

        Mockito.when(userService.create(
            request.getUsername(), request.getEmail(), request.getPassword(), request.getCountryCode())
        ).thenReturn(createUserProfile(request.getUsername(), request.getEmail(), request.getPassword()));

        UserProfileResponse response = signUp.execute(request);

        Mockito.verify(sendConfirmEmailLetter).execute(userID, request.getEmail());
        Assertions.assertThat(response).isNotNull();
    }

    @Test
    void shouldThrowException_whenUsernameExists() {
        SignUpRequest request = createRequest();
        Mockito.when(userService.findByUsername(request.getUsername())).thenReturn(
            Optional.of(createUserProfile(request.getUsername(), request.getEmail(), request.getPassword()))
        );
        Assertions.assertThatThrownBy(() -> signUp.execute(request))
            .isInstanceOf(WrongInputException.class)
            .hasMessage("User with this username already exists");
    }

    @Test
    void shouldThrowException_whenEmailExists() {
        SignUpRequest request = createRequest();
        Mockito.when(userService.findByEmail(request.getEmail())).thenReturn(
            Optional.of(createUserProfile(request.getUsername(), request.getEmail(), request.getPassword()))
        );
        Assertions.assertThatThrownBy(() -> signUp.execute(request))
            .isInstanceOf(WrongInputException.class)
            .hasMessage("User with this email already exists");
    }

    @Test
    void shouldThrowException_whenPasswordsDoNotMatch() {
        SignUpRequest request = createRequest();
        request.setRepeatPassword("qwerty123");

        Assertions.assertThatThrownBy(() -> signUp.execute(request))
            .isInstanceOf(WrongInputException.class)
            .hasMessage("Passwords must be equal");
    }

    private SignUpRequest createRequest() {
        SignUpRequest request = new SignUpRequest();
        request.setUsername("username");
        request.setEmail("user@springstore.com");
        request.setPassword("Qwerty123");
        request.setRepeatPassword("Qwerty123");
        request.setCountryCode(Country.UA.name());
        return request;
    }

    private UserProfile createUserProfile(String username, String email, String password) {
        UserProfile profile = new UserProfile();
        profile.setId(userID);
        profile.setUserInfo(new UserInfo());
        profile.setUsername(username);
        profile.setEmail(email);
        profile.setEmailConfirmed(false);
        profile.setPassword(password);
        profile.setCountry(Country.UA);
        profile.setRole(UserRole.USER);
        profile.setCreatedAt(TimeUtils.timeNow());
        return profile;
    }
}