package com.gitlab.bogdan776.springstore.usecase.auth;

import com.gitlab.bogdan776.springstore.dto.request.ResetPasswordRequest;

public interface GenerateRandomPasswordUseCase {

    void execute(ResetPasswordRequest request);
}
