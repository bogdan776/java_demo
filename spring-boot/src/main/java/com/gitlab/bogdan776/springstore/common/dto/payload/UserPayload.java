package com.gitlab.bogdan776.springstore.common.dto.payload;

import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import lombok.Data;

import java.util.UUID;

@Data
public class UserPayload {

    private UUID id;
    private String username;
    private UserRole role;
}
