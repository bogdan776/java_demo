package com.gitlab.bogdan776.springstore.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class WrongInputException extends WebException {

    public WrongInputException(String message) {
        super(message);
    }
}
