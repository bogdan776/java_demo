package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.ProductResponse;

import java.util.UUID;

public interface GetProductUseCase {

    ProductResponse execute(UUID productId);
}
