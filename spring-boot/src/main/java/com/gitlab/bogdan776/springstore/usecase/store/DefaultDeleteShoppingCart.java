package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.entity.store.ShoppingCart;
import com.gitlab.bogdan776.springstore.exception.CartNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.repository.store.ShoppingCartRepository;
import com.gitlab.bogdan776.springstore.scheduler.SchedulerService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultDeleteShoppingCart implements DeleteShoppingCartUseCase {

    private final ShoppingCartRepository repository;
    private final SchedulerService schedulerService;

    @Override
    @Transactional
    public void execute(UUID userId, UUID cartId) {
        if (cartId == null && userId == null) {
            throw new WrongInputException("Cookie value cannot be null for anonymous users");
        }
        ShoppingCart shoppingCart = Optional.ofNullable(cartId)
            .map(repository::findById)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .or(() -> repository.findByUserId(userId))
            .orElseThrow(() -> new CartNotFoundException(cartId, userId));
        schedulerService.unscheduleCartDeletion(shoppingCart.getDeletingCartJob());
        repository.delete(shoppingCart);
    }
}
