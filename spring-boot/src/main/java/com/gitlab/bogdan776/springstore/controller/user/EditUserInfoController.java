package com.gitlab.bogdan776.springstore.controller.user;

import com.gitlab.bogdan776.springstore.dto.request.UserInfoRequest;
import com.gitlab.bogdan776.springstore.dto.response.UserInfoResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.user.EditUserInfoUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class EditUserInfoController extends SecurityController implements UserController {

    private final EditUserInfoUseCase editUserInfo;

    @PutMapping(value = "/info", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserInfoResponse> editUserInfo(@RequestBody @Validated UserInfoRequest request) {
        return ResponseEntity.ok(editUserInfo.execute(getUserId(), request));
    }
}
