package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.request.CommentRequest;
import com.gitlab.bogdan776.springstore.dto.response.CommentResponse;

import java.util.UUID;

public interface CreateCommentUseCase {

    CommentResponse execute(UUID userId, UUID productId, CommentRequest request);
}
