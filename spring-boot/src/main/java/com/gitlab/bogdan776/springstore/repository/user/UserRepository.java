package com.gitlab.bogdan776.springstore.repository.user;

import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserProfile, UUID> {

    Optional<UserProfile> findByUsername(String username);

    Optional<UserProfile> findByEmail(String email);
}
