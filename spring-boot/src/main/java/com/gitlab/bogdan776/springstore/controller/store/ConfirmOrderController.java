package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.ConfirmOrderUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class ConfirmOrderController extends SecurityController implements StoreController {

    private final ConfirmOrderUseCase confirmOrder;

    @PutMapping(value = "/orders/{orderId}/confirmation", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderResponse> confirmOrder(@PathVariable("orderId") UUID orderId) {
        return ResponseEntity.ok(confirmOrder.execute(getUserId(), orderId));
    }
}
