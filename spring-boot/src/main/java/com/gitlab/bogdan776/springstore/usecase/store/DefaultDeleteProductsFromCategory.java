package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.entity.store.Category;
import com.gitlab.bogdan776.springstore.exception.CategoryNotFoundException;
import com.gitlab.bogdan776.springstore.repository.store.CategoryRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultDeleteProductsFromCategory implements DeleteProductsFromCategoryUseCase {

    private final CategoryRepository repository;

    @Override
    @Transactional
    public void execute(UUID categoryId, List<UUID> productIds) {
        Category category = repository.findById(categoryId)
            .orElseThrow(() -> new CategoryNotFoundException(categoryId));
        category.getProducts().removeIf(product -> productIds.contains(product.getId()));
        repository.save(category);
    }
}
