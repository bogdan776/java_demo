package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.request.CategoryRequest;
import com.gitlab.bogdan776.springstore.dto.response.CategoryShortResponse;
import com.gitlab.bogdan776.springstore.entity.store.Category;
import com.gitlab.bogdan776.springstore.exception.CategoryNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.CategoryMapper;
import com.gitlab.bogdan776.springstore.repository.store.CategoryRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultEditCategory implements EditCategoryUseCase {

    public final CategoryMapper mapper;
    private final CategoryRepository repository;

    @Override
    @Transactional
    public CategoryShortResponse execute(UUID categoryId, CategoryRequest request) {
        Category category = repository.findById(categoryId)
            .orElseThrow(() -> new CategoryNotFoundException(categoryId));
        category.setName(request.getName());
        category.setDisplayName(request.getDisplayName());
        category.setDescription(request.getDescription());
        return mapper.map(repository.save(category));
    }
}
