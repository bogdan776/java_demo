package com.gitlab.bogdan776.springstore.filestorage;

import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.function.Consumer;

public interface UploadFileUseCase {

    URI execute(MultipartFile file, String path, String name, Consumer<ResourceKeyJson> resourceKeyJsonConsumer);
}
