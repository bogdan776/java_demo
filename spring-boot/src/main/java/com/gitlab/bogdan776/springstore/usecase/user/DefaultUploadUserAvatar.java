package com.gitlab.bogdan776.springstore.usecase.user;

import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.filestorage.UploadFileUseCase;
import com.gitlab.bogdan776.springstore.service.UserService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.UUID;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
public class DefaultUploadUserAvatar implements UploadUserAvatarUseCase {

    private static final String USER_AVATAR_PATH = "users/avatars";
    private static final String USER_AVATAR_NAME_FORMAT = "%s_avatar";

    private final UserService userService;
    private final UploadFileUseCase uploadFile;

    @Override
    @Transactional
    public URI execute(UUID userId, MultipartFile avatar) {
        UserProfile userProfile = userService.findById(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));
        return uploadFile.execute(avatar, USER_AVATAR_PATH, format(USER_AVATAR_NAME_FORMAT, userProfile.getUsername()),
            resource -> {
                userProfile.setAvatarKey(resource);
                userService.save(userProfile);
            }
        );
    }
}
