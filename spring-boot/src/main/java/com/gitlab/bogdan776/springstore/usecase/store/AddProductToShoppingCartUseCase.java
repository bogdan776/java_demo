package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.ShoppingCartDataResponse;

import java.math.BigInteger;
import java.util.UUID;

public interface AddProductToShoppingCartUseCase {

    ShoppingCartDataResponse execute(UUID userId, UUID productId, BigInteger quantity, UUID cartId);
}
