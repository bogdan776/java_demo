package com.gitlab.bogdan776.springstore.usecase.user;

import com.gitlab.bogdan776.springstore.common.dto.payload.EmailPayload;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import com.gitlab.bogdan776.springstore.security.JwtService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultConfirmUserEmail implements ConfirmUserEmailUseCase {

    private final UserRepository repository;
    private final JwtService jwtService;

    @Override
    @Transactional
    public void execute(UUID userId, String token) {
        EmailPayload payload = jwtService.parseToken(token, EmailPayload.class);
        if (!userId.equals(payload.getUserId())) {
            throw new WrongInputException("User ids do not match");
        }
        UserProfile user = repository.findById(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));
        if (!payload.getEmail().equals(user.getEmail())) {
            throw new WrongInputException("Emails do not match");
        }
        user.setEmailConfirmed(true);
        repository.save(user);
    }
}
