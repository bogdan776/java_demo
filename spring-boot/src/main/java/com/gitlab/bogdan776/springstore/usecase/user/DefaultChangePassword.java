package com.gitlab.bogdan776.springstore.usecase.user;

import com.gitlab.bogdan776.springstore.dto.request.ChangePasswordRequest;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultChangePassword implements ChangePasswordUseCase {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void execute(UUID userId, ChangePasswordRequest request) {
        UserProfile user = repository.findById(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));
        if (!passwordEncoder.matches(request.getOldPassword(), user.getPassword())) {
            throw new WrongInputException("Wrong password");
        }
        if (!request.getNewPassword().equals(request.getRepeatPassword())) {
            throw new WrongInputException("New passwords must be equal");
        }
        user.setPassword(passwordEncoder.encode(request.getNewPassword()));
        repository.save(user);
    }
}
