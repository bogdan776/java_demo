package com.gitlab.bogdan776.springstore.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@Getter
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class CommentNotFoundException extends WebException {

    private static final String MESSAGE = "Comment not found";

    @JsonProperty
    private final UUID commentId;

    public CommentNotFoundException(UUID commentId) {
        super(MESSAGE);
        this.commentId = commentId;
    }
}
