package com.gitlab.bogdan776.springstore.dto.response;

import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import lombok.Data;

import java.util.UUID;

@Data
public class JwtResponse {

    private UUID userId;
    private String username;
    private UserRole role;
    private String token;
    private Long expiration;
}
