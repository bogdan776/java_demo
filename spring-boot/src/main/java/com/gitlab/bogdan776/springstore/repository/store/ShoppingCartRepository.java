package com.gitlab.bogdan776.springstore.repository.store;

import com.gitlab.bogdan776.springstore.common.dto.ProductQuantity;
import com.gitlab.bogdan776.springstore.entity.store.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, UUID> {

    Optional<ShoppingCart> findByUserId(UUID userId);

    @Modifying
    @Query("update ShoppingCart cart set cart.productQuantities = :quantities where cart.id = :id")
    void updateProductQuantities(@Param("id") UUID id, @Param("quantities") List<ProductQuantity> productQuantities);
}
