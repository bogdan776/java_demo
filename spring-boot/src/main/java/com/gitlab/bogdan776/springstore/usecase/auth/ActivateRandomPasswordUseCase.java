package com.gitlab.bogdan776.springstore.usecase.auth;

public interface ActivateRandomPasswordUseCase {

    void execute(String token);
}
