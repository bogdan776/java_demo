package com.gitlab.bogdan776.springstore.mapper;

import com.gitlab.bogdan776.springstore.dto.request.ProductRequest;
import com.gitlab.bogdan776.springstore.dto.response.ProductResponse;
import com.gitlab.bogdan776.springstore.dto.response.ProductShortResponse;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.data.domain.Page;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING, uses = ResourceKeyMapper.class)
public interface ProductMapper {

    @Mapping(target = "finalPrice", expression = "java(calculateFinalPrice(product.getPrice(), product.getDiscount()))")
    ProductResponse map(Product product);

    @Named("shortResponseMapper")
    @Mapping(target = "previewImageUrl", source = "previewImage.url")
    @Mapping(target = "finalPrice", expression = "java(calculateFinalPrice(product.getPrice(), product.getDiscount()))")
    @Mapping(target = "quantity", ignore = true)
    @Mapping(target = "totalPrice", ignore = true)
    ProductShortResponse mapToShortResponse(Product product);

    Product mapRequest(ProductRequest request);

    /*
    Discount price calculation
     */
    default BigDecimal calculateFinalPrice(BigDecimal price, BigDecimal discount) {
        if (discount == null) {
            return price;
        }
        return price.subtract(price.multiply(discount).divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP));
    }

    /*
    Price for N products
     */
    default BigDecimal calculateTotalPrice(BigInteger quantity, BigDecimal price) {
        return price.multiply(new BigDecimal(quantity));
    }

    @Named("mapPage")
    default Page<ProductShortResponse> mapPage(Page<Product> products) {
        return products.map(this::mapToShortResponse);
    }
}
