package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.entity.store.Order;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.OrderNotFoundException;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.OrderMapper;
import com.gitlab.bogdan776.springstore.repository.store.OrderRepository;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetOrder implements GetOrderUseCase {

    private final UserRepository userRepository;
    private final OrderRepository orderRepository;

    private final OrderMapper mapper;

    @Override
    public OrderResponse execute(UUID userId, UUID orderId) {
        Order order = orderRepository.findById(orderId)
            .orElseThrow(() -> new OrderNotFoundException(orderId));
        if (!userId.equals(order.getUser().getId())) {
            UserProfile user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
            if (user.getRole() == UserRole.ADMIN) {
                return mapper.mapWithUser(order);
            } else {
                throw new OrderNotFoundException(orderId);
            }
        }
        return mapper.map(order);
    }
}
