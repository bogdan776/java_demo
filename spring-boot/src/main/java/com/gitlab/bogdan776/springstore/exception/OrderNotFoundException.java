package com.gitlab.bogdan776.springstore.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@Getter
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class OrderNotFoundException extends WebException {

    private static final String MESSAGE = "Order not found";

    @JsonProperty
    private final UUID orderId;

    public OrderNotFoundException(UUID orderId) {
        super(MESSAGE);
        this.orderId = orderId;
    }
}
