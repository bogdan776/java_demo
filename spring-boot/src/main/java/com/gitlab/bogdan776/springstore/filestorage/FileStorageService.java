package com.gitlab.bogdan776.springstore.filestorage;

import java.io.InputStream;

public interface FileStorageService {

    ResourceKey upload(String path, InputStream inputStream);

    InputStream download(ResourceKey resourceKey);

    void delete(ResourceKey key);

    boolean doesFileExist(ResourceKey key);
}
