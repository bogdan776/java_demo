package com.gitlab.bogdan776.springstore.common.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

public final class TimeUtils {

    public static final ZoneId ZONE_ID = ZoneId.of("UTC");
    public static final ZoneOffset ZONE_OFFSET = ZoneOffset.UTC;

    private TimeUtils() {
    }

    public static long localDateTimeToMilli(LocalDateTime localDateTime) {
        return localDateTime.toInstant(ZONE_OFFSET).toEpochMilli();
    }

    public static Instant localDateTimeToInstant(LocalDateTime localDateTime) {
        return localDateTime.toInstant(ZONE_OFFSET);
    }

    public static LocalDateTime timeFromMillis(Long millis) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZONE_ID);
    }

    public static LocalDateTime timeNow() {
        return LocalDateTime.now(ZONE_ID);
    }

    public static LocalDate currentDate() {
        return LocalDate.now(ZONE_ID);
    }

    public static Date localDateTimeToDate(LocalDateTime dateTime) {
        return Date.from(localDateTimeToInstant(dateTime));
    }
}
