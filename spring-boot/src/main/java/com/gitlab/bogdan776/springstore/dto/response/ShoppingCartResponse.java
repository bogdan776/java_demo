package com.gitlab.bogdan776.springstore.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Data
@Builder(setterPrefix = "with")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShoppingCartResponse {

    private UUID id;
    private List<ProductShortResponse> products;
    private BigDecimal totalPrice;
}
