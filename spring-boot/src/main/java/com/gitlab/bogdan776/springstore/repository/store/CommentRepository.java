package com.gitlab.bogdan776.springstore.repository.store;

import com.gitlab.bogdan776.springstore.entity.store.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    Page<Comment> findAllByProductIdAndRootIsNull(UUID productId, Pageable pageable);
}
