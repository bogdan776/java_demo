package com.gitlab.bogdan776.springstore.usecase.auth;

import com.gitlab.bogdan776.springstore.dto.request.LoginRequest;
import com.gitlab.bogdan776.springstore.dto.response.JwtResponse;

public interface LoginUseCase {

    JwtResponse execute(LoginRequest request);
}
