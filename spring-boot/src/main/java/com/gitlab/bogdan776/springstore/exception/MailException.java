package com.gitlab.bogdan776.springstore.exception;

public class MailException extends SpringStoreException {

    public MailException(Throwable cause) {
        super(cause);
    }
}
