package com.gitlab.bogdan776.springstore.entity.store;

import com.gitlab.bogdan776.springstore.common.dto.Specification;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyJson;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@Table(name = "spring_store_product")
@EqualsAndHashCode(of = {"id", "name"})
public class Product {

    @Id
    @GeneratedValue
    private UUID id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @JdbcTypeCode(SqlTypes.JSON)
    private List<Specification> specifications;

    @JdbcTypeCode(SqlTypes.JSON)
    private ResourceKeyJson previewImage;

    @JdbcTypeCode(SqlTypes.JSON)
    private List<ResourceKeyJson> images;

    @NotNull
    private BigDecimal price;

    private BigDecimal discount;

    @NotNull
    private BigInteger availableQuantity;

    private BigDecimal rating;

    private BigInteger ratingCount;

    @NotNull
    private LocalDateTime addedAt;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "spring_store_products_categories",
        joinColumns = @JoinColumn(name = "product_id"),
        inverseJoinColumns = @JoinColumn(name = "category_id"))
    @ToString.Exclude
    private List<Category> categories;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
    private List<Comment> comments;
}
