package com.gitlab.bogdan776.springstore.mapper;

import com.gitlab.bogdan776.springstore.dto.request.CategoryRequest;
import com.gitlab.bogdan776.springstore.dto.response.CategoryResponse;
import com.gitlab.bogdan776.springstore.dto.response.CategoryShortResponse;
import com.gitlab.bogdan776.springstore.entity.store.Category;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING, uses = ProductMapper.class)
public interface CategoryMapper {

    CategoryShortResponse map(Category category);

    Category mapRequest(CategoryRequest request);

    @Mapping(target = "previewImageUrl", source = "category.previewImage.url")
    @Mapping(target = "products", source = "products", qualifiedByName = "mapPage")
    CategoryResponse map(Category category, Page<Product> products);
}
