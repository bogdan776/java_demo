package com.gitlab.bogdan776.springstore.usecase.store;

import java.util.UUID;

public interface DeleteCategoryPreviewImageUseCase {

    void execute(UUID categoryId);
}
