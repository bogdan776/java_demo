package com.gitlab.bogdan776.springstore.usecase.user;

import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.UUID;

public interface UploadUserAvatarUseCase {

    URI execute(UUID userId, MultipartFile avatar);
}
