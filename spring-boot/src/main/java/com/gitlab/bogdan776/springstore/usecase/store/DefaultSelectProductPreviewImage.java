package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.exception.ProductNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyJson;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyMapper;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultSelectProductPreviewImage implements SelectProductPreviewImageUseCase {

    private final ProductRepository repository;
    private final ResourceKeyMapper resourceKeyMapper;

    @Override
    @Transactional
    public ResourceKeyResponse execute(UUID productId, UUID imageId) {
        Product product = repository.findById(productId)
            .orElseThrow(() -> new ProductNotFoundException(productId));
        List<ResourceKeyJson> productImages = product.getImages();
        if (productImages == null || productImages.isEmpty()) {
            throw new WrongInputException("Upload product images before selecting");
        }
        return productImages.stream()
            .filter(resourceKey -> imageId.equals(resourceKey.getId()))
            .findFirst()
            .map(resourceKey -> {
                product.setPreviewImage(resourceKey);
                repository.save(product);
                return resourceKey;
            })
            .map(resourceKeyMapper::mapToResponse)
            .orElseThrow(() -> new WrongInputException(String.format("Image with id: %s not found", imageId)));
    }
}
