package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface UploadCategoryPreviewImageUseCase {

    ResourceKeyResponse execute(UUID categoryId, MultipartFile image);
}
