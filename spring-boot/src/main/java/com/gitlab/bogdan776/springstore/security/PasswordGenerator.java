package com.gitlab.bogdan776.springstore.security;

public interface PasswordGenerator {

    String generate();

    String generate(int length);
}
