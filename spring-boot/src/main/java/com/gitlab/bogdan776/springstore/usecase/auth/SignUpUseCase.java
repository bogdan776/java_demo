package com.gitlab.bogdan776.springstore.usecase.auth;

import com.gitlab.bogdan776.springstore.dto.request.SignUpRequest;
import com.gitlab.bogdan776.springstore.dto.response.UserProfileResponse;

public interface SignUpUseCase {

    UserProfileResponse execute(SignUpRequest request);
}
