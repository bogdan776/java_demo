package com.gitlab.bogdan776.springstore.common.constant;

import java.util.UUID;

public final class Constants {

    public static final UUID ADMIN_ID = UUID.fromString("00979819-3cc8-4c0b-b6da-aedd63cd9dd7");

    public static final String QUARTZ_JOB_DATA_UUID = "uuid";

    public static final String PASSWORD_REGEXP = "[a-zA-Z0-9#!%^&_-]{8,60}";
    public static final String USERNAME_REGEXP = "[a-zA-Z0-9_-]{3,20}";
    public static final String EMAIL_REGEXP = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}";

    private Constants() {
    }
}
