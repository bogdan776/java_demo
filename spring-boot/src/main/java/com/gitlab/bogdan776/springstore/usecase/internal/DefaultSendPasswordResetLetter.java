package com.gitlab.bogdan776.springstore.usecase.internal;

import com.gitlab.bogdan776.springstore.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Component
@RequiredArgsConstructor
public class DefaultSendPasswordResetLetter implements SendPasswordResetLetterUseCase {

    private final MailService mailService;
    private final TemplateEngine templateEngine;

    @Value("${host.api}")
    private String host;
    @Value("${spring.mail.template.reset-password}")
    private String templateName;

    @Override
    public void execute(String email, String generatedPassword, String token) {
        Context context = new Context();
        context.setVariable("host", host);
        context.setVariable("token", token);
        context.setVariable("generatedPassword", generatedPassword);
        mailService.sendMimeMessage(email, "Reset password", templateEngine.process(templateName, context));
    }
}
