package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.entity.store.Category;
import com.gitlab.bogdan776.springstore.exception.CategoryNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.filestorage.FileStorageService;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyJson;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyMapper;
import com.gitlab.bogdan776.springstore.repository.store.CategoryRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultDeleteCategoryPreviewImage implements DeleteCategoryPreviewImageUseCase {

    private final CategoryRepository repository;
    private final FileStorageService fileStorageService;
    private final ResourceKeyMapper resourceKeyMapper;

    @Override
    @Transactional
    public void execute(UUID categoryId) {
        Category category = repository.findById(categoryId)
            .orElseThrow(() -> new CategoryNotFoundException(categoryId));
        ResourceKeyJson previewImage = category.getPreviewImage();
        if (previewImage == null) {
            throw new WrongInputException("Category preview image does not exist");
        }
        fileStorageService.delete(resourceKeyMapper.map(previewImage));
        category.setPreviewImage(null);
        repository.save(category);
    }
}
