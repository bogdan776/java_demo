package com.gitlab.bogdan776.springstore.filestorage;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.net.URL;
import java.util.UUID;

@Setter
@Getter
@ToString
public class ResourceKeyResponse {

    private UUID id;
    private URL url;
}
