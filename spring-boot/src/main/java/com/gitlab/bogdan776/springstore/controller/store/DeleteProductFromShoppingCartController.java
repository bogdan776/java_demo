package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.common.constant.CookieNames;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.DeleteProductFromShoppingCartUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class DeleteProductFromShoppingCartController extends SecurityController implements StoreController {

    private final DeleteProductFromShoppingCartUseCase deleteProductFromShoppingCart;

    @DeleteMapping(value = "/carts/products/{productId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteProductFromShoppingCart(
        @PathVariable("productId") UUID productId,
        @RequestParam(value = "delete_completely", required = false, defaultValue = "false") Boolean deleteCompletely,
        @RequestParam(value = "quantity", required = false, defaultValue = "1") BigInteger quantity,
        @CookieValue(value = CookieNames.SHOPPING_CART_ID, required = false) UUID cartId
    ) {
        deleteProductFromShoppingCart.execute(isAnonymous() ? null : getUserId(), cartId, productId, deleteCompletely, quantity);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
