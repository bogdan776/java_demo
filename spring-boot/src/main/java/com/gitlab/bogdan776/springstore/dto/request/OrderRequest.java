package com.gitlab.bogdan776.springstore.dto.request;

import com.gitlab.bogdan776.springstore.common.constant.enums.Country;
import lombok.Data;

@Data
public class OrderRequest {

    private String fullName;
    private Country country;
    private String phoneNumber;
    private String email;
    private String address;
    private String comment;
}
