package com.gitlab.bogdan776.springstore.dto.request;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.gitlab.bogdan776.springstore.common.dto.Specification;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Data
public class ProductRequest {

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    private List<Specification> specifications;

    @NotNull
    private BigDecimal price;

    private BigDecimal discount;

    @NotNull
    @JsonAlias("available_quantity")
    private BigInteger availableQuantity;
}
