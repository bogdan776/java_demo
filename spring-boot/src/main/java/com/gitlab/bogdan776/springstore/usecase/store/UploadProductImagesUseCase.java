package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

public interface UploadProductImagesUseCase {

    List<ResourceKeyResponse> execute(UUID productId, List<MultipartFile> images);
}
