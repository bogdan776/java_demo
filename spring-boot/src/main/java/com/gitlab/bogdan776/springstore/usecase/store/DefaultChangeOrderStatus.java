package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.constant.enums.OrderStatus;
import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.exception.OrderNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.OrderMapper;
import com.gitlab.bogdan776.springstore.repository.store.OrderRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultChangeOrderStatus implements ChangeOrderStatusUseCase {

    private final OrderRepository repository;
    private final OrderMapper mapper;

    @Override
    @Transactional
    public OrderResponse execute(UUID orderId, OrderStatus status) {
        return repository.findById(orderId)
            .map(order -> {
                order.setStatus(status);
                repository.save(order);
                return order;
            })
            .map(mapper::mapWithUser)
            .orElseThrow(() -> new OrderNotFoundException(orderId));
    }
}
