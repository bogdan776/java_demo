package com.gitlab.bogdan776.springstore.dto.request;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.gitlab.bogdan776.springstore.common.constant.Constants;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class SignUpRequest {

    @NotBlank
    @Pattern(regexp = Constants.USERNAME_REGEXP)
    private String username;

    @NotBlank
    @Pattern(regexp = Constants.EMAIL_REGEXP)
    private String email;

    @NotBlank
    @Pattern(regexp = Constants.PASSWORD_REGEXP)
    private String password;

    @NotBlank
    @JsonAlias({"repeat_password", "repeatPassword"})
    private String repeatPassword;

    @NotBlank
    @JsonAlias({"country_code", "countryCode"})
    private String countryCode;
}
