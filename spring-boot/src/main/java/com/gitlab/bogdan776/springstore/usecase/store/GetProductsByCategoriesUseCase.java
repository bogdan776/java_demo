package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.ProductShortResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GetProductsByCategoriesUseCase {

    Page<ProductShortResponse> execute(List<String> categoryNames, Pageable pageable);
}
