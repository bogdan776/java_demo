package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.constant.enums.OrderStatus;
import com.gitlab.bogdan776.springstore.common.dto.DeliveryInfo;
import com.gitlab.bogdan776.springstore.common.dto.ProductQuantity;
import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.dto.request.OrderRequest;
import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.entity.store.Order;
import com.gitlab.bogdan776.springstore.entity.store.OrderedProduct;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.entity.store.ShoppingCart;
import com.gitlab.bogdan776.springstore.entity.user.UserInfo;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.CartNotFoundException;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.mapper.OrderMapper;
import com.gitlab.bogdan776.springstore.mapper.ProductMapper;
import com.gitlab.bogdan776.springstore.repository.store.OrderRepository;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import com.gitlab.bogdan776.springstore.repository.store.ShoppingCartRepository;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import com.gitlab.bogdan776.springstore.scheduler.SchedulerService;
import com.gitlab.bogdan776.springstore.usecase.internal.SendOrderLetterUseCase;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DefaultCreateOrder implements CreateOrderUseCase {

    private final UserRepository userRepository;
    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;

    private final SchedulerService schedulerService;

    private final ProductMapper productMapper;
    private final OrderMapper orderMapper;

    private final SendOrderLetterUseCase sendOrderLetter;

    @Override
    @Transactional
    public OrderResponse execute(UUID userId, UUID cartId, OrderRequest request, Boolean force) {
        UserProfile user = userRepository.findById(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserId(user.getId())
            .or(() -> shoppingCartRepository.findById(cartId))
            .orElseThrow(() -> new CartNotFoundException(cartId, userId));
        if (shoppingCart.getProductQuantities() == null) {
            throw new WrongInputException("Cart is empty. Put the product(s) in the shopping cart or order separately");
        }

        Order order = createOrder(user, request);


        Map<UUID, BigInteger> productQuantityMap = shoppingCart.getProductQuantities().stream()
            .collect(Collectors.toMap(ProductQuantity::getProductId, ProductQuantity::getQuantity));
        List<Product> products = productRepository.findAllById(productQuantityMap.keySet());
        validateAvailableQuantity(products, productQuantityMap, force);

        List<OrderedProduct> orderedProducts = products.stream()
            .map(product -> createOrderedProduct(order, product, productQuantityMap.get(product.getId())))
            .toList();
        BigDecimal totalPrice = orderedProducts.stream()
            .map(orderedProduct -> productMapper.calculateTotalPrice(
                orderedProduct.getQuantity(), orderedProduct.getUnitPrice()
            )).reduce(BigDecimal.ZERO, BigDecimal::add);
        order.setOrderedProducts(orderedProducts);
        order.setPrice(totalPrice);
        updateProductAvailableQuantity(products, productQuantityMap);
        orderRepository.save(order);
        schedulerService.unscheduleCartDeletion(shoppingCart.getDeletingCartJob());
        shoppingCartRepository.delete(shoppingCart);
        OrderResponse response = orderMapper.map(order);
        sendOrderLetter.execute(order.getDeliveryInfo().getEmail(), response);
        return response;
    }

    private void validateAvailableQuantity(List<Product> products, Map<UUID, BigInteger> productQuantityMap, Boolean force) {
        if (force) {
            return;
        }
        for (Product product : products) {
            if (product.getAvailableQuantity().compareTo(productQuantityMap.get(product.getId())) < 0) {
                throw new WrongInputException("Not enough product in stock");
            }
        }
    }

    private void updateProductAvailableQuantity(List<Product> products, Map<UUID, BigInteger> productQuantityMap) {
        for (Product product : products) {
            BigInteger requiredQuantity = productQuantityMap.get(product.getId());
            if (product.getAvailableQuantity().compareTo(requiredQuantity) > 0) {
                product.setAvailableQuantity(product.getAvailableQuantity().subtract(requiredQuantity));
            } else {
                product.setAvailableQuantity(BigInteger.ZERO);
            }
        }
        productRepository.saveAll(products);
    }

    private Order createOrder(UserProfile user, OrderRequest request) {
        Order order = new Order();
        order.setId(UUID.randomUUID());
        order.setUser(user);
        order.setStatus(OrderStatus.RECEIVED);
        order.setDeliveryInfo(deliveryInfo(user, request));
        order.setOrderCancellationJob(schedulerService.scheduleOrderCancellation(order.getId(), TimeUtils.timeNow().plusMinutes(2)));
        order.setCreatedAt(TimeUtils.timeNow());
        return order;
    }

    private DeliveryInfo deliveryInfo(UserProfile user, OrderRequest request) {
        UserInfo userInfo = user.getUserInfo();
        DeliveryInfo deliveryInfo = new DeliveryInfo();
        deliveryInfo.setFullName(requireNonNullElse("fullName", request.getFullName(), userInfo.getFullName()));
        deliveryInfo.setCountry(requireNonNullElse("country", request.getCountry(), user.getCountry()));
        deliveryInfo.setPhoneNumber(requireNonNullElse("phoneNumber", request.getPhoneNumber(), userInfo.getPhoneNumber()));
        deliveryInfo.setEmail(requireNonNullElse("email", request.getEmail(), user.getEmail()));
        deliveryInfo.setAddress(requireNonNullElse("address", request.getAddress(), userInfo.getAddress()));
        deliveryInfo.setComment(request.getComment());
        return deliveryInfo;
    }

    private <T> T requireNonNullElse(String paramName, T value, T defaultValue) {
        if (value == null) {
            if (defaultValue == null) {
                throw new WrongInputException(paramName + " cannot be empty");
            } else {
                return defaultValue;
            }
        }
        return value;
    }

    private OrderedProduct createOrderedProduct(Order order, Product product, BigInteger quantity) {
        OrderedProduct orderedProduct = new OrderedProduct();
        orderedProduct.setOrder(order);
        orderedProduct.setProduct(product);
        orderedProduct.setUnitPrice(productMapper.calculateFinalPrice(product.getPrice(), product.getDiscount()));
        orderedProduct.setQuantity(
            product.getAvailableQuantity().compareTo(quantity) > 0 ? quantity : product.getAvailableQuantity()
        );
        orderedProduct.setCreatedAt(TimeUtils.timeNow());
        return orderedProduct;
    }
}
