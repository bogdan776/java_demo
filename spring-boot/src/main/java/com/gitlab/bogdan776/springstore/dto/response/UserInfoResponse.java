package com.gitlab.bogdan776.springstore.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfoResponse {

    private String fullName;
    private String address;
    private String phoneNumber;
    private String introduction;
}
