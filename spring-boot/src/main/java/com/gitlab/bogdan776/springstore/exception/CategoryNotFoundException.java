package com.gitlab.bogdan776.springstore.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@Getter
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class CategoryNotFoundException extends WebException {

    private static final String MESSAGE = "Category not found";

    @JsonProperty
    private final UUID categoryId;

    public CategoryNotFoundException(UUID categoryId) {
        super(MESSAGE);
        this.categoryId = categoryId;
    }
}
