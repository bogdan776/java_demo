package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.CategoryShortResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GetCategoriesUseCase {

    Page<CategoryShortResponse> execute(Pageable pageable);
}
