package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.common.constant.CookieNames;
import com.gitlab.bogdan776.springstore.dto.response.ShoppingCartResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.GetShoppingCartUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class GetShoppingCartController extends SecurityController implements StoreController {

    private final GetShoppingCartUseCase getShoppingCart;

    @GetMapping(value = "/carts", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ShoppingCartResponse> getShoppingCart(
        @CookieValue(value = CookieNames.SHOPPING_CART_ID, required = false) UUID cartId
    ) {
        return ResponseEntity.ok(getShoppingCart.execute(isAnonymous() ? null : getUserId(), cartId));
    }
}
