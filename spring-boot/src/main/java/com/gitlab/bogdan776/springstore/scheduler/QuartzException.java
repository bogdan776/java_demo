package com.gitlab.bogdan776.springstore.scheduler;

import com.gitlab.bogdan776.springstore.exception.SpringStoreException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class QuartzException extends SpringStoreException {

    public QuartzException(String message, Throwable cause) {
        super(message, cause);
    }
}
