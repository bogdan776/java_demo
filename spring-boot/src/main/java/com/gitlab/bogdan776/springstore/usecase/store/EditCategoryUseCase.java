package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.request.CategoryRequest;
import com.gitlab.bogdan776.springstore.dto.response.CategoryShortResponse;

import java.util.UUID;

public interface EditCategoryUseCase {

    CategoryShortResponse execute(UUID categoryId, CategoryRequest request);
}
