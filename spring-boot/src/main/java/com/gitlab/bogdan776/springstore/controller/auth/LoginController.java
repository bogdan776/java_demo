package com.gitlab.bogdan776.springstore.controller.auth;

import com.gitlab.bogdan776.springstore.dto.request.LoginRequest;
import com.gitlab.bogdan776.springstore.dto.response.JwtResponse;
import com.gitlab.bogdan776.springstore.usecase.auth.LoginUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class LoginController {

    private final LoginUseCase login;

    @PostMapping(value = "/login", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<JwtResponse> login(@RequestBody @Validated LoginRequest request) {
        return ResponseEntity.ok(login.execute(request));
    }
}
