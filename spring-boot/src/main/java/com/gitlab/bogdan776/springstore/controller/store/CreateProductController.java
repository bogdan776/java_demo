package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.request.ProductRequest;
import com.gitlab.bogdan776.springstore.dto.response.ProductResponse;
import com.gitlab.bogdan776.springstore.usecase.store.CreateProductUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class CreateProductController implements StoreController {

    private final CreateProductUseCase createProduct;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping(value = "/products", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductResponse> createProduct(@RequestBody @Validated ProductRequest request) {
        return ResponseEntity.ok(createProduct.execute(request));
    }
}
