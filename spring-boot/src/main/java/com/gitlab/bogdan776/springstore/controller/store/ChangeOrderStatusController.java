package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.common.constant.enums.OrderStatus;
import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.ChangeOrderStatusUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class ChangeOrderStatusController extends SecurityController implements StoreController {

    private final ChangeOrderStatusUseCase changeOrderStatus;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping(value = "/orders/{orderId}/statuses/{statusName}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderResponse> changeOrderStatus(@PathVariable("orderId") UUID orderId,
                                                           @PathVariable("statusName") OrderStatus orderStatus) {
        return ResponseEntity.ok(changeOrderStatus.execute(orderId, orderStatus));
    }
}
