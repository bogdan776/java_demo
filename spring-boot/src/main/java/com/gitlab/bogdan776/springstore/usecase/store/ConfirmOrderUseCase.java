package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;

import java.util.UUID;

public interface ConfirmOrderUseCase {

    OrderResponse execute(UUID userId, UUID orderId);
}
