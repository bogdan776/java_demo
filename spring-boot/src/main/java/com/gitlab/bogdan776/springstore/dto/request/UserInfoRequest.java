package com.gitlab.bogdan776.springstore.dto.request;

import lombok.Data;

@Data
public class UserInfoRequest {

    private String fullName;
    private String address;
    private String phoneNumber;
    private String introduction;
}
