package com.gitlab.bogdan776.springstore.entity.user;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "userId")
@Table(name = "spring_store_user_info")
public class UserInfo {

    @Id
    @Column(name = "user_id")
    private UUID userId;

    @MapsId("userId")
    @OneToOne(mappedBy = "userInfo", fetch = FetchType.LAZY)
    @ToString.Exclude
    private UserProfile user;

    private String fullName;

    private String address;

    private String phoneNumber;

    private String introduction;
}
