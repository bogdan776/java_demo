package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.request.CategoryRequest;
import com.gitlab.bogdan776.springstore.dto.response.CategoryShortResponse;

public interface CreateCategoryUseCase {

    CategoryShortResponse execute(CategoryRequest request);
}
