package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.response.CommentResponse;
import com.gitlab.bogdan776.springstore.usecase.store.GetProductCommentsUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class GetProductCommentsController implements StoreController {

    private final GetProductCommentsUseCase getProductComments;

    @GetMapping(value = "/products/{productId}/comments", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<CommentResponse>> getProductComments(@PathVariable("productId") UUID productId,
                                                                    Pageable pageable) {
        return ResponseEntity.ok(getProductComments.execute(productId, pageable));
    }
}
