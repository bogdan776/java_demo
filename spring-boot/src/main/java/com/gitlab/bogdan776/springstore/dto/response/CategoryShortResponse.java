package com.gitlab.bogdan776.springstore.dto.response;

import lombok.Data;

import java.util.UUID;

@Data
public class CategoryShortResponse {

    private UUID id;
    private String name;
    private String displayName;
    private String description;
}
