package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;
import com.gitlab.bogdan776.springstore.filestorage.validation.ImageConstraint;
import com.gitlab.bogdan776.springstore.usecase.store.UploadProductImagesUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@Validated
@RestController
@RequiredArgsConstructor
public class UploadProductImagesController implements StoreController {

    private final UploadProductImagesUseCase uploadProductImages;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping(
        value = "/products/{productId}/images",
        consumes = MULTIPART_FORM_DATA_VALUE,
        produces = APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<ResourceKeyResponse>> uploadImages(@PathVariable("productId") UUID productId,
                                                                  @RequestParam("images") List<@ImageConstraint MultipartFile> images) {
        return ResponseEntity.ok(uploadProductImages.execute(productId, images));
    }
}
