package com.gitlab.bogdan776.springstore.usecase.user;

import com.gitlab.bogdan776.springstore.dto.request.UserInfoRequest;
import com.gitlab.bogdan776.springstore.dto.response.UserInfoResponse;

import java.util.UUID;

public interface EditUserInfoUseCase {

    UserInfoResponse execute(UUID userId, UserInfoRequest request);
}
