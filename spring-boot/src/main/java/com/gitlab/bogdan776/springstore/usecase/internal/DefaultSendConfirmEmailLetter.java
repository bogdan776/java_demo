package com.gitlab.bogdan776.springstore.usecase.internal;

import com.gitlab.bogdan776.springstore.common.dto.payload.EmailPayload;
import com.gitlab.bogdan776.springstore.security.JwtService;
import com.gitlab.bogdan776.springstore.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultSendConfirmEmailLetter implements SendConfirmEmailLetterUseCase {

    private final MailService mailService;
    private final TemplateEngine templateEngine;
    private final JwtService jwtService;

    @Value("${host.api}")
    private String host;
    @Value("${spring.mail.template.confirm-email}")
    private String templateName;

    @Override
    public void execute(UUID userId, String email) {
        String token = jwtService.createToken(new EmailPayload(userId, email));
        Context context = new Context();
        context.setVariable("host", host);
        context.setVariable("token", token);
        context.setVariable("userId", userId);
        mailService.sendMimeMessage(email, "Confirm email", templateEngine.process(templateName, context));
    }
}
