package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.dto.request.ProductRequest;
import com.gitlab.bogdan776.springstore.dto.response.ProductResponse;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.mapper.ProductMapper;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class DefaultCreateProduct implements CreateProductUseCase {

    private final ProductRepository repository;
    private final ProductMapper mapper;

    @Override
    @Transactional
    public ProductResponse execute(ProductRequest request) {
        Product product = mapper.mapRequest(request);
        product.setDiscount(Objects.requireNonNullElse(request.getDiscount(), BigDecimal.ZERO));
        product.setAddedAt(TimeUtils.timeNow());
        return mapper.map(repository.save(product));
    }
}
