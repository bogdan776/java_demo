package com.gitlab.bogdan776.springstore.controller.base;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.dto.response.ExceptionResponse;
import com.gitlab.bogdan776.springstore.dto.response.WrongParameterResponse;
import com.gitlab.bogdan776.springstore.exception.SpringStoreException;
import jakarta.annotation.Nonnull;
import jakarta.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.Objects;

@RestControllerAdvice
@RequiredArgsConstructor
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private final ObjectMapper objectMapper;

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ExceptionResponse> handleRuntimeException(RuntimeException runtimeException) {
        return new ResponseEntity<>(exceptionResponse(runtimeException), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ExceptionResponse> handleAccessDeniedException(AccessDeniedException exception) {
        return new ResponseEntity<>(exceptionResponse(exception), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(SpringStoreException.class)
    public ResponseEntity<ExceptionResponse> handleAppException(SpringStoreException springStoreException) {
        JsonNode data = objectMapper.convertValue(springStoreException, JsonNode.class);
        ResponseStatus annotation = AnnotationUtils.findAnnotation(springStoreException.getClass(), ResponseStatus.class);

        return Objects.isNull(annotation)
            ? new ResponseEntity<>(exceptionResponse(springStoreException).setData(data), HttpStatus.BAD_REQUEST)
            : new ResponseEntity<>(exceptionResponse(springStoreException, annotation).setData(data), annotation.code());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException constraintException) {
        List<WrongParameterResponse> wrongParameters = constraintException.getConstraintViolations().stream().map(violation -> {
            if (violation.getPropertyPath() != null) {
                return new WrongParameterResponse(violation.getPropertyPath().toString(), violation.getMessage());
            }
            DefaultMessageSourceResolvable messageSource = getMessageSource(violation.getExecutableParameters());
            return Objects.isNull(messageSource)
                ? new WrongParameterResponse(null, violation.getMessage())
                : new WrongParameterResponse(messageSource.getDefaultMessage(), violation.getMessage());
        }).toList();
        return new ResponseEntity<>(exceptionResponse(constraintException).setWrongParameters(wrongParameters), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(@Nonnull Exception ex,
                                                             Object body,
                                                             @Nonnull HttpHeaders headers,
                                                             @Nonnull HttpStatusCode statusCode,
                                                             @Nonnull WebRequest request) {
        return new ResponseEntity<>(exceptionResponse(ex), statusCode);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@Nonnull MethodArgumentNotValidException ex,
                                                                  @Nonnull HttpHeaders headers,
                                                                  @Nonnull HttpStatusCode status,
                                                                  @Nonnull WebRequest request) {
        List<WrongParameterResponse> wrongParameters = ex.getBindingResult().getAllErrors().stream().map(objectError -> {
            DefaultMessageSourceResolvable messageSource = getMessageSource(objectError.getArguments());
            return Objects.nonNull(messageSource)
                ? new WrongParameterResponse(messageSource.getDefaultMessage(), objectError.getDefaultMessage())
                : new WrongParameterResponse(null, objectError.getDefaultMessage());
        }).toList();
        return new ResponseEntity<>(exceptionResponse(ex).setWrongParameters(wrongParameters), status);
    }

    private DefaultMessageSourceResolvable getMessageSource(Object[] arguments) {
        if (arguments != null && arguments.length > 0) {
            if (arguments[0] instanceof DefaultMessageSourceResolvable messageSource) {
                return messageSource;
            }
        }
        return null;
    }

    private ExceptionResponse exceptionResponse(String message) {
        return new ExceptionResponse().setMessage(message).setDateTime(TimeUtils.timeNow());
    }

    private ExceptionResponse exceptionResponse(Throwable throwable) {
        return exceptionResponse(throwable.getMessage());
    }

    private ExceptionResponse exceptionResponse(Throwable throwable, ResponseStatus annotation) {
        if (annotation.reason().isEmpty()) {
            return exceptionResponse(throwable);
        }
        return exceptionResponse(annotation.reason());
    }
}
