package com.gitlab.bogdan776.springstore.security;

import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Component
public final class SimplePasswordGenerator implements PasswordGenerator {

    private static final int DEFAULT_LENGTH = 15;
    private static final char[] BASE64_CODE = {
        '_',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };

    @Override
    public String generate() {
        return generate(DEFAULT_LENGTH);
    }

    @Override
    public String generate(int length) {
        return ThreadLocalRandom.current().ints(length, 0, BASE64_CODE.length)
            .mapToObj(randomIndex -> String.valueOf(BASE64_CODE[randomIndex]))
            .collect(Collectors.joining());
    }
}
