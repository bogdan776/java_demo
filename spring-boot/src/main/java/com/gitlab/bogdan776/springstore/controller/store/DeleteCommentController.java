package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.response.CommentResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.DeleteCommentUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class DeleteCommentController extends SecurityController implements StoreController {

    private final DeleteCommentUseCase deleteComment;

    @DeleteMapping(value = "/products/{productId}/comments/{commentId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CommentResponse> deleteComment(@PathVariable("productId") UUID productId,
                                                         @PathVariable("commentId") UUID commentId) {
        deleteComment.execute(getUserId(), productId, commentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
