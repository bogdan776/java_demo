package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.GetUserOrdersUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class GetUserOrdersController extends SecurityController implements StoreController {

    private final GetUserOrdersUseCase getUserOrders;

    @GetMapping(value = "/orders", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<OrderResponse>> getUserOrders(Pageable pageable) {
        return ResponseEntity.ok(getUserOrders.execute(getUserId(), pageable));
    }
}
