package com.gitlab.bogdan776.springstore.common.constant.enums;

public enum OrderStatus {
    RECEIVED, PENDING, PROCESSING, FULFILLED, CANCELLED, PREPARING, IN_PROGRESS, COMPLETED, ERROR
}
