package com.gitlab.bogdan776.springstore.controller.auth;

import com.gitlab.bogdan776.springstore.controller.PublicController;
import com.gitlab.bogdan776.springstore.usecase.auth.ActivateRandomPasswordUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@PreAuthorize("isAnonymous()")
public class ActivateRandomPasswordController implements PublicController {

    private final ActivateRandomPasswordUseCase activateRandomPassword;

    @RequestMapping(
        value = "/passwords",
        method = {RequestMethod.GET, RequestMethod.POST},
        produces = APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> activateRandomPassword(@RequestParam("token") String token) {
        activateRandomPassword.execute(token);
        return ResponseEntity.ok("Random password activated");
    }
}
