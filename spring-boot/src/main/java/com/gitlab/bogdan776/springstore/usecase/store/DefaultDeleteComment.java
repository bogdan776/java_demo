package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import com.gitlab.bogdan776.springstore.entity.store.Comment;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.CommentNotFoundException;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.repository.store.CommentRepository;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultDeleteComment implements DeleteCommentUseCase {

    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final CommentRepository commentRepository;

    @Override
    @Transactional
    public void execute(UUID userId, UUID productId, UUID commentId) {
        Comment comment = commentRepository.findById(commentId)
            .orElseThrow(() -> new CommentNotFoundException(commentId));
        if (!userId.equals(comment.getAuthor().getId())) {
            UserProfile user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
            if (user.getRole() != UserRole.ADMIN) {
                throw new WrongInputException("You cannot delete this comment");
            }
        }
        recalculateProductRating(comment);
        commentRepository.delete(comment);
    }

    private void recalculateProductRating(Comment comment) {
        if (comment.getRating() == null) {
            return;
        }
        Product product = comment.getProduct();
        if (product.getRating() != null) {
            BigDecimal count = new BigDecimal(product.getRatingCount());
            if (count.compareTo(BigDecimal.ONE) == 0) {
                product.setRating(BigDecimal.ZERO);
            } else {
                BigDecimal newRatingValue = product.getRating().multiply(count)
                    .subtract(new BigDecimal(comment.getRating()))
                    .divide(count.subtract(BigDecimal.ONE), 1, RoundingMode.HALF_UP);
                product.setRating(newRatingValue);
            }
        }
        Optional.ofNullable(product.getRatingCount())
            .map(count -> count.subtract(BigInteger.ONE))
            .ifPresent(product::setRatingCount);
        productRepository.save(product);
    }
}
