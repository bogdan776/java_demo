package com.gitlab.bogdan776.springstore.usecase.internal;

import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;

public interface SendOrderLetterUseCase {

    void execute(String email, OrderResponse response);
}
