package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.dto.ProductQuantity;
import com.gitlab.bogdan776.springstore.dto.response.ShoppingCartDataResponse;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.entity.store.ShoppingCart;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.ProductNotFoundException;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import com.gitlab.bogdan776.springstore.repository.store.ShoppingCartRepository;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import com.gitlab.bogdan776.springstore.scheduler.SchedulerService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static com.gitlab.bogdan776.springstore.common.utils.TimeUtils.localDateTimeToMilli;
import static com.gitlab.bogdan776.springstore.common.utils.TimeUtils.timeNow;

@Component
@RequiredArgsConstructor
public class DefaultAddProductToShoppingCart implements AddProductToShoppingCartUseCase {

    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final ShoppingCartRepository shoppingCartRepository;
    private final SchedulerService schedulerService;

    @Value("${store.cart.expiration:1}")
    private Integer cartExpirationDays;

    @Override
    @Transactional
    public ShoppingCartDataResponse execute(UUID userId, UUID productId, BigInteger quantity, UUID cartId) {
        Product product = productRepository.findById(productId)
            .orElseThrow(() -> new ProductNotFoundException(productId));
        if (product.getAvailableQuantity().compareTo(BigInteger.ZERO) == 0) {
            throw new WrongInputException("Product is out of stock");
        }
        if (product.getAvailableQuantity().compareTo(quantity) < 0) {
            throw new WrongInputException("Insufficient quantity of goods");
        }
        ShoppingCart shoppingCart = Optional.ofNullable(cartId)
            .map(shoppingCartRepository::findById)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .or(() -> shoppingCartRepository.findByUserId(userId))
            .orElseGet(this::createNewShoppingCart);

        List<ProductQuantity> productQuantities = Objects.requireNonNullElse(shoppingCart.getProductQuantities(), new ArrayList<>());
        productQuantities.stream()
            .filter(productCounter -> productId.equals(productCounter.getProductId()))
            .findFirst()
            .ifPresentOrElse(
                productQuantity -> {
                    BigInteger newQuantity = productQuantity.getQuantity().add(quantity);
                    if (newQuantity.compareTo(product.getAvailableQuantity()) > 0) {
                        throw new WrongInputException("Insufficient quantity of goods");
                    }
                    productQuantity.setQuantity(newQuantity);
                    shoppingCartRepository.updateProductQuantities(shoppingCart.getId(), productQuantities);
                },
                () -> productQuantities.add(new ProductQuantity(product.getId(), quantity))
            );
        shoppingCart.setProductQuantities(productQuantities);
        setUserOrScheduleCartDeletion(shoppingCart, userId);
        shoppingCartRepository.save(shoppingCart);
        return ShoppingCartDataResponse.builder()
            .withCartId(shoppingCart.getId())
            .withExpiration(
                shoppingCart.getUser() == null
                    ? localDateTimeToMilli(shoppingCart.getCreatedAt().plusDays(cartExpirationDays))
                    : Long.MIN_VALUE
            ).build();
    }

    private void setUserOrScheduleCartDeletion(ShoppingCart shoppingCart, UUID userId) {
        if (shoppingCart.getUser() == null && userId != null) {
            UserProfile userProfile = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
            shoppingCart.setUser(userProfile);
            if (shoppingCart.getDeletingCartJob() != null) {
                schedulerService.unscheduleCartDeletion(shoppingCart.getDeletingCartJob());
            }
        } else if (shoppingCart.getUser() == null && shoppingCart.getDeletingCartJob() == null) {
            shoppingCart.setDeletingCartJob(
                schedulerService.scheduleCartDeletion(shoppingCart.getId(), timeNow().plusDays(cartExpirationDays))
            );
        }
    }

    private ShoppingCart createNewShoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setId(UUID.randomUUID());
        shoppingCart.setCreatedAt(timeNow());
        return shoppingCart;
    }
}
