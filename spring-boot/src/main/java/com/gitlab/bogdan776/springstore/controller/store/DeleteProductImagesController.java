package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.usecase.store.DeleteProductImagesUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
@RequiredArgsConstructor
public class DeleteProductImagesController implements StoreController {

    private final DeleteProductImagesUseCase deleteProductImages;

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping(
        value = "/products/{productId}/images/{imageIds}",
        consumes = MULTIPART_FORM_DATA_VALUE,
        produces = APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Object> deleteProductImages(@PathVariable("productId") UUID productId,
                                                      @PathVariable("imageIds") List<UUID> imageIds) {
        deleteProductImages.execute(productId, imageIds);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
