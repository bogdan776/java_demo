package com.gitlab.bogdan776.springstore.filestorage.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class ImageConstraintValidator implements ConstraintValidator<ImageConstraint, MultipartFile> {

    private List<String> supportedMediaTypes;
    private long maxBytes;

    @Override
    public void initialize(ImageConstraint constraintAnnotation) {
        this.supportedMediaTypes = List.of(constraintAnnotation.mediaTypes());
        this.maxBytes = constraintAnnotation.maxSize();
    }

    @Override
    public boolean isValid(MultipartFile file, ConstraintValidatorContext constraintValidatorContext) {
        return file.getSize() <= maxBytes && supportedMediaTypes.contains(file.getContentType());
    }
}
