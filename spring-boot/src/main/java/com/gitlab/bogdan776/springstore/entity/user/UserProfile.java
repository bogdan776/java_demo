package com.gitlab.bogdan776.springstore.entity.user;

import com.gitlab.bogdan776.springstore.common.constant.enums.Country;
import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyJson;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@Table(name = "spring_store_user_profile")
@EqualsAndHashCode(of = {"id", "username", "email"})
public class UserProfile {

    @Id
    @NotNull
    private UUID id;

    @PrimaryKeyJoinColumn
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    private UserInfo userInfo;

    @NotNull
    @Column(unique = true)
    private String username;

    @NotNull
    @Column(unique = true)
    private String email;

    private boolean emailConfirmed;

    @NotNull
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Country country;

    @NotNull
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @JdbcTypeCode(SqlTypes.JSON)
    private ResourceKeyJson avatarKey;

    @NotNull
    private LocalDateTime createdAt;
}
