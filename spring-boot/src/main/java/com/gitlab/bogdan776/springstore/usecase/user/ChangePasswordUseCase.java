package com.gitlab.bogdan776.springstore.usecase.user;

import com.gitlab.bogdan776.springstore.dto.request.ChangePasswordRequest;

import java.util.UUID;

public interface ChangePasswordUseCase {

    void execute(UUID userId, ChangePasswordRequest request);
}
