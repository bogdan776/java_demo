package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.dto.request.CommentRequest;
import com.gitlab.bogdan776.springstore.dto.response.CommentResponse;
import com.gitlab.bogdan776.springstore.entity.store.Comment;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.CommentNotFoundException;
import com.gitlab.bogdan776.springstore.exception.ProductNotFoundException;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.CommentMapper;
import com.gitlab.bogdan776.springstore.repository.store.CommentRepository;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultReplyToComment implements ReplyToCommentUseCase {

    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final CommentRepository commentRepository;
    private final CommentMapper mapper;

    @Override
    @Transactional
    public CommentResponse execute(UUID userId, UUID productId, UUID commentId, CommentRequest request) {
        UserProfile user = userRepository.findById(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));
        Product product = productRepository.findById(productId)
            .orElseThrow(() -> new ProductNotFoundException(productId));
        Comment rootComment = commentRepository.findById(commentId)
            .orElseThrow(() -> new CommentNotFoundException(commentId));
        Comment reply = new Comment();
        reply.setAuthor(user);
        reply.setProduct(product);
        reply.setRoot(rootComment);
        reply.setContent(request.getContent());
        reply.setCreatedAt(TimeUtils.timeNow());
        return mapper.map(commentRepository.save(reply));
    }
}
