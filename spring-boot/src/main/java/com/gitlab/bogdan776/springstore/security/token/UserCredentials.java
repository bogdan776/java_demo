package com.gitlab.bogdan776.springstore.security.token;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserCredentials {

    private String email;
    private String password;
}
