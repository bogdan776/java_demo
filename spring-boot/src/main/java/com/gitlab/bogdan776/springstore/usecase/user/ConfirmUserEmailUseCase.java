package com.gitlab.bogdan776.springstore.usecase.user;

import java.util.UUID;

public interface ConfirmUserEmailUseCase {

    void execute(UUID userId, String token);
}
