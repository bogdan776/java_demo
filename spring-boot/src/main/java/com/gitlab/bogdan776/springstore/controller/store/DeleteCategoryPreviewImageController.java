package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;
import com.gitlab.bogdan776.springstore.usecase.store.DeleteCategoryPreviewImageUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class DeleteCategoryPreviewImageController implements StoreController {

    private final DeleteCategoryPreviewImageUseCase deleteCategoryPreviewImage;

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping(value = "/categories/{categoryId}/preview", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResourceKeyResponse> uploadPreviewImage(@PathVariable("categoryId") UUID categoryId) {
        deleteCategoryPreviewImage.execute(categoryId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
