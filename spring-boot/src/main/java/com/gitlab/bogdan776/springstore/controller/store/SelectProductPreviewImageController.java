package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;
import com.gitlab.bogdan776.springstore.usecase.store.SelectProductPreviewImageUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class SelectProductPreviewImageController implements StoreController {

    private final SelectProductPreviewImageUseCase selectProductPreviewImage;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping(value = "/products/{productId}/images/{imageId}/preview", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResourceKeyResponse> selectProductPreview(@PathVariable("productId") UUID productId,
                                                                    @PathVariable("imageId") UUID imageId) {
        return ResponseEntity.ok(selectProductPreviewImage.execute(productId, imageId));
    }
}
