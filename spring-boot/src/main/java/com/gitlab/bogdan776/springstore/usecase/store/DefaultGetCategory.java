package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.CategoryResponse;
import com.gitlab.bogdan776.springstore.exception.CategoryNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.CategoryMapper;
import com.gitlab.bogdan776.springstore.repository.store.CategoryRepository;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetCategory implements GetCategoryUseCase {

    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final CategoryMapper categoryMapper;

    @Override
    public CategoryResponse execute(UUID categoryId, Pageable pageable) {
        return categoryRepository.findById(categoryId)
            .map(category -> categoryMapper.map(
                category, productRepository.findAllByCategoryNames(List.of(category.getDisplayName()), pageable)
            )).orElseThrow(() -> new CategoryNotFoundException(categoryId));
    }
}
