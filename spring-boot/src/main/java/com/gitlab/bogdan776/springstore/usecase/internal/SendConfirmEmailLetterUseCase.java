package com.gitlab.bogdan776.springstore.usecase.internal;

import java.util.UUID;

public interface SendConfirmEmailLetterUseCase {

    void execute(UUID userId, String email);
}
