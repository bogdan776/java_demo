package com.gitlab.bogdan776.springstore.filestorage.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.gitlab.bogdan776.springstore.filestorage.FileStorageException;
import com.gitlab.bogdan776.springstore.filestorage.FileStorageService;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKey;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.UUID;

@Log4j2
@Service
@RequiredArgsConstructor
public class AmazonS3FileStorageService implements FileStorageService {

    private final AmazonS3 amazonS3;

    @Value("${aws.s3.bucket}")
    private String bucketName;

    @Override
    public ResourceKey upload(String path, InputStream inputStream) {
        try {
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, path, inputStream, new ObjectMetadata())
                .withCannedAcl(CannedAccessControlList.PublicRead);
            amazonS3.putObject(putObjectRequest);
            return ResourceKey.builder()
                .id(UUID.randomUUID())
                .path(path)
                .url(amazonS3.getUrl(bucketName, path))
                .property(ResourceKey.AWS_BUCKET_NAME, bucketName)
                .build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new FileStorageException(e);
        }
    }

    @Override
    public InputStream download(ResourceKey resourceKey) {
        try {
            String bucket = resourceKey.getProperty(ResourceKey.AWS_BUCKET_NAME);
            S3Object s3Object = amazonS3.getObject(new GetObjectRequest(bucket, resourceKey.getPath()));
            return s3Object.getObjectContent();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new FileStorageException(e);
        }
    }

    @Override
    public void delete(ResourceKey key) {
        try {
            String bucket = key.getProperty(ResourceKey.AWS_BUCKET_NAME);
            amazonS3.deleteObject(bucket, key.getPath());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new FileStorageException(e);
        }
    }

    @Override
    public boolean doesFileExist(ResourceKey key) {
        String bucket = key.getProperty(ResourceKey.AWS_BUCKET_NAME);
        return amazonS3.doesObjectExist(bucket, key.getPath());
    }
}
