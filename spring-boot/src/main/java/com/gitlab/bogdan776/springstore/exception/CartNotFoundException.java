package com.gitlab.bogdan776.springstore.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@Getter
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class CartNotFoundException extends WebException {

    private static final String MESSAGE = "Cart not found";

    @JsonProperty
    private final UUID cartId;

    @JsonProperty
    private UUID userId;

    public CartNotFoundException(UUID cartId) {
        super(MESSAGE);
        this.cartId = cartId;
    }

    public CartNotFoundException(UUID cartId, UUID userId) {
        super(MESSAGE);
        this.cartId = cartId;
        this.userId = userId;
    }
}
