package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.request.OrderRequest;
import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;

import java.util.UUID;

public interface CreateOrderUseCase {

    OrderResponse execute(UUID userId, UUID cartId, OrderRequest request, Boolean force);
}
