package com.gitlab.bogdan776.springstore.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentResponse {

    private UUID id;
    private BigInteger rating;
    private String content;
    private String authorName;
    private LocalDateTime createdAt;
    private List<CommentResponse> answers;
}
