package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.response.ProductShortResponse;
import com.gitlab.bogdan776.springstore.usecase.store.GetAllProductsUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class GetAllProductsController implements StoreController {

    private final GetAllProductsUseCase getAllProducts;

    @GetMapping(value = "/products", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<ProductShortResponse>> getAllProducts(Pageable pageable) {
        return ResponseEntity.ok(getAllProducts.execute(pageable));
    }
}
