package com.gitlab.bogdan776.springstore.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@Getter
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class UserNotFoundException extends WebException {

    private static final String MESSAGE = "User not found";

    @JsonProperty
    private UUID id;
    @JsonProperty
    private String login;

    public UserNotFoundException(UUID id) {
        super(MESSAGE);
        this.id = id;
    }

    public UserNotFoundException(String login) {
        super(MESSAGE);
        this.login = login;
    }
}
