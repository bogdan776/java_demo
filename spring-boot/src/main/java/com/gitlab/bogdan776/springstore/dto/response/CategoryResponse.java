package com.gitlab.bogdan776.springstore.dto.response;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.net.URL;
import java.util.UUID;

@Data
public class CategoryResponse {

    private UUID id;
    private String name;
    private String displayName;
    private String description;
    private URL previewImageUrl;
    private Page<ProductShortResponse> products;
}
