package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.common.constant.CookieNames;
import com.gitlab.bogdan776.springstore.dto.response.ShoppingCartDataResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.AddProductToShoppingCartUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class AddProductToShoppingCartController extends SecurityController implements StoreController {

    private final AddProductToShoppingCartUseCase addProductToShoppingCart;

    @PutMapping(value = "/carts/products/{productId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> addProductToShoppingCart(
        @PathVariable("productId") UUID productId,
        @RequestParam(value = "quantity", required = false, defaultValue = "1") BigInteger quantity,
        @CookieValue(name = CookieNames.SHOPPING_CART_ID, required = false) UUID cartId
    ) {
        ShoppingCartDataResponse dataResponse = addProductToShoppingCart.execute(
            isAnonymous() ? null : getUserId(), productId, quantity, cartId
        );
        return ResponseEntity.ok()
            .header(HttpHeaders.SET_COOKIE, createCookie(dataResponse))
            .build();
    }

    private String createCookie(ShoppingCartDataResponse dataResponse) {
        return ResponseCookie.from(CookieNames.SHOPPING_CART_ID)
            .value(dataResponse.getCartId().toString())
            .maxAge(dataResponse.getExpiration())
            .path("/")
            .secure(true)
            .httpOnly(true)
            .build()
            .toString();
    }
}
