package com.gitlab.bogdan776.springstore.security.filter;

import com.gitlab.bogdan776.springstore.security.JwtAuthenticationProvider;
import jakarta.annotation.Nonnull;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;

@RequiredArgsConstructor
public class JwtTokenFilter extends OncePerRequestFilter {

    private static final String BEARER_PREFIX = "Bearer ";

    private final JwtAuthenticationProvider authenticationProvider;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    @Nonnull HttpServletResponse response,
                                    @Nonnull FilterChain filterChain) throws ServletException, IOException {
        Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION))
            .filter(token -> token.startsWith(BEARER_PREFIX))
            .map(token -> token.substring(BEARER_PREFIX.length()))
            .map(authenticationProvider::validateToken)
            .ifPresent(
                jwtToken -> SecurityContextHolder.getContext().setAuthentication(jwtToken)
            );
        filterChain.doFilter(request, response);
    }
}
