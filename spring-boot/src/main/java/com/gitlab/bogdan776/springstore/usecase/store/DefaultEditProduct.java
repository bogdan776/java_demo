package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.request.ProductRequest;
import com.gitlab.bogdan776.springstore.dto.response.ProductResponse;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.exception.ProductNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.ProductMapper;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultEditProduct implements EditProductUseCase {

    private final ProductRepository repository;
    private final ProductMapper mapper;

    @Override
    @Transactional
    public ProductResponse execute(UUID productId, ProductRequest request) {
        Product product = repository.findById(productId)
            .orElseThrow(() -> new ProductNotFoundException(productId));
        product.setName(request.getName());
        product.setDescription(request.getDescription());
        product.setSpecifications(request.getSpecifications());
        product.setPrice(request.getPrice());
        product.setDiscount(request.getDiscount());
        product.setAvailableQuantity(request.getAvailableQuantity());
        return mapper.map(repository.save(product));
    }
}
