package com.gitlab.bogdan776.springstore.repository.user;

import com.gitlab.bogdan776.springstore.entity.user.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserInfoRepository extends JpaRepository<UserInfo, UUID> {
}
