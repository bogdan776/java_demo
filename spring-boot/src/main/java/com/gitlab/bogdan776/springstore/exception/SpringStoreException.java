package com.gitlab.bogdan776.springstore.exception;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@JsonAutoDetect(
    getterVisibility = JsonAutoDetect.Visibility.NONE,
    isGetterVisibility = JsonAutoDetect.Visibility.NONE,
    setterVisibility = JsonAutoDetect.Visibility.NONE,
    creatorVisibility = JsonAutoDetect.Visibility.NONE,
    fieldVisibility = JsonAutoDetect.Visibility.NONE
)
@ResponseStatus(
    reason = "Something went wrong. Nobody will fix it. Thank you for being with us, all the best!",
    code = HttpStatus.INTERNAL_SERVER_ERROR
)
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class SpringStoreException extends RuntimeException {

    public SpringStoreException() {
        super();
    }

    public SpringStoreException(String message) {
        super(message);
    }

    public SpringStoreException(String message, Throwable cause) {
        super(message, cause);
    }

    public SpringStoreException(Throwable cause) {
        super(cause);
    }
}
