package com.gitlab.bogdan776.springstore.repository.store;

import com.gitlab.bogdan776.springstore.entity.store.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {

    @Query("select product from Product product"
        + " left join OrderedProduct orderedProduct on product.id = orderedProduct.product.id"
        + " left join Order order on order.id = orderedProduct.order.id"
        + " where order.id = :orderId")
    List<Product> findAllByOrderId(@Param("orderId") UUID orderId);

    @Query(value = "select product.* from spring_store_product product"
        + " left outer join spring_store_products_categories categories on categories.product_id = product.id"
        + " left outer join spring_store_category category on categories.category_id = category.id"
        + " where category.display_name in :categoryNames", nativeQuery = true)
    Page<Product> findAllByCategoryNames(@Param("categoryNames") List<String> categoryNames, Pageable pageable);
}
