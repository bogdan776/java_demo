package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.entity.store.Category;
import com.gitlab.bogdan776.springstore.exception.CategoryNotFoundException;
import com.gitlab.bogdan776.springstore.repository.store.CategoryRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultDeleteCategory implements DeleteCategoryUseCase {

    private final CategoryRepository repository;
    private final DeleteCategoryPreviewImageUseCase deleteCategoryPreviewImage;

    @Override
    @Transactional
    public void execute(UUID categoryId) {
        Category category = repository.findById(categoryId)
            .orElseThrow(() -> new CategoryNotFoundException(categoryId));
        if (category.getPreviewImage() != null) {
            deleteCategoryPreviewImage.execute(category.getId());
        }
        repository.delete(category);
    }
}
