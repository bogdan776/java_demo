package com.gitlab.bogdan776.springstore.usecase.internal;

public interface SendPasswordResetLetterUseCase {

    void execute(String email, String generatedPassword, String token);
}
