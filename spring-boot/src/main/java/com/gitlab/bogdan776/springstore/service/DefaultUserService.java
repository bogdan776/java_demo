package com.gitlab.bogdan776.springstore.service;

import com.gitlab.bogdan776.springstore.common.constant.enums.Country;
import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.entity.user.UserInfo;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.repository.user.UserInfoRepository;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

@Service
@RequiredArgsConstructor
public class DefaultUserService implements UserService {

    private final UserRepository userRepository;
    private final UserInfoRepository userInfoRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserProfile create(String username, String email, String rawPassword, String countryCode) {
        UserProfile user = new UserProfile();
        user.setId(UUID.randomUUID());
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(rawPassword));
        user.setCountry(Country.valueOf(countryCode));
        user.setRole(UserRole.USER);
        user.setCreatedAt(TimeUtils.timeNow());
        user.setUserInfo(createDefaultUserInfo(user));
        user.setEmailConfirmed(false);
        userRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    public UserInfo createUserInfo(UserProfile user, Consumer<UserInfo> consumer) {
        UserInfo userInfo = createDefaultUserInfo(user);
        consumer.accept(userInfo);
        return userInfoRepository.save(userInfo);
    }

    @Override
    public Optional<UserProfile> findById(UUID id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<UserProfile> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<UserProfile> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    @Transactional
    public void save(UserProfile userProfile) {
        userRepository.save(userProfile);
    }

    private UserInfo createDefaultUserInfo(UserProfile user) {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(user.getId());
        userInfo.setUser(user);
        return userInfo;
    }
}
