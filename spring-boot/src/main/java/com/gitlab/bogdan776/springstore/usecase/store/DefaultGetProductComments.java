package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.CommentResponse;
import com.gitlab.bogdan776.springstore.mapper.CommentMapper;
import com.gitlab.bogdan776.springstore.repository.store.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetProductComments implements GetProductCommentsUseCase {

    private final CommentRepository repository;
    private final CommentMapper mapper;

    @Override
    public Page<CommentResponse> execute(UUID productId, Pageable pageable) {
        return repository.findAllByProductIdAndRootIsNull(productId, pageable)
            .map(mapper::map);
    }
}
