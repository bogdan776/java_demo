package com.gitlab.bogdan776.springstore.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductQuantity {

    private UUID productId;
    private BigInteger quantity;
}
