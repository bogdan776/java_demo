package com.gitlab.bogdan776.springstore.filestorage;

import com.gitlab.bogdan776.springstore.exception.SpringStoreException;

public class FileStorageException extends SpringStoreException {

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileStorageException(Throwable cause) {
        super(cause);
    }
}
