package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.CategoryShortResponse;
import com.gitlab.bogdan776.springstore.mapper.CategoryMapper;
import com.gitlab.bogdan776.springstore.repository.store.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DefaultGetCategories implements GetCategoriesUseCase {

    private final CategoryRepository repository;
    private final CategoryMapper mapper;

    @Override
    public Page<CategoryShortResponse> execute(Pageable pageable) {
        return repository.findAll(pageable)
            .map(mapper::map);
    }
}
