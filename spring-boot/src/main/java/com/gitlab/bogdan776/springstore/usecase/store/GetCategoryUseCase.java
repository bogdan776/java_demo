package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.CategoryResponse;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface GetCategoryUseCase {

    CategoryResponse execute(UUID categoryId, Pageable pageable);
}
