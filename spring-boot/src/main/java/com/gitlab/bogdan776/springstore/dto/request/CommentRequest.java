package com.gitlab.bogdan776.springstore.dto.request;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.math.BigInteger;

@Data
public class CommentRequest {

    @Min(1)
    @Max(5)
    private BigInteger rating;

    @NotBlank
    @Length(max = 500)
    private String content;
}
