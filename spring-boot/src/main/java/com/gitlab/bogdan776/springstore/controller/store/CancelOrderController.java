package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.CancelOrderUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class CancelOrderController extends SecurityController implements StoreController {

    private final CancelOrderUseCase cancelOrder;

    @DeleteMapping(value = "/orders/{orderId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderResponse> cancelOrder(@PathVariable("orderId") UUID orderId) {
        return ResponseEntity.ok(cancelOrder.execute(getUserId(), orderId));
    }
}
