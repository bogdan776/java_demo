package com.gitlab.bogdan776.springstore.service;

public interface MailService {

    void sendSimpleMailMessage(String recipient, String subject, String content);

    void sendMimeMessage(String recipient, String subject, String content);
}
