package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.usecase.store.DeleteCategoryUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class DeleteCategoryController implements StoreController {

    private final DeleteCategoryUseCase deleteCategory;

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping(value = "/categories/{categoryId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteCategory(@PathVariable("categoryId") UUID categoryId) {
        deleteCategory.execute(categoryId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
