package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.exception.ProductNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.filestorage.FileStorageService;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyJson;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyMapper;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultDeleteProductImages implements DeleteProductImagesUseCase {

    private final ProductRepository repository;
    private final FileStorageService fileStorageService;
    private final ResourceKeyMapper mapper;

    @Override
    @Transactional
    public void execute(UUID productId, List<UUID> imageIds) {
        Product product = repository.findById(productId)
            .orElseThrow(() -> new ProductNotFoundException(productId));
        List<ResourceKeyJson> productImages = product.getImages();
        if (productImages == null || productImages.isEmpty()) {
            throw new WrongInputException("Upload product images before selecting");
        }
        List<ResourceKeyJson> deleted = productImages.stream()
            .filter(resourceKey -> imageIds.contains(resourceKey.getId()))
            .peek(resourceKey -> fileStorageService.delete(mapper.map(resourceKey)))
            .toList();
        productImages.removeAll(deleted);
        if (product.getPreviewImage() != null && imageIds.contains(product.getPreviewImage().getId())) {
            product.setPreviewImage(null);
        }
        repository.save(product);
    }
}
