package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.response.CategoryResponse;
import com.gitlab.bogdan776.springstore.usecase.store.GetCategoryUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class GetCategoryController implements StoreController {

    private final GetCategoryUseCase getCategory;

    @GetMapping(value = "/categories/{categoryId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryResponse> getCategory(@PathVariable("categoryId") UUID categoryId,
                                                        Pageable pageable) {
        return ResponseEntity.ok(getCategory.execute(categoryId, pageable));
    }
}
