package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.ProductResponse;
import com.gitlab.bogdan776.springstore.exception.ProductNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.ProductMapper;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetProduct implements GetProductUseCase {

    private final ProductRepository repository;
    private final ProductMapper mapper;

    @Override
    public ProductResponse execute(UUID productId) {
        return repository.findById(productId)
            .map(mapper::map)
            .orElseThrow(() -> new ProductNotFoundException(productId));
    }
}
