package com.gitlab.bogdan776.springstore.controller.auth;

import com.gitlab.bogdan776.springstore.dto.request.SignUpRequest;
import com.gitlab.bogdan776.springstore.dto.response.UserProfileResponse;
import com.gitlab.bogdan776.springstore.usecase.auth.SignUpUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class SignUpController {

    private final SignUpUseCase signUp;

    @PostMapping(value = "/signup", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserProfileResponse> signUp(@RequestBody @Validated SignUpRequest request) {
        return ResponseEntity.ok(signUp.execute(request));
    }
}
