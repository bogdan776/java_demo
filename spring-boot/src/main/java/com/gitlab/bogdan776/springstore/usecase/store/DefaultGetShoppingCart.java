package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.dto.ProductQuantity;
import com.gitlab.bogdan776.springstore.dto.response.ProductShortResponse;
import com.gitlab.bogdan776.springstore.dto.response.ShoppingCartResponse;
import com.gitlab.bogdan776.springstore.entity.store.ShoppingCart;
import com.gitlab.bogdan776.springstore.exception.CartNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.ProductMapper;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import com.gitlab.bogdan776.springstore.repository.store.ShoppingCartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DefaultGetShoppingCart implements GetShoppingCartUseCase {

    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Override
    public ShoppingCartResponse execute(UUID userId, UUID cartId) {
        ShoppingCart shoppingCart = Optional.ofNullable(cartId)
            .map(shoppingCartRepository::findById)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .or(() -> shoppingCartRepository.findByUserId(userId))
            .orElseThrow(() -> new CartNotFoundException(cartId, userId));

        if (shoppingCart.getProductQuantities() == null) {
            return ShoppingCartResponse.builder()
                .withId(shoppingCart.getId())
                .withTotalPrice(BigDecimal.ZERO)
                .build();
        }
        Map<UUID, BigInteger> productQuantityMap = shoppingCart.getProductQuantities().stream()
            .collect(Collectors.toMap(ProductQuantity::getProductId, ProductQuantity::getQuantity));
        List<ProductShortResponse> productResponses = productRepository.findAllById(productQuantityMap.keySet()).stream()
            .map(product -> {
                ProductShortResponse response = productMapper.mapToShortResponse(product);
                response.setQuantity(productQuantityMap.get(product.getId()));
                response.setTotalPrice(productMapper.calculateTotalPrice(
                    response.getQuantity(), response.getFinalPrice()
                ));
                return response;
            }).toList();
        BigDecimal totalPrice = productResponses.stream()
            .map(ProductShortResponse::getTotalPrice)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
        return ShoppingCartResponse.builder()
            .withId(shoppingCart.getId())
            .withProducts(productResponses)
            .withTotalPrice(totalPrice)
            .build();
    }
}
