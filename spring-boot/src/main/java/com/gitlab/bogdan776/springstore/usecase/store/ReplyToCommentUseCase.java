package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.request.CommentRequest;
import com.gitlab.bogdan776.springstore.dto.response.CommentResponse;

import java.util.UUID;

public interface ReplyToCommentUseCase {

    CommentResponse execute(UUID userId, UUID productId, UUID commentId, CommentRequest request);
}
