package com.gitlab.bogdan776.springstore.mapper;

import com.gitlab.bogdan776.springstore.dto.response.UserInfoResponse;
import com.gitlab.bogdan776.springstore.dto.response.UserProfileResponse;
import com.gitlab.bogdan776.springstore.entity.user.UserInfo;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface UserMapper {

    @Named("mapUser")
    @Mapping(target = "avatarUrl", source = "avatarKey.url")
    UserProfileResponse mapUser(UserProfile userProfile);

    @Mapping(target = "avatarUrl", source = "avatarKey.url")
    @Mapping(target = "email", ignore = true)
    @Mapping(target = "userInfo.address", ignore = true)
    @Mapping(target = "userInfo.phoneNumber", ignore = true)
    UserProfileResponse mapUserToShortResponse(UserProfile userProfile);

    UserInfoResponse mapUserInfo(UserInfo userInfo);
}
