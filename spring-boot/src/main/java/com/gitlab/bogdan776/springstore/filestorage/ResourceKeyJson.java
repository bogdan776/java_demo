package com.gitlab.bogdan776.springstore.filestorage;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.net.URL;
import java.util.Map;
import java.util.UUID;

@Getter
@Setter
@ToString
public class ResourceKeyJson {

    private UUID id;
    private URL url;
    private String path;
    private Map<String, String> properties;
}
