package com.gitlab.bogdan776.springstore.repository.store;

import com.gitlab.bogdan776.springstore.entity.store.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CategoryRepository extends JpaRepository<Category, UUID> {
}
