package com.gitlab.bogdan776.springstore.usecase.store;

import java.util.List;
import java.util.UUID;

public interface DeleteProductImagesUseCase {

    void execute(UUID productId, List<UUID> imageIds);
}
