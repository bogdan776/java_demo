package com.gitlab.bogdan776.springstore.scheduler;

import java.time.LocalDateTime;
import java.util.UUID;

public interface SchedulerService {

    UUID scheduleCartDeletion(UUID cartId, LocalDateTime dateTime);

    void unscheduleCartDeletion(UUID jobIdentity);

    UUID scheduleOrderCancellation(UUID orderId, LocalDateTime dateTime);

    void unscheduleOrderCancellation(UUID jobIdentity);

    boolean checkExists(UUID jobIdentity, String group);

    void unscheduleJob(UUID jobIdentity, String group);
}
