package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;

import java.util.UUID;

public interface SelectProductPreviewImageUseCase {

    ResourceKeyResponse execute(UUID productId, UUID imageId);
}
