package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.ProductShortResponse;
import com.gitlab.bogdan776.springstore.mapper.ProductMapper;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DefaultGetProductsByCategories implements GetProductsByCategoriesUseCase {

    private final ProductRepository repository;
    private final ProductMapper mapper;

    @Override
    public Page<ProductShortResponse> execute(List<String> categoryNames, Pageable pageable) {
        return repository.findAllByCategoryNames(categoryNames, pageable)
            .map(mapper::mapToShortResponse);
    }
}
