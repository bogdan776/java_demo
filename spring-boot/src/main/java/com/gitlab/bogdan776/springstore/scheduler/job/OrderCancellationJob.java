package com.gitlab.bogdan776.springstore.scheduler.job;

import com.gitlab.bogdan776.springstore.usecase.store.CancelOrderUseCase;
import jakarta.transaction.Transactional;
import lombok.Setter;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.UUID;

import static com.gitlab.bogdan776.springstore.common.constant.Constants.ADMIN_ID;
import static com.gitlab.bogdan776.springstore.common.constant.Constants.QUARTZ_JOB_DATA_UUID;

@DisallowConcurrentExecution
public class OrderCancellationJob implements Job {

    public static final String JOB_GROUP = "order-group";

    @Setter(onMethod_ = @Autowired)
    public CancelOrderUseCase cancelOrder;

    @Override
    @Transactional
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Optional.ofNullable(context.getMergedJobDataMap())
            .map(jobDataMap -> jobDataMap.getString(QUARTZ_JOB_DATA_UUID))
            .map(UUID::fromString)
            .ifPresent(id -> cancelOrder.execute(ADMIN_ID, id));
    }
}
