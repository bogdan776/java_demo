package com.gitlab.bogdan776.springstore.usecase.store;

import java.math.BigInteger;
import java.util.UUID;

public interface DeleteProductFromShoppingCartUseCase {

    void execute(UUID userId, UUID cartId, UUID productId, Boolean deleteCompletely, BigInteger quantity);
}
