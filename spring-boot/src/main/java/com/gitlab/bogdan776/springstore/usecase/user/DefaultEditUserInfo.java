package com.gitlab.bogdan776.springstore.usecase.user;

import com.gitlab.bogdan776.springstore.dto.request.UserInfoRequest;
import com.gitlab.bogdan776.springstore.dto.response.UserInfoResponse;
import com.gitlab.bogdan776.springstore.entity.user.UserInfo;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.UserMapper;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultEditUserInfo implements EditUserInfoUseCase {

    private final UserRepository repository;
    private final UserMapper mapper;

    @Override
    @Transactional
    public UserInfoResponse execute(UUID userId, UserInfoRequest request) {
        UserProfile user = repository.findById(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));
        UserInfo userInfo = user.getUserInfo();
        userInfo.setFullName(requireNonNullOrUseOldValue(request.getFullName(), userInfo.getFullName()));
        userInfo.setAddress(requireNonNullOrUseOldValue(request.getAddress(), userInfo.getAddress()));
        userInfo.setPhoneNumber(requireNonNullOrUseOldValue(request.getPhoneNumber(), userInfo.getPhoneNumber()));
        userInfo.setIntroduction(requireNonNullOrUseOldValue(request.getIntroduction(), userInfo.getIntroduction()));
        repository.save(user);
        return mapper.mapUserInfo(userInfo);
    }

    public String requireNonNullOrUseOldValue(String newValue, String oldValue) {
        return (newValue != null && !newValue.isBlank()) ? newValue : oldValue;
    }
}
