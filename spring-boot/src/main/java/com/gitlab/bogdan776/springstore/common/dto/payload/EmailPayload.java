package com.gitlab.bogdan776.springstore.common.dto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailPayload {

    public UUID userId;
    private String email;
}
