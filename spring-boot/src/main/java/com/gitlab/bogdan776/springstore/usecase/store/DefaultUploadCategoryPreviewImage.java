package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.entity.store.Category;
import com.gitlab.bogdan776.springstore.exception.CategoryNotFoundException;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyMapper;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;
import com.gitlab.bogdan776.springstore.filestorage.UploadFileUseCase;
import com.gitlab.bogdan776.springstore.repository.store.CategoryRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultUploadCategoryPreviewImage implements UploadCategoryPreviewImageUseCase {

    private static final String CATEGORY_IMAGES_DIR_PATH = "categories/images";

    private final CategoryRepository repository;
    private final UploadFileUseCase uploadFile;
    private final ResourceKeyMapper mapper;

    @Override
    @Transactional
    public ResourceKeyResponse execute(UUID categoryId, MultipartFile image) {
        Category category = repository.findById(categoryId)
            .orElseThrow(() -> new CategoryNotFoundException(categoryId));
        uploadFile.execute(image, CATEGORY_IMAGES_DIR_PATH, category.getId().toString(), category::setPreviewImage);
        repository.save(category);
        return mapper.mapToResponse(category.getPreviewImage());
    }
}
