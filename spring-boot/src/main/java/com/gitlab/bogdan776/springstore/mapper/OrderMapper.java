package com.gitlab.bogdan776.springstore.mapper;

import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.dto.response.ProductShortResponse;
import com.gitlab.bogdan776.springstore.entity.store.Order;
import com.gitlab.bogdan776.springstore.entity.store.OrderedProduct;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyJson;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.math.BigDecimal;
import java.util.Optional;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING, uses = UserMapper.class)
public interface OrderMapper {

    @Mapping(target = "user", ignore = true)
    @Mapping(target = "products", source = "orderedProducts")
    OrderResponse map(Order order);

    @Mapping(target = "products", source = "orderedProducts")
    @Mapping(target = "user", qualifiedByName = "mapUser")
    OrderResponse mapWithUser(Order order);

    default ProductShortResponse mapProduct(OrderedProduct orderedProduct) {
        ProductShortResponse response = new ProductShortResponse();
        Product product = orderedProduct.getProduct();
        response.setId(product.getId());
        response.setName(product.getName());
        Optional.ofNullable(product.getPreviewImage())
            .map(ResourceKeyJson::getUrl)
            .ifPresent(response::setPreviewImageUrl);
        response.setFinalPrice(orderedProduct.getUnitPrice());
        response.setQuantity(orderedProduct.getQuantity());
        response.setTotalPrice(orderedProduct.getUnitPrice().multiply(new BigDecimal(orderedProduct.getQuantity())));
        return response;
    }
}
