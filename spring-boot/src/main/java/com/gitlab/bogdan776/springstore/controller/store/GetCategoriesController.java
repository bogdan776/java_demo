package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.response.CategoryShortResponse;
import com.gitlab.bogdan776.springstore.usecase.store.GetCategoriesUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class GetCategoriesController implements StoreController {

    private final GetCategoriesUseCase getCategories;

    @GetMapping(value = "/categories", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<CategoryShortResponse>> getCategories(Pageable pageable) {
        return ResponseEntity.ok(getCategories.execute(pageable));
    }
}
