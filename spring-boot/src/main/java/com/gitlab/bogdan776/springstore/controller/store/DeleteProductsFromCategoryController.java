package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.usecase.store.DeleteProductsFromCategoryUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class DeleteProductsFromCategoryController implements StoreController {

    private final DeleteProductsFromCategoryUseCase deleteProductsFromCategory;

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping(value = "/categories/{categoryId}/products/{productIds}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteProductsFromCategory(@PathVariable("categoryId") UUID categoryId,
                                                             @PathVariable("productIds") List<UUID> productIds) {
        deleteProductsFromCategory.execute(categoryId, productIds);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
