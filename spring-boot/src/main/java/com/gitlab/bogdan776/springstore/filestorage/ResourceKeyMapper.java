package com.gitlab.bogdan776.springstore.filestorage;

import org.mapstruct.Mapper;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface ResourceKeyMapper {

    ResourceKeyJson map(ResourceKey resourceKey);

    ResourceKey map(ResourceKeyJson resourceKeyJson);

    ResourceKeyResponse mapToResponse(ResourceKeyJson resourceKeyJson);
}
