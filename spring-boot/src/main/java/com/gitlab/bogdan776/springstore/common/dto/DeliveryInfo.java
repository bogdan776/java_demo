package com.gitlab.bogdan776.springstore.common.dto;

import com.gitlab.bogdan776.springstore.common.constant.enums.Country;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryInfo {

    private String fullName;
    private Country country;
    private String phoneNumber;
    private String email;
    private String address;
    private String comment;
}
