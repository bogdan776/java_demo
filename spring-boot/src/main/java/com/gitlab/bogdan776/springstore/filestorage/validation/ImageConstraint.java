package com.gitlab.bogdan776.springstore.filestorage.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.springframework.http.MediaType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = ImageConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ImageConstraint {

    long maxSize() default 2 * 1024 * 1024;

    String[] mediaTypes() default {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_GIF_VALUE, MediaType.IMAGE_PNG_VALUE};

    String message() default "File size is too large or unsupported media type";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
