package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.request.CategoryRequest;
import com.gitlab.bogdan776.springstore.dto.response.CategoryShortResponse;
import com.gitlab.bogdan776.springstore.entity.store.Category;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.mapper.CategoryMapper;
import com.gitlab.bogdan776.springstore.repository.store.CategoryRepository;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DefaultCreateCategory implements CreateCategoryUseCase {

    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final CategoryMapper mapper;

    @Override
    @Transactional
    public CategoryShortResponse execute(CategoryRequest request) {
        Category category = mapper.mapRequest(request);
        List<Product> products = productRepository.findAllById(request.getProductsIds());
        category.setProducts(products);
        return mapper.map(categoryRepository.save(category));
    }
}
