package com.gitlab.bogdan776.springstore.controller.user;

import com.gitlab.bogdan776.springstore.dto.response.UserProfileResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.user.GetUserProfileUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class GetUserProfileController extends SecurityController implements UserController {

    private final GetUserProfileUseCase getUserProfile;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserProfileResponse> getCurrentUser() {
        return ResponseEntity.ok(getUserProfile.execute(getUserId(), getUserId()));
    }

    @GetMapping(value = "/{userId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserProfileResponse> getUser(@PathVariable("userId") UUID userId) {
        return ResponseEntity.ok(getUserProfile.execute(getUserId(), userId));
    }
}
