package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.exception.ProductNotFoundException;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyJson;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyMapper;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;
import com.gitlab.bogdan776.springstore.filestorage.UploadFileUseCase;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DefaultUploadProductImages implements UploadProductImagesUseCase {

    private static final String PRODUCT_IMAGES_DIR_PATH = "products/images";

    private final ProductRepository repository;
    private final UploadFileUseCase uploadFile;
    private final ResourceKeyMapper mapper;

    @Override
    @Transactional
    public List<ResourceKeyResponse> execute(UUID productId, List<MultipartFile> images) {
        Product product = repository.findById(productId)
            .orElseThrow(() -> new ProductNotFoundException(productId));
        List<ResourceKeyJson> productImages = product.getImages() == null ? new ArrayList<>() : product.getImages();
        images.forEach(image -> uploadFile.execute(
            image, PRODUCT_IMAGES_DIR_PATH, UUID.randomUUID().toString(), productImages::add
        ));
        product.setImages(productImages);
        repository.save(product);
        return productImages.stream()
            .map(mapper::mapToResponse)
            .collect(Collectors.toList());
    }
}
