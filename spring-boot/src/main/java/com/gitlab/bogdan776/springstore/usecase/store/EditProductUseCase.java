package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.request.ProductRequest;
import com.gitlab.bogdan776.springstore.dto.response.ProductResponse;

import java.util.UUID;

public interface EditProductUseCase {

    ProductResponse execute(UUID productId, ProductRequest request);
}
