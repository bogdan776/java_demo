package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.request.OrderRequest;
import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.CreateOrderUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class CreateOrderController extends SecurityController implements StoreController {

    private final CreateOrderUseCase createOrder;

    @PostMapping(value = "/orders", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderResponse> createOrderUseCookieValue(
        @RequestBody OrderRequest request,
        @RequestParam(value = "force", required = false, defaultValue = "false") Boolean force,
        @CookieValue(name = "cart-id", required = false) UUID cartId
    ) {
        return ResponseEntity.ok()
            .header(HttpHeaders.SET_COOKIE, deleteCookie())
            .body(createOrder.execute(getUserId(), cartId, request, force));
    }

    private String deleteCookie() {
        return ResponseCookie.from("cart-id")
            .maxAge(0)
            .path("/")
            .build()
            .toString();
    }
}
