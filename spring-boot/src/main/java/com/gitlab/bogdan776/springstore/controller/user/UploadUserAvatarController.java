package com.gitlab.bogdan776.springstore.controller.user;

import com.gitlab.bogdan776.springstore.filestorage.validation.ImageConstraint;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.user.UploadUserAvatarUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
@RequiredArgsConstructor
public class UploadUserAvatarController extends SecurityController implements UserController {

    private final UploadUserAvatarUseCase uploadUserAvatar;

    @PostMapping(value = "/avatars", consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<URI> uploadUserAvatar(@RequestParam("avatar") @ImageConstraint MultipartFile avatar) {
        return ResponseEntity.ok(uploadUserAvatar.execute(getUserId(), avatar));
    }
}
