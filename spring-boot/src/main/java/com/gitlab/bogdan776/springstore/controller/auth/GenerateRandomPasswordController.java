package com.gitlab.bogdan776.springstore.controller.auth;

import com.gitlab.bogdan776.springstore.controller.PublicController;
import com.gitlab.bogdan776.springstore.dto.request.ResetPasswordRequest;
import com.gitlab.bogdan776.springstore.usecase.auth.GenerateRandomPasswordUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@PreAuthorize("isAnonymous()")
public class GenerateRandomPasswordController implements PublicController {

    private final GenerateRandomPasswordUseCase generateRandomPassword;

    @PutMapping(value = "/passwords", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> generateRandomPassword(@RequestBody @Validated ResetPasswordRequest request) {
        generateRandomPassword.execute(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
