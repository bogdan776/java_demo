package com.gitlab.bogdan776.springstore.security;

import java.time.LocalDateTime;

public interface JwtService {

    <T> String createToken(T payload);

    <T> String createToken(T payload, LocalDateTime expirationDate);

    <R> R parseToken(String token, Class<R> klass);
}
