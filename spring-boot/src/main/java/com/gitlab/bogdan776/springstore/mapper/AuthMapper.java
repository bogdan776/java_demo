package com.gitlab.bogdan776.springstore.mapper;

import com.gitlab.bogdan776.springstore.common.dto.payload.UserPayload;
import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.dto.response.JwtResponse;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface AuthMapper {

    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "username", source = "user.username")
    @Mapping(target = "role", source = "user.role")
    JwtResponse map(UserProfile user, String token, LocalDateTime expiration);

    UserPayload userPayload(UserProfile user);

    default Long mapLocalDateTime(LocalDateTime dateTime) {
        return TimeUtils.localDateTimeToMilli(dateTime);
    }
}
