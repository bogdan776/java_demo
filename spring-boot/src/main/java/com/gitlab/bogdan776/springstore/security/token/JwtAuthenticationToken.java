package com.gitlab.bogdan776.springstore.security.token;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;

import java.io.Serial;
import java.util.Collections;

public class JwtAuthenticationToken extends AbstractAuthenticationToken {

    @Serial
    private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

    private final transient UserDetails userDetails;
    private transient UserCredentials userCredentials;

    public JwtAuthenticationToken(UserDetails userDetails, GrantedAuthority authority) {
        super(Collections.singleton(authority));
        this.eraseCredentials();
        this.userDetails = userDetails;
        super.setAuthenticated(true);
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        if (authenticated) {
            throw new IllegalArgumentException("Cannot set authenticated. Use constructor which takes GrantedAuthority");
        }
        super.setAuthenticated(false);
    }

    @Override
    public String getName() {
        return userDetails.getUsername();
    }

    @Override
    public Object getCredentials() {
        return userCredentials;
    }

    @Override
    public Object getPrincipal() {
        return userDetails;
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        userCredentials = null;
    }
}
