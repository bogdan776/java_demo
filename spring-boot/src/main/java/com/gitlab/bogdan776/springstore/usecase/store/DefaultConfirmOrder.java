package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.constant.enums.OrderStatus;
import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.exception.OrderNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.OrderMapper;
import com.gitlab.bogdan776.springstore.repository.store.OrderRepository;
import com.gitlab.bogdan776.springstore.scheduler.SchedulerService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultConfirmOrder implements ConfirmOrderUseCase {

    private final OrderRepository repository;
    private final OrderMapper mapper;

    private final SchedulerService schedulerService;

    @Override
    @Transactional
    public OrderResponse execute(UUID userId, UUID orderId) {
        return repository.findByIdAndUserId(orderId, userId)
            .filter(order -> order.getStatus() == OrderStatus.RECEIVED)
            .map(order -> {
                order.setStatus(OrderStatus.PENDING);
                schedulerService.unscheduleOrderCancellation(order.getOrderCancellationJob());
                repository.save(order);
                return order;
            })
            .map(mapper::map)
            .orElseThrow(() -> new OrderNotFoundException(orderId));
    }
}
