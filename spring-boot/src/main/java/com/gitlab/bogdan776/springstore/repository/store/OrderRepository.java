package com.gitlab.bogdan776.springstore.repository.store;

import com.gitlab.bogdan776.springstore.entity.store.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {

    Optional<Order> findByIdAndUserId(UUID id, UUID userId);

    Page<Order> findAllByUserId(UUID userId, Pageable pageable);
}
