package com.gitlab.bogdan776.springstore.dto.request;

import com.fasterxml.jackson.annotation.JsonAlias;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class CategoryRequest {

    @NotBlank
    private String name;

    @NotBlank
    @JsonAlias("display_name")
    private String displayName;

    @NotBlank
    private String description;

    @JsonAlias("products_ids")
    private List<UUID> productsIds = new ArrayList<>();
}
