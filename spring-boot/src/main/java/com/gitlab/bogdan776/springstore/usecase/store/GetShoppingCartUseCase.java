package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.ShoppingCartResponse;

import java.util.UUID;

public interface GetShoppingCartUseCase {

    ShoppingCartResponse execute(UUID userId, UUID cartId);
}
