package com.gitlab.bogdan776.springstore.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductShortResponse {

    private UUID id;
    private String name;
    private URL previewImageUrl;
    private BigDecimal price;
    private BigDecimal discount;
    private BigDecimal finalPrice;
    private BigInteger availableQuantity;
    private BigInteger rating;
    /*
    Shopping cart data
     */
    private BigInteger quantity;
    private BigDecimal totalPrice;
}
