package com.gitlab.bogdan776.springstore.entity.store;

import com.gitlab.bogdan776.springstore.common.dto.ProductQuantity;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import jakarta.annotation.Nullable;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@Table(name = "spring_store_shopping_cart")
@EqualsAndHashCode(of = "id")
public class ShoppingCart {

    @Id
    private UUID id;

    @Nullable
    @JdbcTypeCode(SqlTypes.JSON)
    private List<ProductQuantity> productQuantities;

    @Nullable
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    private UserProfile user;

    @Nullable
    private UUID deletingCartJob;

    @NotNull
    private LocalDateTime createdAt;
}
