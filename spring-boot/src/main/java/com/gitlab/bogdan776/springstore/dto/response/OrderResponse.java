package com.gitlab.bogdan776.springstore.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gitlab.bogdan776.springstore.common.constant.enums.OrderStatus;
import com.gitlab.bogdan776.springstore.common.dto.DeliveryInfo;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderResponse {

    private UUID id;
    private UserProfileResponse user;
    private OrderStatus status;
    private List<ProductShortResponse> products;
    private BigDecimal price;
    private DeliveryInfo deliveryInfo;
    private LocalDateTime createdAt;
}
