package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;
import com.gitlab.bogdan776.springstore.filestorage.validation.ImageConstraint;
import com.gitlab.bogdan776.springstore.usecase.store.UploadCategoryPreviewImageUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
@RequiredArgsConstructor
public class UploadCategoryPreviewImageController implements StoreController {

    private final UploadCategoryPreviewImageUseCase uploadCategoryPreviewImage;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping(
        value = "/categories/{categoryId}/preview",
        consumes = MULTIPART_FORM_DATA_VALUE,
        produces = APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ResourceKeyResponse> uploadPreviewImage(@PathVariable("categoryId") UUID categoryId,
                                                                  @RequestParam("image") @ImageConstraint MultipartFile image) {
        return ResponseEntity.ok(uploadCategoryPreviewImage.execute(categoryId, image));
    }
}
