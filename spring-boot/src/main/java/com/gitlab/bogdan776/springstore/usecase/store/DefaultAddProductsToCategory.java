package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.entity.store.Category;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.exception.CategoryNotFoundException;
import com.gitlab.bogdan776.springstore.repository.store.CategoryRepository;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultAddProductsToCategory implements AddProductsToCategoryUseCase {

    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;

    @Override
    @Transactional
    public void execute(UUID categoryId, List<UUID> productIds) {
        Category category = categoryRepository.findById(categoryId)
            .orElseThrow(() -> new CategoryNotFoundException(categoryId));
        List<Product> products = productRepository.findAllById(productIds).stream()
            .peek(product -> {
                List<Category> categories = getCategoryListOrCreateNew(product);
                categories.add(category);
                product.setCategories(categories);
            }).toList();
        productRepository.saveAll(products);
    }

    private List<Category> getCategoryListOrCreateNew(Product product) {
        return product.getCategories() == null ? new ArrayList<>() : product.getCategories();
    }
}
