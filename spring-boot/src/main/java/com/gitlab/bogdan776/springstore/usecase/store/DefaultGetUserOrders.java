package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.mapper.OrderMapper;
import com.gitlab.bogdan776.springstore.repository.store.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetUserOrders implements GetUserOrdersUseCase {

    private final OrderRepository repository;
    private final OrderMapper mapper;

    @Override
    public Page<OrderResponse> execute(UUID userId, Pageable pageable) {
        return repository.findAllByUserId(userId, pageable)
            .map(mapper::map);
    }
}
