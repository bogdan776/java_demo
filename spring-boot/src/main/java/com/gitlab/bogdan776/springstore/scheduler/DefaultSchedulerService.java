package com.gitlab.bogdan776.springstore.scheduler;

import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.scheduler.job.CartDeletionJob;
import com.gitlab.bogdan776.springstore.scheduler.job.OrderCancellationJob;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

import static com.gitlab.bogdan776.springstore.common.constant.Constants.QUARTZ_JOB_DATA_UUID;

@Log4j2
@Service
@RequiredArgsConstructor
public class DefaultSchedulerService implements SchedulerService {

    private final Scheduler scheduler;

    @Override
    public boolean checkExists(UUID jobIdentity, String group) {
        try {
            return scheduler.checkExists(TriggerKey.triggerKey(jobIdentity.toString(), group));
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
            throw new QuartzException(e.getMessage(), e);
        }
    }

    @Override
    @Transactional
    public void unscheduleJob(UUID jobIdentity, String group) {
        try {
            scheduler.unscheduleJob(TriggerKey.triggerKey(jobIdentity.toString(), group));
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
            throw new QuartzException(e.getMessage(), e);
        }
    }

    @Override
    @Transactional
    public UUID scheduleCartDeletion(UUID cartId, LocalDateTime dateTime) {
        return scheduleJob(
            CartDeletionJob.class,
            CartDeletionJob.JOB_GROUP,
            singleValueJobDataMap(cartId),
            defaultTriggerBuilder(dateTime)
        );
    }

    @Override
    @Transactional
    public void unscheduleCartDeletion(UUID jobIdentity) {
        if (jobIdentity == null) {
            return;
        }
        if (checkExists(jobIdentity, CartDeletionJob.JOB_GROUP)) {
            unscheduleJob(jobIdentity, CartDeletionJob.JOB_GROUP);
        }
    }

    @Override
    @Transactional
    public UUID scheduleOrderCancellation(UUID orderId, LocalDateTime dateTime) {
        return scheduleJob(
            OrderCancellationJob.class,
            OrderCancellationJob.JOB_GROUP,
            singleValueJobDataMap(orderId),
            defaultTriggerBuilder(dateTime)
        );
    }

    @Override
    @Transactional
    public void unscheduleOrderCancellation(UUID jobIdentity) {
        if (jobIdentity == null) {
            return;
        }
        if (checkExists(jobIdentity, OrderCancellationJob.JOB_GROUP)) {
            unscheduleJob(jobIdentity, OrderCancellationJob.JOB_GROUP);
        }
    }

    private <T extends Trigger> UUID scheduleJob(Class<? extends Job> jobClass, String group,
                                                 JobDataMap jobDataMap, TriggerBuilder<T> triggerBuilder) {
        UUID scheduleJobUuid = UUID.randomUUID();
        JobDetail jobDetail = JobBuilder.newJob(jobClass)
            .withIdentity(scheduleJobUuid.toString(), group)
            .usingJobData(jobDataMap)
            .requestRecovery(true)
            .build();
        Trigger trigger = triggerBuilder
            .withIdentity(scheduleJobUuid.toString(), group)
            .build();
        try {
            scheduler.scheduleJob(jobDetail, trigger);
            return scheduleJobUuid;
        } catch (SchedulerException e) {
            log.error(e.getMessage(), e);
            throw new QuartzException(e.getMessage(), e);
        }
    }

    private JobDataMap singleValueJobDataMap(UUID value) {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(QUARTZ_JOB_DATA_UUID, value.toString());
        return jobDataMap;
    }

    private TriggerBuilder<Trigger> defaultTriggerBuilder(LocalDateTime dateTime) {
        return TriggerBuilder.newTrigger()
            .startAt(TimeUtils.localDateTimeToDate(dateTime));
    }
}
