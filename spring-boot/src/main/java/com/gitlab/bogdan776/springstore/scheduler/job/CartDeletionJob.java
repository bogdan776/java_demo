package com.gitlab.bogdan776.springstore.scheduler.job;

import com.gitlab.bogdan776.springstore.common.constant.Constants;
import com.gitlab.bogdan776.springstore.repository.store.ShoppingCartRepository;
import jakarta.transaction.Transactional;
import lombok.Setter;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.UUID;

@DisallowConcurrentExecution
public class CartDeletionJob implements Job {

    public static final String JOB_GROUP = "cart-group";

    @Setter(onMethod_ = @Autowired)
    public ShoppingCartRepository repository;

    @Override
    @Transactional
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Optional.ofNullable(context.getMergedJobDataMap())
            .map(jobDataMap -> jobDataMap.getString(Constants.QUARTZ_JOB_DATA_UUID))
            .map(UUID::fromString)
            .ifPresent(id -> repository.deleteById(id));
    }
}
