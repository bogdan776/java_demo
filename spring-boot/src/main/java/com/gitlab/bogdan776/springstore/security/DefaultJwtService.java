package com.gitlab.bogdan776.springstore.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.jackson.io.JacksonDeserializer;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

import static com.gitlab.bogdan776.springstore.common.utils.TimeUtils.localDateTimeToDate;

@Service
public class DefaultJwtService implements JwtService {

    private static final String PAYLOAD_FIELD = "payload";

    @Value("${security.jwt.token.secret-key}")
    private String secretKey;

    @Override
    public <T> String createToken(T payload) {
        Claims claims = Jwts.claims();
        claims.put(PAYLOAD_FIELD, payload);
        return Jwts.builder()
            .setClaims(claims)
            .signWith(Keys.hmacShaKeyFor(secretKey.getBytes()), SignatureAlgorithm.HS256)
            .compact();
    }

    @Override
    public <T> String createToken(T payload, LocalDateTime expirationDate) {
        Claims claims = Jwts.claims();
        claims.put(PAYLOAD_FIELD, payload);
        return Jwts.builder()
            .setClaims(claims)
            .setIssuedAt(new Date())
            .setExpiration(localDateTimeToDate(expirationDate))
            .signWith(Keys.hmacShaKeyFor(secretKey.getBytes()), SignatureAlgorithm.HS256)
            .compact();
    }

    @Override
    public <R> R parseToken(String token, Class<R> klass) {
        return Jwts.parserBuilder()
            .setSigningKey(Keys.hmacShaKeyFor(secretKey.getBytes()))
            .deserializeJsonWith(new JacksonDeserializer<>(Map.of(PAYLOAD_FIELD, klass)))
            .build()
            .parseClaimsJws(token)
            .getBody()
            .get(PAYLOAD_FIELD, klass);
    }
}
