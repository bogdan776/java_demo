package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface GetUserOrdersUseCase {

    Page<OrderResponse> execute(UUID userId, Pageable pageable);
}
