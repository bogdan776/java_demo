package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.common.constant.CookieNames;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.DeleteShoppingCartUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class DeleteShoppingCartController extends SecurityController implements StoreController {

    private final DeleteShoppingCartUseCase deleteShoppingCart;

    @DeleteMapping(value = "/carts", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteShoppingCart(
        @CookieValue(value = CookieNames.SHOPPING_CART_ID, required = false) UUID cartId
    ) {
        deleteShoppingCart.execute(isAnonymous() ? null : getUserId(), cartId);
        return ResponseEntity.ok()
            .header(HttpHeaders.SET_COOKIE, deleteCookie())
            .build();
    }

    private String deleteCookie() {
        return ResponseCookie.from(CookieNames.SHOPPING_CART_ID)
            .maxAge(0)
            .path("/")
            .build()
            .toString();
    }
}
