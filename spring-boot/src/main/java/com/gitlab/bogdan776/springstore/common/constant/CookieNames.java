package com.gitlab.bogdan776.springstore.common.constant;

public final class CookieNames {

    public static final String SHOPPING_CART_ID = "shopping-cart-id";

    private CookieNames() {
    }
}
