package com.gitlab.bogdan776.springstore.usecase.user;

import com.gitlab.bogdan776.springstore.dto.response.UserProfileResponse;

import java.util.UUID;

public interface GetUserProfileUseCase {

    UserProfileResponse execute(UUID currentUserId, UUID targetUserId);
}
