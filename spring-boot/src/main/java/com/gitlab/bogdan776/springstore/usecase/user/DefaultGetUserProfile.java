package com.gitlab.bogdan776.springstore.usecase.user;

import com.gitlab.bogdan776.springstore.dto.response.UserProfileResponse;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.UserMapper;
import com.gitlab.bogdan776.springstore.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetUserProfile implements GetUserProfileUseCase {

    private final UserService userService;
    private final UserMapper userMapper;

    @Override
    public UserProfileResponse execute(UUID currentUserId, UUID targetUserId) {
        if (currentUserId.equals(targetUserId)) {
            return userService.findById(targetUserId)
                .map(userMapper::mapUser)
                .orElseThrow(() -> new UserNotFoundException(targetUserId));
        }
        return userService.findById(targetUserId)
            .map(userMapper::mapUserToShortResponse)
            .orElseThrow(() -> new UserNotFoundException(targetUserId));
    }
}
