package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.dto.ProductQuantity;
import com.gitlab.bogdan776.springstore.entity.store.ShoppingCart;
import com.gitlab.bogdan776.springstore.exception.CartNotFoundException;
import com.gitlab.bogdan776.springstore.exception.ProductNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.repository.store.ShoppingCartRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultDeleteProductFromShoppingCart implements DeleteProductFromShoppingCartUseCase {

    private final ShoppingCartRepository shoppingCartRepository;

    @Override
    @Transactional
    public void execute(UUID userId, UUID cartId, UUID productId, Boolean deleteCompletely, BigInteger quantity) {
        ShoppingCart shoppingCart = Optional.ofNullable(cartId)
            .map(shoppingCartRepository::findById)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .or(() -> shoppingCartRepository.findByUserId(userId))
            .orElseThrow(() -> new CartNotFoundException(cartId, userId));

        List<ProductQuantity> productQuantities = shoppingCart.getProductQuantities();
        if (productQuantities == null) {
            throw new WrongInputException("Cannot find products in cart");
        }
        ProductQuantity productQuantity = productQuantities.stream()
            .filter(pQuantity -> productId.equals(pQuantity.getProductId()))
            .findFirst()
            .orElseThrow(() -> new ProductNotFoundException(productId));

        int comparison = productQuantity.getQuantity().compareTo(quantity);
        if (comparison == 0 || comparison < 0 || deleteCompletely) {
            productQuantities.remove(productQuantity);
        } else {
            productQuantity.setQuantity(productQuantity.getQuantity().subtract(quantity));
        }
        shoppingCartRepository.updateProductQuantities(shoppingCart.getId(), productQuantities);
    }
}
