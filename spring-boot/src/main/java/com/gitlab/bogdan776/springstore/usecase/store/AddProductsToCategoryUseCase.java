package com.gitlab.bogdan776.springstore.usecase.store;

import java.util.List;
import java.util.UUID;

public interface AddProductsToCategoryUseCase {

    void execute(UUID categoryId, List<UUID> productIds);
}
