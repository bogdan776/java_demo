package com.gitlab.bogdan776.springstore.dto.response;

import com.gitlab.bogdan776.springstore.common.dto.Specification;
import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyResponse;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

@Data
public class ProductResponse {

    private UUID id;
    private String name;
    private String description;
    private List<Specification> specifications;
    private ResourceKeyResponse previewImage;
    private List<ResourceKeyResponse> images;
    private BigDecimal price;
    private BigDecimal discount;
    private BigDecimal finalPrice;
    private BigInteger availableQuantity;
    private BigDecimal rating;
}
