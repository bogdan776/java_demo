package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.dto.response.ProductShortResponse;
import com.gitlab.bogdan776.springstore.mapper.ProductMapper;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DefaultGetAllProducts implements GetAllProductsUseCase {

    private final ProductRepository repository;
    private final ProductMapper mapper;

    @Override
    public Page<ProductShortResponse> execute(Pageable pageable) {
        return repository.findAll(pageable)
            .map(mapper::mapToShortResponse);
    }
}
