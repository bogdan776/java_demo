package com.gitlab.bogdan776.springstore.usecase.auth;

import com.gitlab.bogdan776.springstore.common.dto.payload.PasswordPayload;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import com.gitlab.bogdan776.springstore.security.JwtService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DefaultActivateRandomPassword implements ActivateRandomPasswordUseCase {

    private final UserRepository repository;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void execute(String token) {
        PasswordPayload payload = jwtService.parseToken(token, PasswordPayload.class);
        UserProfile user = repository.findById(payload.getUserId())
            .orElseThrow(() -> new UserNotFoundException(payload.getUserId()));
        user.setPassword(passwordEncoder.encode(payload.getPassword()));
        repository.save(user);
    }
}
