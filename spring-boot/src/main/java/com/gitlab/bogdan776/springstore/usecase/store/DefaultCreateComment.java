package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.dto.request.CommentRequest;
import com.gitlab.bogdan776.springstore.dto.response.CommentResponse;
import com.gitlab.bogdan776.springstore.entity.store.Comment;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.ProductNotFoundException;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.mapper.CommentMapper;
import com.gitlab.bogdan776.springstore.repository.store.CommentRepository;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultCreateComment implements CreateCommentUseCase {

    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final CommentRepository commentRepository;
    private final CommentMapper mapper;

    @Override
    @Transactional
    public CommentResponse execute(UUID userId, UUID productId, CommentRequest request) {
        UserProfile user = userRepository.findById(userId)
            .orElseThrow(() -> new UserNotFoundException(userId));
        Product product = productRepository.findById(productId)
            .orElseThrow(() -> new ProductNotFoundException(productId));
        Comment comment = new Comment();
        comment.setContent(request.getContent());
        comment.setRating(request.getRating());
        comment.setAuthor(user);
        comment.setProduct(product);
        comment.setCreatedAt(TimeUtils.timeNow());
        recalculateProductRating(product, request.getRating());
        return mapper.map(commentRepository.save(comment));
    }

    private void recalculateProductRating(Product product, BigInteger rating) {
        if (rating == null) {
            return;
        }
        Optional.ofNullable(product.getRating()).ifPresentOrElse(
            originalRating -> {
                BigDecimal count = new BigDecimal(product.getRatingCount());
                BigDecimal newRatingValue = originalRating.multiply(count)
                    .add(new BigDecimal(rating))
                    .divide(count.add(BigDecimal.ONE), 1, RoundingMode.HALF_UP);
                product.setRating(newRatingValue);
            },
            () -> product.setRating(new BigDecimal(rating))
        );
        Optional.ofNullable(product.getRatingCount())
            .map(count -> count.add(BigInteger.ONE))
            .or(() -> Optional.of(BigInteger.ONE))
            .ifPresent(product::setRatingCount);
        productRepository.save(product);
    }
}
