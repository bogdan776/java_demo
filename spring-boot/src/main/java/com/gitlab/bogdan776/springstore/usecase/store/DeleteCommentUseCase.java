package com.gitlab.bogdan776.springstore.usecase.store;

import java.util.UUID;

public interface DeleteCommentUseCase {

    void execute(UUID userId, UUID productId, UUID commentId);
}
