package com.gitlab.bogdan776.springstore.controller.user;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;

@Validated
@RequestMapping("/v1/users")
public interface UserController {
}
