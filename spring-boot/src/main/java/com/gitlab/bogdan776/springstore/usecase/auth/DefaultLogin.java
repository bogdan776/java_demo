package com.gitlab.bogdan776.springstore.usecase.auth;

import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.dto.request.LoginRequest;
import com.gitlab.bogdan776.springstore.dto.response.JwtResponse;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.mapper.AuthMapper;
import com.gitlab.bogdan776.springstore.security.JwtService;
import com.gitlab.bogdan776.springstore.security.exception.AuthenticationException;
import com.gitlab.bogdan776.springstore.service.UserService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class DefaultLogin implements LoginUseCase {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthMapper authMapper;

    @Value("${security.jwt.token.expiration-days:5}")
    private Long expirationDays;

    @Override
    @Transactional
    public JwtResponse execute(LoginRequest request) {
        UserProfile profile = userService.findByUsername(request.getLogin())
            .or(() -> userService.findByEmail(request.getLogin()))
            .orElseThrow(() -> new UserNotFoundException(request.getLogin()));
        if (!passwordEncoder.matches(request.getPassword(), profile.getPassword())) {
            throw new WrongInputException("Wrong password");
        }
        if (!profile.isEmailConfirmed()) {
            throw new AuthenticationException("Please confirm your email address");
        }
        LocalDateTime expiration = TimeUtils.timeNow().plusDays(expirationDays);
        String token = jwtService.createToken(authMapper.userPayload(profile), expiration);
        return authMapper.map(profile, token, expiration);
    }
}
