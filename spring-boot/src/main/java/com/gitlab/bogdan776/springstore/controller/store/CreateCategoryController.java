package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.request.CategoryRequest;
import com.gitlab.bogdan776.springstore.dto.response.CategoryShortResponse;
import com.gitlab.bogdan776.springstore.usecase.store.CreateCategoryUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class CreateCategoryController implements StoreController {

    private final CreateCategoryUseCase createCategory;

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping(value = "/categories", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryShortResponse> createCategory(@RequestBody @Validated CategoryRequest request) {
        return ResponseEntity.ok(createCategory.execute(request));
    }
}
