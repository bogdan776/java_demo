package com.gitlab.bogdan776.springstore.filestorage;

import lombok.Data;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Data
public class ResourceKey {

    public static final String AWS_BUCKET_NAME = "aws.s3.bucket.name";

    private final UUID id;
    private final URL url;
    private final String path;
    private final Map<String, String> properties = new HashMap<>();

    ResourceKey(UUID id, URL url, String path, Map<String, String> properties) {
        this.id = id;
        this.url = url;
        this.path = path;
        this.properties.putAll(properties);
    }

    public static ResourceKeyBuilder builder() {
        return new ResourceKeyBuilder();
    }

    public void addProperty(String key, String value) {
        properties.put(key, value);
    }

    public String getProperty(String key) {
        return properties.get(key);
    }

    public static class ResourceKeyBuilder {

        private final Map<String, String> properties = new HashMap<>();

        private UUID id;
        private URL url;
        private String path;

        public ResourceKeyBuilder id(UUID id) {
            this.id = Objects.requireNonNull(id);
            return this;
        }

        public ResourceKeyBuilder url(URL url) {
            this.url = Objects.requireNonNull(url);
            return this;
        }

        public ResourceKeyBuilder path(String path) {
            this.path = Objects.requireNonNull(path);
            return this;
        }

        public ResourceKeyBuilder property(String key, String value) {
            this.properties.put(key, value);
            return this;
        }

        public ResourceKeyBuilder properties(Map<String, String> properties) {
            this.properties.putAll(properties);
            return this;
        }

        public ResourceKey build() {
            return new ResourceKey(this.id, this.url, this.path, this.properties);
        }
    }
}
