package com.gitlab.bogdan776.springstore.controller.user;

import com.gitlab.bogdan776.springstore.dto.request.ChangePasswordRequest;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.user.ChangePasswordUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class ChangePasswordController extends SecurityController implements UserController {

    private final ChangePasswordUseCase changePassword;

    @PostMapping(value = "/password", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> changePassword(@RequestBody ChangePasswordRequest request) {
        changePassword.execute(getUserId(), request);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
