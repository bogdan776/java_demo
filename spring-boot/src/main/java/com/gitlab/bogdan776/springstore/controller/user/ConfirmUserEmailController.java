package com.gitlab.bogdan776.springstore.controller.user;

import com.gitlab.bogdan776.springstore.controller.PublicController;
import com.gitlab.bogdan776.springstore.usecase.user.ConfirmUserEmailUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class ConfirmUserEmailController implements PublicController {

    private final ConfirmUserEmailUseCase confirmUserEmail;

    @RequestMapping(
        value = "/users/{userId}/emails/confirmation",
        method = {RequestMethod.GET, RequestMethod.POST},
        produces = APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> confirmEmail(@PathVariable("userId") UUID userId,
                                               @RequestParam("token") String token) {
        confirmUserEmail.execute(userId, token);
        return ResponseEntity.ok("Email confirmed");
    }
}
