package com.gitlab.bogdan776.springstore.security;

import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import com.gitlab.bogdan776.springstore.security.exception.AuthenticationException;
import com.gitlab.bogdan776.springstore.security.token.JwtAuthenticationToken;
import com.gitlab.bogdan776.springstore.security.token.UserDetails;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;
import java.util.UUID;

public abstract class SecurityController {

    protected UUID getUserId() {
        return userDetails()
            .orElseThrow(() -> new AuthenticationException("Cannot get user id"))
            .getId();
    }

    protected String getUsername() {
        return userDetails()
            .orElseThrow(() -> new AuthenticationException("Cannot get username"))
            .getUsername();
    }

    protected UserRole getUserRole() {
        return userDetails()
            .orElseThrow(() -> new AuthenticationException("Cannot get role"))
            .getRole();
    }

    protected Optional<UserDetails> userDetails() {
        return userAuthenticationToken()
            .map(JwtAuthenticationToken::getPrincipal)
            .map(UserDetails.class::cast);
    }

    protected Optional<JwtAuthenticationToken> userAuthenticationToken() {
        return authentication()
            .filter(Authentication::isAuthenticated)
            .filter(authentication -> authentication instanceof JwtAuthenticationToken)
            .map(JwtAuthenticationToken.class::cast);
    }

    protected boolean isAnonymous() {
        return authentication()
            .filter(authentication -> authentication instanceof AnonymousAuthenticationToken)
            .map(Authentication::isAuthenticated)
            .orElse(false);
    }

    protected Optional<Authentication> authentication() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
            .map(SecurityContext::getAuthentication);
    }
}
