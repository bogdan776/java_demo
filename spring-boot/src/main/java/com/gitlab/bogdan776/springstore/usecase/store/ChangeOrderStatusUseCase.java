package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.constant.enums.OrderStatus;
import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;

import java.util.UUID;

public interface ChangeOrderStatusUseCase {

    OrderResponse execute(UUID orderId, OrderStatus status);
}
