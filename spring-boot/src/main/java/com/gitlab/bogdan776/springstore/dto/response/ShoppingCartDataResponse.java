package com.gitlab.bogdan776.springstore.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder(setterPrefix = "with")
public class ShoppingCartDataResponse {

    private UUID cartId;
    private long expiration;
}
