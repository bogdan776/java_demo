package com.gitlab.bogdan776.springstore.usecase.internal;

import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Locale;

@Component
@RequiredArgsConstructor
public class DefaultSendOrderLetter implements SendOrderLetterUseCase {

    private final MailService mailService;
    private final TemplateEngine templateEngine;

    @Value("${spring.mail.template.order}")
    private String templateName;

    @Override
    public void execute(String email, OrderResponse response) {
        Context context = new Context(Locale.ENGLISH);
        context.setVariable("order", response);
        mailService.sendMimeMessage(email, "Order", templateEngine.process(templateName, context));
    }
}
