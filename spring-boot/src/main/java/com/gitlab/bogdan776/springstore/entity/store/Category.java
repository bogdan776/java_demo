package com.gitlab.bogdan776.springstore.entity.store;

import com.gitlab.bogdan776.springstore.filestorage.ResourceKeyJson;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@Table(name = "spring_store_category")
@EqualsAndHashCode(of = {"id", "name"})
public class Category {

    @Id
    @GeneratedValue
    private UUID id;

    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    @Column(unique = true)
    private String displayName;

    @NotNull
    private String description;

    @JdbcTypeCode(SqlTypes.JSON)
    private ResourceKeyJson previewImage;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "spring_store_products_categories",
        joinColumns = @JoinColumn(name = "category_id"),
        inverseJoinColumns = @JoinColumn(name = "product_id"))
    @ToString.Exclude
    private List<Product> products;
}
