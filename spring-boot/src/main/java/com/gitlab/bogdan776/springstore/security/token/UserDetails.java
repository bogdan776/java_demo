package com.gitlab.bogdan776.springstore.security.token;

import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import com.gitlab.bogdan776.springstore.common.dto.payload.UserPayload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetails {

    private UUID id;
    private String username;
    private UserRole role;

    public static UserDetails of(UserPayload payload) {
        return new UserDetails(
            payload.getId(),
            payload.getUsername(),
            payload.getRole()
        );
    }
}
