package com.gitlab.bogdan776.springstore.filestorage;

import lombok.RequiredArgsConstructor;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.function.Consumer;

@Component
@RequiredArgsConstructor
public class DefaultUploadFile implements UploadFileUseCase {

    private static final String FILE_NAME_FORMAT = "%s/%s%s";

    private final TikaConfig tikaConfig = TikaConfig.getDefaultConfig();
    private final MimeTypes mimeRepository = tikaConfig.getMimeRepository();
    private final Tika tika = new Tika();

    private final FileStorageService fileStorageService;
    private final ResourceKeyMapper mapper;

    @Override
    public URI execute(MultipartFile file, String path, String name, Consumer<ResourceKeyJson> resourceKeyJsonConsumer) {
        try (InputStream inputStream = new ByteArrayInputStream(file.getBytes())) {
            String fileExtension = getPreferredFileExtension(detectMediaType(inputStream));
            String fullPath = String.format(FILE_NAME_FORMAT, validatePath(path), name, fileExtension);
            ResourceKey resourceKey = fileStorageService.upload(fullPath, inputStream);
            ResourceKeyJson key = mapper.map(resourceKey);
            resourceKeyJsonConsumer.accept(key);
            return getUrlWithRnd(resourceKey);
        } catch (IOException e) {
            throw new FileStorageException(e);
        }
    }

    public String validatePath(String path) {
        return path.endsWith("/")
            ? path.substring(0, path.length() - 1)
            : path;
    }

    public String detectMediaType(InputStream inputStream) {
        try {
            return tika.detect(inputStream);
        } catch (IOException e) {
            throw new FileStorageException("Cannot detect media type", e);
        }
    }

    public String getPreferredFileExtension(String contentType) {
        try {
            return mimeRepository.forName(contentType)
                .getExtension();
        } catch (MimeTypeException e) {
            throw new FileStorageException("Cannot get mime type", e);
        }
    }

    public URI getUrlWithRnd(ResourceKey resourceKey) {
        UriComponents resourceUri = UriComponentsBuilder
            .fromHttpUrl(resourceKey.getUrl().toString())
            .queryParam("rnd", System.currentTimeMillis())
            .build();
        return resourceUri.toUri();
    }
}
