package com.gitlab.bogdan776.springstore.entity.store;

import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@Table(name = "spring_store_comment")
@EqualsAndHashCode(of = "id")
public class Comment {

    @Id
    @GeneratedValue
    private UUID id;

    private String content;

    private BigInteger rating;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private UserProfile author;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    @ToString.Exclude
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "root_id")
    @ToString.Exclude
    private Comment root;

    private LocalDateTime createdAt;

    @OneToMany(mappedBy = "root", fetch = FetchType.LAZY)
    private List<Comment> answers;
}
