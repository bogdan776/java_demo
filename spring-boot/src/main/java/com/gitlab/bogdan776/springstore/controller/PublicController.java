package com.gitlab.bogdan776.springstore.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;

@Validated
@RequestMapping("/v1/public")
public interface PublicController {
}
