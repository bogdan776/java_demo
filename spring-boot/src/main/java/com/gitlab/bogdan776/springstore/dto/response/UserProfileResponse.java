package com.gitlab.bogdan776.springstore.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gitlab.bogdan776.springstore.common.constant.enums.Country;
import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import lombok.Data;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserProfileResponse {

    private UUID id;
    private String username;
    private String email;
    private Country country;
    private UserRole role;
    private URL avatarUrl;
    private UserInfoResponse userInfo;
    private LocalDateTime createdAt;
}
