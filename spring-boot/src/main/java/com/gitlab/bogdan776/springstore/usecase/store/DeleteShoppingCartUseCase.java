package com.gitlab.bogdan776.springstore.usecase.store;

import java.util.UUID;

public interface DeleteShoppingCartUseCase {

    void execute(UUID userId, UUID cartId);
}
