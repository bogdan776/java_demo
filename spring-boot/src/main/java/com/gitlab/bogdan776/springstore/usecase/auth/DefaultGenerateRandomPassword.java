package com.gitlab.bogdan776.springstore.usecase.auth;

import com.gitlab.bogdan776.springstore.common.dto.payload.PasswordPayload;
import com.gitlab.bogdan776.springstore.common.utils.TimeUtils;
import com.gitlab.bogdan776.springstore.dto.request.ResetPasswordRequest;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import com.gitlab.bogdan776.springstore.security.JwtService;
import com.gitlab.bogdan776.springstore.security.PasswordGenerator;
import com.gitlab.bogdan776.springstore.usecase.internal.SendPasswordResetLetterUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DefaultGenerateRandomPassword implements GenerateRandomPasswordUseCase {

    private static final int TOKEN_ALIVE_IN_MINUTES = 30;

    private final UserRepository repository;
    private final PasswordGenerator passwordGenerator;
    private final JwtService jwtService;

    private final SendPasswordResetLetterUseCase sendPasswordResetLetter;

    @Override
    public void execute(ResetPasswordRequest request) {
        UserProfile user = repository.findByUsername(request.getLogin())
            .or(() -> repository.findByEmail(request.getLogin()))
            .filter(UserProfile::isEmailConfirmed)
            .orElseThrow(() -> new UserNotFoundException(request.getLogin()));
        String generatedPassword = passwordGenerator.generate();
        PasswordPayload payload = new PasswordPayload(user.getId(), generatedPassword);
        String token = jwtService.createToken(payload, TimeUtils.timeNow().plusMinutes(TOKEN_ALIVE_IN_MINUTES));
        sendPasswordResetLetter.execute(user.getEmail(), generatedPassword, token);
    }
}
