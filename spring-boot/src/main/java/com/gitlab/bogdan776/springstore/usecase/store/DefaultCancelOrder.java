package com.gitlab.bogdan776.springstore.usecase.store;

import com.gitlab.bogdan776.springstore.common.constant.enums.OrderStatus;
import com.gitlab.bogdan776.springstore.common.constant.enums.UserRole;
import com.gitlab.bogdan776.springstore.dto.response.OrderResponse;
import com.gitlab.bogdan776.springstore.entity.store.Order;
import com.gitlab.bogdan776.springstore.entity.store.OrderedProduct;
import com.gitlab.bogdan776.springstore.entity.store.Product;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.OrderNotFoundException;
import com.gitlab.bogdan776.springstore.exception.UserNotFoundException;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.mapper.OrderMapper;
import com.gitlab.bogdan776.springstore.repository.store.OrderRepository;
import com.gitlab.bogdan776.springstore.repository.store.ProductRepository;
import com.gitlab.bogdan776.springstore.repository.user.UserRepository;
import com.gitlab.bogdan776.springstore.scheduler.SchedulerService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.gitlab.bogdan776.springstore.common.constant.Constants.ADMIN_ID;

@Component
@RequiredArgsConstructor
public class DefaultCancelOrder implements CancelOrderUseCase {

    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;

    private final SchedulerService schedulerService;
    private final OrderMapper mapper;

    @Override
    @Transactional
    public OrderResponse execute(UUID userId, UUID orderId) {
        Order order = orderRepository.findById(orderId)
            .orElseThrow(() -> new OrderNotFoundException(orderId));
        if (order.getStatus() == OrderStatus.CANCELLED) {
            throw new WrongInputException("Order already cancelled");
        }
        if (!userId.equals(ADMIN_ID) && !userId.equals(order.getUser().getId())) {
            UserProfile user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
            if (user.getRole() != UserRole.ADMIN) {
                throw new WrongInputException("Not authorized cancel this order");
            }
        }
        Map<UUID, BigInteger> productQuantityMap = order.getOrderedProducts().stream()
            .collect(Collectors.toMap(OrderedProduct::getProductId, OrderedProduct::getQuantity));
        List<Product> products = productRepository.findAllByOrderId(orderId);
        products.forEach(
            product -> product.setAvailableQuantity(
                product.getAvailableQuantity().add(productQuantityMap.get(product.getId()))
            )
        );
        productRepository.saveAll(products);
        order.setStatus(OrderStatus.CANCELLED);
        orderRepository.save(order);
        schedulerService.unscheduleOrderCancellation(order.getOrderCancellationJob());
        return mapper.map(order);
    }
}
