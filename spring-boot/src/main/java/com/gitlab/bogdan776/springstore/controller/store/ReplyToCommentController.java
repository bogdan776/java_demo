package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.request.CommentRequest;
import com.gitlab.bogdan776.springstore.dto.response.CommentResponse;
import com.gitlab.bogdan776.springstore.security.SecurityController;
import com.gitlab.bogdan776.springstore.usecase.store.ReplyToCommentUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class ReplyToCommentController extends SecurityController implements StoreController {

    private final ReplyToCommentUseCase replyToComment;

    @PostMapping(value = "/products/{productId}/comments/{commentId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CommentResponse> replyToComment(@PathVariable("productId") UUID productId,
                                                          @PathVariable("commentId") UUID commentId,
                                                          @RequestBody @Validated CommentRequest request) {
        return ResponseEntity.ok(replyToComment.execute(getUserId(), productId, commentId, request));
    }
}
