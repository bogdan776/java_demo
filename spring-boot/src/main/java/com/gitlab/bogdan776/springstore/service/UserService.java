package com.gitlab.bogdan776.springstore.service;

import com.gitlab.bogdan776.springstore.entity.user.UserInfo;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;

import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

public interface UserService {

    UserProfile create(String username, String email, String password, String countryCode);

    UserInfo createUserInfo(UserProfile user, Consumer<UserInfo> consumer);

    Optional<UserProfile> findById(UUID id);

    Optional<UserProfile> findByUsername(String username);

    Optional<UserProfile> findByEmail(String email);

    void save(UserProfile userProfile);
}
