package com.gitlab.bogdan776.springstore.usecase.auth;

import com.gitlab.bogdan776.springstore.dto.request.SignUpRequest;
import com.gitlab.bogdan776.springstore.dto.response.UserProfileResponse;
import com.gitlab.bogdan776.springstore.entity.user.UserProfile;
import com.gitlab.bogdan776.springstore.exception.WrongInputException;
import com.gitlab.bogdan776.springstore.mapper.UserMapper;
import com.gitlab.bogdan776.springstore.service.UserService;
import com.gitlab.bogdan776.springstore.usecase.internal.SendConfirmEmailLetterUseCase;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DefaultSignUp implements SignUpUseCase {

    private final UserService userService;
    private final UserMapper userMapper;

    private final SendConfirmEmailLetterUseCase sendConfirmEmailLetter;

    @Override
    @Transactional
    public UserProfileResponse execute(SignUpRequest request) {
        if (!request.getPassword().equals(request.getRepeatPassword())) {
            throw new WrongInputException("Passwords must be equal");
        }
        userService.findByUsername(request.getUsername()).ifPresent(user -> {
            throw new WrongInputException("User with this username already exists");
        });
        userService.findByEmail(request.getEmail()).ifPresent(user -> {
            throw new WrongInputException("User with this email already exists");
        });
        UserProfile userProfile = userService.create(
            request.getUsername(), request.getEmail(), request.getPassword(), request.getCountryCode()
        );
        sendConfirmEmailLetter.execute(userProfile.getId(), userProfile.getEmail());
        return userMapper.mapUser(userProfile);
    }
}
