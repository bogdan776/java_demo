package com.gitlab.bogdan776.springstore.controller.store;

import com.gitlab.bogdan776.springstore.dto.response.ProductShortResponse;
import com.gitlab.bogdan776.springstore.usecase.store.GetProductsByCategoriesUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class GetProductsByCategoriesController implements StoreController {

    private final GetProductsByCategoriesUseCase getProductsByCategories;

    @GetMapping(value = "/products/categories/{categoryNames}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<ProductShortResponse>> getProductsByCategories(@PathVariable("categoryNames") List<String> categoryNames,
                                                                              Pageable pageable) {
        return ResponseEntity.ok(getProductsByCategories.execute(categoryNames, pageable));
    }
}
