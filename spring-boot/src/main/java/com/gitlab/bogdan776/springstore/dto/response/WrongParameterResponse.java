package com.gitlab.bogdan776.springstore.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WrongParameterResponse {

    private String field;
    private String description;
}
