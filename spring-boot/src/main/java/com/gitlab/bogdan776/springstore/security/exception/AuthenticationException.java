package com.gitlab.bogdan776.springstore.security.exception;

public class AuthenticationException extends SecurityException {

    public AuthenticationException(String message) {
        super(message);
    }
}
