package com.gitlab.bogdan776.springstore.security;

import com.gitlab.bogdan776.springstore.common.dto.payload.UserPayload;
import com.gitlab.bogdan776.springstore.security.exception.AuthenticationException;
import com.gitlab.bogdan776.springstore.security.token.JwtAuthenticationToken;
import com.gitlab.bogdan776.springstore.security.token.UserDetails;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationProvider implements AuthenticationProvider {

    private final JwtService jwtService;

    public JwtAuthenticationToken validateToken(String token) {
        try {
            UserDetails userDetails = UserDetails.of(jwtService.parseToken(token, UserPayload.class));
            return new JwtAuthenticationToken(userDetails, userDetails.getRole());
        } catch (ExpiredJwtException e) {
            throw new AuthenticationException("Authorization token expired");
        } catch (Exception e) {
            throw new AuthenticationException("Invalid authorization token");
        }
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
