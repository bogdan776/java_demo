create table if not exists spring_store_user_profile
(
    id              uuid      not null,
    username        varchar   not null,
    email           varchar   not null,
    email_confirmed boolean   not null,
    password        varchar   not null,
    country         varchar   not null,
    role            varchar   not null,
    avatar_key      jsonb,
    created_at      timestamp not null,
    constraint spring_store_user_profile_pk primary key (id),
    constraint spring_store_user_profile_uq_email unique (email),
    constraint spring_store_user_profile_uq_username unique (username)
);

create table if not exists spring_store_user_info
(
    user_id      uuid not null,
    full_name    varchar,
    address      varchar,
    phone_number varchar,
    introduction varchar,
    constraint spring_store_user_info_pk primary key (user_id)
);

create table if not exists spring_store_product
(
    id                 uuid           not null,
    name               varchar        not null,
    description        varchar        not null,
    specifications     jsonb,
    preview_image      jsonb,
    images             jsonb,
    price              numeric(38, 2) not null,
    discount           numeric(38, 2),
    available_quantity numeric(38)    not null,
    rating             numeric(38, 2),
    rating_count       numeric(38),
    added_at           timestamp      not null,
    constraint spring_store_product_pk primary key (id)
);

create table if not exists spring_store_category
(
    id            uuid    not null,
    name          varchar not null,
    display_name  varchar not null,
    description   varchar not null,
    preview_image jsonb,
    constraint spring_store_category_pk primary key (id),
    constraint spring_store_category_uq_name unique (name),
    constraint spring_store_category_uq_display_name unique (display_name)
);

create table if not exists spring_store_products_categories
(
    product_id  uuid not null,
    category_id uuid not null,
    constraint spring_store_products_categories_fk_product_id foreign key (product_id) references spring_store_product,
    constraint spring_store_products_categories_fk_category_id foreign key (category_id) references spring_store_category
);

create table if not exists spring_store_shopping_cart
(
    id                 uuid      not null,
    product_quantities jsonb,
    user_id            uuid,
    deleting_cart_job  uuid,
    created_at         timestamp not null,
    constraint spring_store_shopping_cart_pk primary key (id),
    constraint spring_store_shopping_cart_uq_user unique (user_id),
    constraint spring_store_shopping_cart_fk_user foreign key (user_id) references spring_store_user_profile
);

create table if not exists spring_store_comment
(
    id         uuid      not null,
    content    varchar,
    rating     numeric(38),
    author_id  uuid      not null,
    product_id uuid      not null,
    root_id    uuid,
    created_at timestamp not null,
    constraint spring_store_comment_pk primary key (id),
    constraint spring_store_comment_fk_author foreign key (author_id) references spring_store_user_profile,
    constraint spring_store_comment_fk_product foreign key (product_id) references spring_store_product,
    constraint spring_store_comment_fk_root_comment foreign key (root_id) references spring_store_comment
);

create table if not exists spring_store_order
(
    id                     uuid           not null,
    user_id                uuid           not null,
    status                 varchar        not null,
    price                  numeric(38, 2) not null,
    delivery_info          jsonb          not null,
    order_cancellation_job uuid,
    created_at             timestamp      not null,
    constraint spring_store_order_pk primary key (id),
    constraint spring_store_order_fk_user foreign key (user_id) references spring_store_user_profile
);

create table if not exists spring_store_ordered_product
(
    id         uuid           not null,
    order_id   uuid           not null,
    product_id uuid           not null,
    unit_price numeric(38, 2) not null,
    quantity   numeric(38)    not null,
    created_at timestamp      not null,
    constraint spring_store_ordered_product_pk primary key (id),
    constraint spring_store_ordered_product_fk_order foreign key (order_id) references spring_store_order,
    constraint spring_store_ordered_product_fk_product foreign key (product_id) references spring_store_product
);
