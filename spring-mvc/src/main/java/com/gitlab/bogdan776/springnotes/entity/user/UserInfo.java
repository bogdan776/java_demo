package com.gitlab.bogdan776.springnotes.entity.user;

import com.gitlab.bogdan776.springnotes.filestorage.ResourceKeyJson;
import lombok.Data;

import java.util.UUID;

@Data
public class UserInfo {

    private UUID userId;
    private ResourceKeyJson avatar;
    private String name;
    private String surname;
    private String address;
    private String instagram;
    private String facebook;
    private String twitter;
    private String biography;
}
