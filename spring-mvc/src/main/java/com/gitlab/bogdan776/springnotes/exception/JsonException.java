package com.gitlab.bogdan776.springnotes.exception;

public class JsonException extends AppException {

    public JsonException(String message) {
        super(message);
    }

    public JsonException(String message, Throwable cause) {
        super(message, cause);
    }
}
