package com.gitlab.bogdan776.springnotes.filestorage;

import java.net.URL;
import java.util.Map;

public interface ResourceKey {

    String AWS_BUCKET_NAME = "aws.s3.bucket.name";

    static ResourceKeyBuilder builder() {
        return new ResourceKeyBuilder();
    }

    URL getUrl();

    void setUrl(URL url);

    String getPath();

    void setPath(String path);

    void addProperty(String key, String value);

    String getProperty(String key);

    Map<String, String> properties();
}
