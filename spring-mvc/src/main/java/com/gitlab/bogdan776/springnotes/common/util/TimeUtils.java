package com.gitlab.bogdan776.springnotes.common.util;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;

public final class TimeUtils {

    private static final Clock CLOCK = Clock.systemUTC();

    private TimeUtils() {
    }

    public static Instant instant() {
        return Instant.now(CLOCK);
    }

    public static LocalDateTime now() {
        return LocalDateTime.now(CLOCK);
    }
}
