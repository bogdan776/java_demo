package com.gitlab.bogdan776.springnotes.controller.user;

import com.gitlab.bogdan776.springnotes.security.SecurityController;
import com.gitlab.bogdan776.springnotes.usecase.user.DeleteUserAvatarUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class DeleteUserAvatarController extends SecurityController implements BaseUserController {

    private final DeleteUserAvatarUseCase deleteUserAvatar;

    @PostMapping(value = "/avatar/delete")
    public String deleteUserAvatar() {
        deleteUserAvatar.execute(getUserId());
        return "redirect:/users/profile/edit";
    }
}
