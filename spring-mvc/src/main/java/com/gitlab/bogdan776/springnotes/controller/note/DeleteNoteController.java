package com.gitlab.bogdan776.springnotes.controller.note;

import com.gitlab.bogdan776.springnotes.security.SecurityController;
import com.gitlab.bogdan776.springnotes.usecase.note.DeleteNoteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.UUID;

@Controller
@RequiredArgsConstructor
public class DeleteNoteController extends SecurityController implements BaseNoteController {

    private final DeleteNoteUseCase deleteNote;

    /*
    It would be cool to use @DeleteMapping but with JSP without JS...
     */
    @PostMapping("/{noteId}/delete")
    public String deleteNote(@PathVariable("noteId") UUID noteId) {
        deleteNote.execute(getUserId(), noteId);
        return "redirect:/notes";
    }
}
