package com.gitlab.bogdan776.springnotes.exception;

public class DetectMediaException extends AppException {

    public DetectMediaException(Throwable cause) {
        super(cause);
    }
}
