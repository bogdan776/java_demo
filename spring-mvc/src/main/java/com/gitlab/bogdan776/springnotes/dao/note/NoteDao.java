package com.gitlab.bogdan776.springnotes.dao.note;

import com.gitlab.bogdan776.springnotes.entity.note.Note;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface NoteDao {

    Optional<Note> findById(UUID id);

    List<Note> findAllByAuthorId(UUID authorId);

    void save(Note note);

    void update(Note note);

    void delete(Note note);
}
