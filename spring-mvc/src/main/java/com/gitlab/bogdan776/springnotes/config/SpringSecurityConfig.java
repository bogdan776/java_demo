package com.gitlab.bogdan776.springnotes.config;

import com.gitlab.bogdan776.springnotes.dao.user.UserDao;
import com.gitlab.bogdan776.springnotes.entity.user.UserRole;
import com.gitlab.bogdan776.springnotes.security.JdbcUserDetailsService;
import jakarta.servlet.DispatcherType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import static java.util.Arrays.asList;
import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends AbstractSecurityWebApplicationInitializer {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsService jdbcUserDetailsService(UserDao userDao) {
        return new JdbcUserDetailsService(userDao);
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable)
            .cors(cors -> cors.configurationSource(corsConfigurationSource()))
            .authorizeHttpRequests(
                authorizeRequests -> authorizeRequests
                    .dispatcherTypeMatchers(DispatcherType.FORWARD)
                    .permitAll()
                    .requestMatchers("/")
                    .permitAll()
                    .requestMatchers("/signup")
                    .anonymous()
                    .requestMatchers("/admin/**")
                    .hasAuthority(UserRole.ADMIN.name())
                    .anyRequest()
                    .authenticated())
            .formLogin(FormLoginConfigurer::permitAll)
            .logout(LogoutConfigurer::permitAll)
            .httpBasic(withDefaults());
        return http.build();
    }

    private CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedOriginPattern("*");
        configuration.setAllowedMethods(asList("GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"));
        configuration.setAllowCredentials(true);
        configuration.addAllowedHeader("*");
        UrlBasedCorsConfigurationSource urlBasedCorsConfig = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfig.registerCorsConfiguration("/**", configuration);
        return urlBasedCorsConfig;
    }
}
