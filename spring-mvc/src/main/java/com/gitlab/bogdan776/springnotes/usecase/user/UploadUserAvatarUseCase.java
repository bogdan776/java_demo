package com.gitlab.bogdan776.springnotes.usecase.user;

import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface UploadUserAvatarUseCase {

    void execute(UUID userId, MultipartFile avatar);
}
