package com.gitlab.bogdan776.springnotes.usecase.note;

import com.gitlab.bogdan776.springnotes.common.util.TimeUtils;
import com.gitlab.bogdan776.springnotes.dao.note.NoteDao;
import com.gitlab.bogdan776.springnotes.dto.request.NoteRequest;
import com.gitlab.bogdan776.springnotes.entity.note.Note;
import com.gitlab.bogdan776.springnotes.mapper.NoteMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DefaultCreateNote implements CreateNoteUseCase {

    private final NoteDao noteDao;
    private final NoteMapper mapper;

    @Override
    public void execute(UUID userId, NoteRequest request) {
        Note note = mapper.mapRequest(request);
        note.setId(UUID.randomUUID());
        note.setAuthorId(userId);
        note.setCreatedAt(TimeUtils.now());
        noteDao.save(note);
    }
}
