package com.gitlab.bogdan776.springnotes.filestorage;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.net.URL;
import java.util.Map;

@Getter
@Setter
@ToString
public class ResourceKeyJson {

    private URL url;
    private String path;
    private Map<String, String> properties;
}
