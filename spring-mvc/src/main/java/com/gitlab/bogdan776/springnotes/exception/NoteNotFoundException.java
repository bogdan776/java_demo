package com.gitlab.bogdan776.springnotes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(
    code = HttpStatus.NOT_FOUND,
    reason = "Note not found"
)
public class NoteNotFoundException extends WebException {

    public NoteNotFoundException() {
        super(HttpStatus.NOT_FOUND);
    }
}
