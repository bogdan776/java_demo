package com.gitlab.bogdan776.springnotes.filestorage;

import com.gitlab.bogdan776.springnotes.exception.FileStorageException;
import org.springframework.stereotype.Component;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.regex.Pattern;

@Component
public class DefaultImageCompressor implements ImageCompressor {

    private static final float QUALITY = 0.5f;
    private static final Pattern IMAGE_CONTENT_TYPE_PATTERN = Pattern.compile("image/\\w+");

    @Override
    public InputStream compress(InputStream inputStream, String extension) {
        try {
            BufferedImage image = ImageIO.read(inputStream);
            try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
                Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName(
                    extension.replace(".", ""));
                ImageWriter writer = writers.next();
                try (ImageOutputStream ios = ImageIO.createImageOutputStream(byteArrayOutputStream)) {
                    writer.setOutput(ios);
                    ImageWriteParam param = writer.getDefaultWriteParam();
                    param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                    param.setCompressionQuality(QUALITY);
                    writer.write(null, new IIOImage(image, null, null), param);
                }
                writer.dispose();
                return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            }
        } catch (IOException e) {
            throw new FileStorageException(e.getMessage());
        }
    }

    @Override
    public boolean compressible(String contentType) {
        return IMAGE_CONTENT_TYPE_PATTERN.matcher(contentType).find();
    }
}
