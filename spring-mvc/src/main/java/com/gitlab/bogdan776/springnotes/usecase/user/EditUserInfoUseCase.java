package com.gitlab.bogdan776.springnotes.usecase.user;

import com.gitlab.bogdan776.springnotes.dto.request.UserInfoRequest;

import java.util.UUID;

public interface EditUserInfoUseCase {

    void execute(UUID userId, UserInfoRequest request);
}
