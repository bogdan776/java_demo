package com.gitlab.bogdan776.springnotes.dto.response;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class NoteResponse {

    private UUID id;
    private String title;
    private String content;
    private LocalDateTime createdAt;
}
