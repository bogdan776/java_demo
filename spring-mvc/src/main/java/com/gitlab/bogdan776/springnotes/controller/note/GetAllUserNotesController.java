package com.gitlab.bogdan776.springnotes.controller.note;

import com.gitlab.bogdan776.springnotes.security.SecurityController;
import com.gitlab.bogdan776.springnotes.usecase.note.GetAllUserNotesUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class GetAllUserNotesController extends SecurityController implements BaseNoteController {

    private final GetAllUserNotesUseCase getAllUserNotes;

    @GetMapping
    public String getAllUserNotes(Model model) {
        model.addAttribute("notes", getAllUserNotes.execute(getUserId()));
        return "notes";
    }
}
