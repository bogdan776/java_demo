package com.gitlab.bogdan776.springnotes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(
    code = HttpStatus.FORBIDDEN,
    reason = "You do not have permission to modify or delete this resource"
)
public class EditAccessException extends WebException {

    public EditAccessException() {
        super(HttpStatus.FORBIDDEN);
    }
}
