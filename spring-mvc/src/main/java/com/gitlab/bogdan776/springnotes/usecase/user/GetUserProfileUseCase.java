package com.gitlab.bogdan776.springnotes.usecase.user;

import com.gitlab.bogdan776.springnotes.dto.response.UserResponse;

public interface GetUserProfileUseCase {

    UserResponse execute(String username);
}
