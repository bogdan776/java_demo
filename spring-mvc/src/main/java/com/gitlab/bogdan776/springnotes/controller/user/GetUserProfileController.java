package com.gitlab.bogdan776.springnotes.controller.user;

import com.gitlab.bogdan776.springnotes.security.SecurityController;
import com.gitlab.bogdan776.springnotes.usecase.user.GetUserProfileUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequiredArgsConstructor
public class GetUserProfileController extends SecurityController implements BaseUserController {

    private final GetUserProfileUseCase getUserProfile;

    @GetMapping
    public String getCurrentUserProfile(Model model) {
        model.addAttribute("user", getUserProfile.execute(getUsername()));
        return "user_profile";
    }

    @GetMapping("/{username}")
    public String getUserProfile(@PathVariable("username") String username, Model model) {
        model.addAttribute("user", getUserProfile.execute(username));
        return "user_profile";
    }
}
