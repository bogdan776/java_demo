package com.gitlab.bogdan776.springnotes.dto.request;

import com.gitlab.bogdan776.springnotes.common.constant.Constants;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SignUpRequest {

    @NotBlank
    @Pattern(regexp = Constants.USERNAME_REGEXP, message = "Wrong username")
    private String username;

    @NotBlank
    @Pattern(regexp = Constants.EMAIL_REGEXP, message = "Wrong email")
    private String email;

    @NotBlank
    @Pattern(regexp = Constants.PASSWORD_REGEXP, message = "Wrong password")
    private String password;

    @NotBlank
    private String passwordRepeat;
}
