package com.gitlab.bogdan776.springnotes.usecase.note;

import com.gitlab.bogdan776.springnotes.dao.note.NoteDao;
import com.gitlab.bogdan776.springnotes.entity.note.Note;
import com.gitlab.bogdan776.springnotes.exception.EditAccessException;
import com.gitlab.bogdan776.springnotes.exception.NoteNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DefaultDeleteNote implements DeleteNoteUseCase {

    private final NoteDao noteDao;

    @Override
    public void execute(UUID userId, UUID noteId) {
        Note note = noteDao.findById(noteId)
            .orElseThrow(NoteNotFoundException::new);
        if (!userId.equals(note.getAuthorId())) {
            throw new EditAccessException();
        }
        noteDao.delete(note);
    }
}
