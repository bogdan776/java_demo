package com.gitlab.bogdan776.springnotes.usecase.note;

import com.gitlab.bogdan776.springnotes.dto.response.NoteResponse;

import java.util.UUID;

public interface GetNoteUseCase {

    NoteResponse execute(UUID noteId);
}
