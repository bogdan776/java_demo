package com.gitlab.bogdan776.springnotes.dao.user;

import com.gitlab.bogdan776.springnotes.entity.user.User;
import com.gitlab.bogdan776.springnotes.entity.user.UserInfo;

import java.util.Optional;
import java.util.UUID;

public interface UserDao {

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    Optional<UserInfo> findInfoByUserId(UUID userId);

    void save(User user);

    void save(UserInfo info);

    void update(UserInfo info);
}
