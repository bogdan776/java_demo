package com.gitlab.bogdan776.springnotes.usecase.user;

import com.gitlab.bogdan776.springnotes.dao.user.UserDao;
import com.gitlab.bogdan776.springnotes.dto.request.UserInfoRequest;
import com.gitlab.bogdan776.springnotes.entity.user.UserInfo;
import com.gitlab.bogdan776.springnotes.exception.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DefaultEditUserInfo implements EditUserInfoUseCase {

    private final UserDao userDao;

    @Override
    public void execute(UUID userId, UserInfoRequest request) {
        UserInfo info = userDao.findInfoByUserId(userId)
            .orElseThrow(UserNotFoundException::new);
        info.setName(request.getName());
        info.setSurname(request.getSurname());
        info.setAddress(request.getAddress());
        info.setInstagram(request.getInstagram());
        info.setFacebook(request.getFacebook());
        info.setTwitter(request.getTwitter());
        info.setBiography(request.getBiography());
        userDao.update(info);
    }
}
