package com.gitlab.bogdan776.springnotes.usecase.user;

import com.gitlab.bogdan776.springnotes.dao.user.UserDao;
import com.gitlab.bogdan776.springnotes.entity.user.UserInfo;
import com.gitlab.bogdan776.springnotes.exception.UserNotFoundException;
import com.gitlab.bogdan776.springnotes.filestorage.UploadResourceUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DefaultUploadUserAvatar implements UploadUserAvatarUseCase {

    private static final String USER_AVATAR_PATH_FORMAT = "users/avatars/%s%s";

    private final UserDao userDao;
    private final UploadResourceUseCase uploadResource;

    @Override
    public void execute(UUID userId, MultipartFile avatar) {
        UserInfo info = userDao.findInfoByUserId(userId)
            .orElseThrow(UserNotFoundException::new);
        uploadResource.execute(avatar, USER_AVATAR_PATH_FORMAT, userId.toString(), info::setAvatar);
        userDao.update(info);
    }
}
