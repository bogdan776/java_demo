package com.gitlab.bogdan776.springnotes.exception;

public class FileStorageException extends AppException {

    public FileStorageException(Throwable cause) {
        super(cause);
    }

    public FileStorageException(String message) {
        super(message);
    }
}
