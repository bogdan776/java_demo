package com.gitlab.bogdan776.springnotes.dao.user.repository;

import com.gitlab.bogdan776.springnotes.entity.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class UserRepository {

    private final JdbcTemplate jdbcTemplate;

    public Optional<User> findUserByUsername(String username) {
        return jdbcTemplate.queryForStream(
            "select * from user_profile where username = ?",
            new UserRowMapper(),
            username
        ).findFirst();
    }

    public Optional<User> findUserByEmail(String email) {
        return jdbcTemplate.queryForStream(
            "select * from user_profile where email = ?",
            new UserRowMapper(),
            email
        ).findFirst();
    }

    public void save(User user) {
        SqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("id", user.getId())
            .addValue("username", user.getUsername())
            .addValue("email", user.getEmail())
            .addValue("password", user.getPassword())
            .addValue("role", user.getRole().getAuthority())
            .addValue("created_at", Timestamp.valueOf(user.getCreatedAt()));
        new SimpleJdbcInsert(jdbcTemplate)
            .withTableName("user_profile")
            .execute(parameters);
    }
}
