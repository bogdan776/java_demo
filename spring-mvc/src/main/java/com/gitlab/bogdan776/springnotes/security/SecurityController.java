package com.gitlab.bogdan776.springnotes.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;
import java.util.UUID;

public abstract class SecurityController {

    protected UUID getUserId() {
        return userDetails()
            .orElseThrow(() -> new AuthenticationException("Cannot get user id"))
            .getId();
    }

    protected String getUsername() {
        return userDetails()
            .orElseThrow(() -> new AuthenticationException("Cannot get username"))
            .getUsername();
    }

    protected Optional<CustomUserDetails> userDetails() {
        return authentication()
            .map(Authentication::getPrincipal)
            .map(CustomUserDetails.class::cast);
    }

    protected Optional<Authentication> authentication() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
            .map(SecurityContext::getAuthentication)
            .filter(Authentication::isAuthenticated);
    }
}
