package com.gitlab.bogdan776.springnotes.mapper;

import com.gitlab.bogdan776.springnotes.dto.request.NoteRequest;
import com.gitlab.bogdan776.springnotes.dto.response.NoteResponse;
import com.gitlab.bogdan776.springnotes.entity.note.Note;
import org.mapstruct.Mapper;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface NoteMapper {

    NoteResponse mapNote(Note note);

    Note mapRequest(NoteRequest request);
}
