package com.gitlab.bogdan776.springnotes.filestorage;

import java.util.HashMap;
import java.util.Map;

public abstract class PropertyAwareResourceKey implements ResourceKey {

    protected final Map<String, String> properties = new HashMap<>();

    @Override
    public void addProperty(String key, String value) {
        properties.put(key, value);
    }

    @Override
    public String getProperty(String key) {
        return properties.get(key);
    }

    @Override
    public Map<String, String> properties() {
        return new HashMap<>(properties);
    }
}
