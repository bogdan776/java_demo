package com.gitlab.bogdan776.springnotes.usecase.user;

import com.gitlab.bogdan776.springnotes.dao.user.UserDao;
import com.gitlab.bogdan776.springnotes.entity.user.UserInfo;
import com.gitlab.bogdan776.springnotes.exception.UserNotFoundException;
import com.gitlab.bogdan776.springnotes.filestorage.FileStorageService;
import com.gitlab.bogdan776.springnotes.filestorage.ResourceKeyMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DefaultDeleteUserAvatar implements DeleteUserAvatarUseCase {

    private final UserDao userDao;
    private final ResourceKeyMapper resourceKeyMapper;
    private final FileStorageService fileStorageService;

    @Override
    public void execute(UUID userId) {
        UserInfo info = userDao.findInfoByUserId(userId)
            .orElseThrow(UserNotFoundException::new);
        fileStorageService.deleteFile(resourceKeyMapper.mapResourceKey(info.getAvatar()));
        info.setAvatar(null);
        userDao.update(info);
    }
}
