package com.gitlab.bogdan776.springnotes.dao.user.repository;

import com.gitlab.bogdan776.springnotes.common.util.JsonUtils;
import com.gitlab.bogdan776.springnotes.entity.user.UserInfo;
import com.gitlab.bogdan776.springnotes.filestorage.ResourceKeyJson;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class UserInfoRowMapper implements RowMapper<UserInfo> {

    @Override
    public UserInfo mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        UserInfo info = new UserInfo();
        info.setUserId(UUID.fromString(resultSet.getString("user_id")));
        info.setAvatar(JsonUtils.fromJson(resultSet.getString("avatar"), ResourceKeyJson.class));
        info.setName(resultSet.getString("name"));
        info.setSurname(resultSet.getString("surname"));
        info.setAddress(resultSet.getString("address"));
        info.setInstagram(resultSet.getString("instagram"));
        info.setFacebook(resultSet.getString("facebook"));
        info.setTwitter(resultSet.getString("twitter"));
        info.setBiography(resultSet.getString("biography"));
        return info;
    }
}
