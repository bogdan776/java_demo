package com.gitlab.bogdan776.springnotes.dao.note.repository;

import com.gitlab.bogdan776.springnotes.entity.note.Note;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class NoteRepository {

    private final JdbcTemplate jdbcTemplate;

    public Optional<Note> findById(UUID id) {
        return jdbcTemplate.queryForStream(
            "select * from note where id = ?", new NoteRowMapper(), id
        ).findFirst();
    }

    public List<Note> findAllByAuthorId(UUID authorId) {
        return jdbcTemplate.queryForStream(
            "select * from note where author_id = ? order by created_at desc", new NoteRowMapper(), authorId
        ).collect(Collectors.toCollection(LinkedList::new));
    }

    public void update(Note note) {
        jdbcTemplate.update(
            "update note set title = ?, content = ? where id = ?",
            note.getTitle(), note.getContent(), note.getId()
        );
    }

    public void save(Note note) {
        SqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("id", note.getId())
            .addValue("author_id", note.getAuthorId())
            .addValue("title", note.getTitle())
            .addValue("content", note.getContent())
            .addValue("created_at", Timestamp.valueOf(note.getCreatedAt()));
        new SimpleJdbcInsert(jdbcTemplate)
            .withTableName("note")
            .execute(parameters);
    }

    public void delete(Note note) {
        jdbcTemplate.update("delete from note where id = ?", note.getId());
    }
}
