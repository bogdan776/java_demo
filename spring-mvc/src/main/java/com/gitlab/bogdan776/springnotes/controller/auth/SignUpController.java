package com.gitlab.bogdan776.springnotes.controller.auth;

import com.gitlab.bogdan776.springnotes.dto.request.SignUpRequest;
import com.gitlab.bogdan776.springnotes.usecase.auth.SignUpUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/signup")
@RequiredArgsConstructor
public class SignUpController {

    private final SignUpUseCase signUp;

    @GetMapping
    public String getRegistrationForm(Model model) {
        /*
        Use addAttribute("attributeName", value) to specify the attribute name,
        for this example, the standard class name is ok
         */
        model.addAttribute(new SignUpRequest());
        return "signup";
    }

    @PostMapping
    public String signUp(@Validated SignUpRequest request) {
        signUp.execute(request);
        return "redirect:/login";
    }
}
