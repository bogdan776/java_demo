package com.gitlab.bogdan776.springnotes.dto.response;

import lombok.Data;

import java.net.URL;

@Data
public class UserInfoResponse {

    private URL avatarUrl;
    private String name;
    private String surname;
    private String address;
    private String instagram;
    private String facebook;
    private String twitter;
    private String biography;
}
