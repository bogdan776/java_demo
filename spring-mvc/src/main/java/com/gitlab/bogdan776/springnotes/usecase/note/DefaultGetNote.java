package com.gitlab.bogdan776.springnotes.usecase.note;

import com.gitlab.bogdan776.springnotes.dao.note.NoteDao;
import com.gitlab.bogdan776.springnotes.dto.response.NoteResponse;
import com.gitlab.bogdan776.springnotes.entity.note.Note;
import com.gitlab.bogdan776.springnotes.exception.NoteNotFoundException;
import com.gitlab.bogdan776.springnotes.mapper.NoteMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DefaultGetNote implements GetNoteUseCase {

    private final NoteDao noteDao;
    private final NoteMapper mapper;

    @Override
    public NoteResponse execute(UUID noteId) {
        Note note = noteDao.findById(noteId)
            .orElseThrow(NoteNotFoundException::new);
        return mapper.mapNote(note);
    }
}
