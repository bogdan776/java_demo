package com.gitlab.bogdan776.springnotes.mapper;

import com.gitlab.bogdan776.springnotes.dto.response.UserInfoResponse;
import com.gitlab.bogdan776.springnotes.dto.response.UserResponse;
import com.gitlab.bogdan776.springnotes.entity.user.User;
import com.gitlab.bogdan776.springnotes.entity.user.UserInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface UserMapper {

    UserResponse mapUser(User user);

    @Mapping(target = "avatarUrl", source = "info.avatar.url")
    UserInfoResponse mapUserInfo(UserInfo info);
}
