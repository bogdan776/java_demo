package com.gitlab.bogdan776.springnotes.filestorage;

import java.io.InputStream;

public interface ImageCompressor {

    InputStream compress(InputStream inputStream, String extension);

    boolean compressible(String contentType);
}
