package com.gitlab.bogdan776.springnotes.dao.note.repository;

import com.gitlab.bogdan776.springnotes.entity.note.Note;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class NoteRowMapper implements RowMapper<Note> {

    @Override
    public Note mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Note note = new Note();
        note.setId(UUID.fromString(resultSet.getString("id")));
        note.setAuthorId(UUID.fromString(resultSet.getString("author_id")));
        note.setTitle(resultSet.getString("title"));
        note.setContent(resultSet.getString("content"));
        note.setCreatedAt(resultSet.getTimestamp("created_at").toLocalDateTime());
        return note;
    }
}
