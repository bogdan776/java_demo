package com.gitlab.bogdan776.springnotes.usecase.note;

import com.gitlab.bogdan776.springnotes.dao.note.NoteDao;
import com.gitlab.bogdan776.springnotes.dto.response.NoteResponse;
import com.gitlab.bogdan776.springnotes.mapper.NoteMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DefaultGetAllUserNotes implements GetAllUserNotesUseCase {

    private final NoteDao noteDao;
    private final NoteMapper mapper;

    @Override
    public List<NoteResponse> execute(UUID userId) {
        return noteDao.findAllByAuthorId(userId)
            .stream()
            .map(mapper::mapNote)
            .collect(Collectors.toList());
    }
}
