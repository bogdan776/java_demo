package com.gitlab.bogdan776.springnotes.filestorage;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ResourceKeyBuilder {

    private final Map<String, String> properties = new HashMap<>();

    private URL url;
    private String path;

    public ResourceKeyBuilder url(URL url) {
        this.url = Objects.requireNonNull(url, "url");
        return this;
    }

    public ResourceKeyBuilder path(String path) {
        this.path = Objects.requireNonNull(path, "path");
        return this;
    }

    public ResourceKeyBuilder property(String key, String value) {
        this.properties.put(key, value);
        return this;
    }

    public ResourceKeyBuilder properties(Map<String, String> properties) {
        this.properties.putAll(properties);
        return this;
    }

    public ResourceKey build() {
        return new SimpleResourceKey(url, path, properties);
    }

    private static final class SimpleResourceKey extends PropertyAwareResourceKey {

        private URL url;
        private String path;

        private SimpleResourceKey(URL url, String path, Map<String, String> properties) {
            this.url = url;
            this.path = Objects.requireNonNull(path, "path");
            this.properties.putAll(properties);
        }

        @Override
        public URL getUrl() {
            return url;
        }

        @Override
        public void setUrl(URL url) {
            this.url = Objects.requireNonNull(url, "url");
        }

        @Override
        public String getPath() {
            return path;
        }

        @Override
        public void setPath(String path) {
            this.path = Objects.requireNonNull(path, "path");
        }
    }
}
