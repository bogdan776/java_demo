package com.gitlab.bogdan776.springnotes.usecase.user;

import com.gitlab.bogdan776.springnotes.dao.user.UserDao;
import com.gitlab.bogdan776.springnotes.dto.response.UserResponse;
import com.gitlab.bogdan776.springnotes.entity.user.User;
import com.gitlab.bogdan776.springnotes.exception.UserNotFoundException;
import com.gitlab.bogdan776.springnotes.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DefaultGetUserProfile implements GetUserProfileUseCase {

    private final UserDao userDao;
    private final UserMapper mapper;

    @Override
    public UserResponse execute(String username) {
        User user = userDao.findByUsername(username)
            .orElseThrow(UserNotFoundException::new);
        userDao.findInfoByUserId(user.getId())
            .ifPresent(user::setInfo);
        return mapper.mapUser(user);
    }
}
