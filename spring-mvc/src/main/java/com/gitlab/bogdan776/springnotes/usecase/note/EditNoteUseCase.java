package com.gitlab.bogdan776.springnotes.usecase.note;

import com.gitlab.bogdan776.springnotes.dto.request.NoteRequest;

import java.util.UUID;

public interface EditNoteUseCase {

    void execute(UUID userId, UUID noteId, NoteRequest request);
}
