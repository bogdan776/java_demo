package com.gitlab.bogdan776.springnotes.filestorage;

import java.io.InputStream;

public interface FileStorageService {

    ResourceKey upload(String path, InputStream inputStream);

    ResourceKey upload(String path, InputStream inputStream, boolean privateRead);

    InputStream download(ResourceKey resourceKey);

    void deleteFile(ResourceKey key);

    boolean isFileExist(ResourceKey key);
}
