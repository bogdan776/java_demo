package com.gitlab.bogdan776.springnotes.dao.note;

import com.gitlab.bogdan776.springnotes.dao.note.repository.NoteRepository;
import com.gitlab.bogdan776.springnotes.entity.note.Note;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class DefaultNoteDao implements NoteDao {

    private final NoteRepository repository;

    @Override
    public Optional<Note> findById(UUID id) {
        return repository.findById(id);
    }

    @Override
    public List<Note> findAllByAuthorId(UUID authorId) {
        return repository.findAllByAuthorId(authorId);
    }

    @Override
    public void save(Note note) {
        repository.save(note);
    }

    @Override
    public void update(Note note) {
        repository.update(note);
    }

    @Override
    public void delete(Note note) {
        repository.delete(note);
    }
}
