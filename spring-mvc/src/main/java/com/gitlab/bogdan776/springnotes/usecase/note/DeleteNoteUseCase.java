package com.gitlab.bogdan776.springnotes.usecase.note;

import java.util.UUID;

public interface DeleteNoteUseCase {

    void execute(UUID userId, UUID noteId);
}
