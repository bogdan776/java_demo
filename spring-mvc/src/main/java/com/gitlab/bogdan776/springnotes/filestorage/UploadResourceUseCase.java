package com.gitlab.bogdan776.springnotes.filestorage;

import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.function.Consumer;

public interface UploadResourceUseCase {

    URI execute(MultipartFile file, String pathFormat, String name, Consumer<ResourceKeyJson> resourceKeyJsonConsumer);
}
