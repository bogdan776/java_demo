package com.gitlab.bogdan776.springnotes.common.constant;

public final class Constants {

    public static final String PASSWORD_REGEXP = "[a-zA-Z0-9#!%^&_-]{8,60}";
    public static final String USERNAME_REGEXP = "[a-zA-Z0-9_-]{3,20}";
    public static final String EMAIL_REGEXP = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}";

    private Constants() {
    }
}
