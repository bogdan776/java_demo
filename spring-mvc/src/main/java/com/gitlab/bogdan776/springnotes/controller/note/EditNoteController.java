package com.gitlab.bogdan776.springnotes.controller.note;

import com.gitlab.bogdan776.springnotes.dto.request.NoteRequest;
import com.gitlab.bogdan776.springnotes.dto.response.NoteResponse;
import com.gitlab.bogdan776.springnotes.security.SecurityController;
import com.gitlab.bogdan776.springnotes.usecase.note.EditNoteUseCase;
import com.gitlab.bogdan776.springnotes.usecase.note.GetNoteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.UUID;

@Controller
@RequiredArgsConstructor
public class EditNoteController extends SecurityController implements BaseNoteController {

    private final GetNoteUseCase getNote;
    private final EditNoteUseCase editNote;

    @GetMapping("/{noteId}/edit")
    public String getForm(@PathVariable("noteId") UUID noteId, Model model) {
        NoteResponse response = getNote.execute(noteId);
        model.addAttribute(setInitialNoteRequestParameters(response));
        model.addAttribute("note", response);
        return "note_editor";
    }

    @PostMapping("/{noteId}/edit")
    public String editNote(@PathVariable("noteId") UUID noteId, @Validated NoteRequest request) {
        editNote.execute(getUserId(), noteId, request);
        return "redirect:/notes";
    }

    private NoteRequest setInitialNoteRequestParameters(NoteResponse response) {
        NoteRequest request = new NoteRequest();
        request.setTitle(response.getTitle());
        request.setContent(response.getContent());
        return request;
    }
}
