package com.gitlab.bogdan776.springnotes.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@ToString
public class NoteRequest {

    @Length(min = 2, max = 250)
    private String title;

    @Length(min = 2, max = 1000)
    private String content;
}
