package com.gitlab.bogdan776.springnotes.usecase.auth;

import com.gitlab.bogdan776.springnotes.common.util.TimeUtils;
import com.gitlab.bogdan776.springnotes.dao.user.UserDao;
import com.gitlab.bogdan776.springnotes.dto.request.SignUpRequest;
import com.gitlab.bogdan776.springnotes.entity.user.User;
import com.gitlab.bogdan776.springnotes.entity.user.UserInfo;
import com.gitlab.bogdan776.springnotes.entity.user.UserRole;
import com.gitlab.bogdan776.springnotes.exception.UserAlreadyExistsException;
import com.gitlab.bogdan776.springnotes.exception.WrongPasswordException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DefaultSignUp implements SignUpUseCase {

    private final UserDao userDao;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void execute(SignUpRequest request) {
        if (!request.getPassword().equals(request.getPasswordRepeat())) {
            throw new WrongPasswordException("Passwords must be equal");
        }
        userDao.findByUsername(request.getUsername()).ifPresentOrElse(user -> {
            throw new UserAlreadyExistsException();
        }, () -> userDao.findByEmail(request.getEmail()).ifPresent(user -> {
            throw new UserAlreadyExistsException();
        }));

        User user = new User();
        user.setId(UUID.randomUUID());
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRole(UserRole.USER);
        user.setCreatedAt(TimeUtils.now());
        userDao.save(user);

        UserInfo info = new UserInfo();
        info.setUserId(user.getId());
        userDao.save(info);
    }
}
