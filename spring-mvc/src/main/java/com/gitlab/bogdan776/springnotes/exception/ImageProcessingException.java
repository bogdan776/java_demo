package com.gitlab.bogdan776.springnotes.exception;

public class ImageProcessingException extends AppException {

    public ImageProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
