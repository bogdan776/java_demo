package com.gitlab.bogdan776.springnotes.filestorage;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.net.URI;
import java.util.function.Consumer;

@Service
@RequiredArgsConstructor
public class DefaultUploadResource extends AbstractUploadResource {

    private final ResourceKeyMapper mapper;
    private final FileStorageService fileStorageService;
    private final ImageCompressor compressor;

    @Override
    public URI execute(MultipartFile file, String pathFormat, String name, Consumer<ResourceKeyJson> resourceKeyJsonConsumer) {
        InputStream stream = getInputStream(file);
        String contentType = getContentType(stream);
        String extension = getExtension(contentType);
        InputStream compressed = compressor.compressible(contentType) ? compressor.compress(stream, extension) : stream;
        String path = String.format(pathFormat, name, extension);
        ResourceKey resourceKey = fileStorageService.upload(path, compressed);
        ResourceKeyJson key = mapper.mapResourceKey(resourceKey);
        resourceKeyJsonConsumer.accept(key);
        return getUrlWithRnd(resourceKey);
    }
}
