package com.gitlab.bogdan776.springnotes.entity.note;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class Note {

    private UUID id;
    private UUID authorId;
    private String title;
    private String content;
    private LocalDateTime createdAt;
}
