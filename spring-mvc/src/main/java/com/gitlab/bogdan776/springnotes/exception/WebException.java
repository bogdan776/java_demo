package com.gitlab.bogdan776.springnotes.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

@Getter
public abstract class WebException extends AppException {

    private final HttpStatusCode statusCode;

    public WebException() {
        this.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public WebException(HttpStatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public WebException(String message, HttpStatusCode statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public WebException(String message, Throwable cause, HttpStatusCode statusCode) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    public WebException(Throwable cause, HttpStatusCode statusCode) {
        super(cause);
        this.statusCode = statusCode;
    }
}
