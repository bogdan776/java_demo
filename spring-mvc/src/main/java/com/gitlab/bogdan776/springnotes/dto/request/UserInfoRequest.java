package com.gitlab.bogdan776.springnotes.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
@ToString
public class UserInfoRequest {

    @Length(max = 50)
    private String name;

    @Length(max = 50)
    private String surname;

    @Length(max = 50)
    private String address;

    @Length(max = 100)
    private String instagram;

    @Length(max = 100)
    private String facebook;

    @Length(max = 100)
    private String twitter;

    @Length(max = 500)
    private String biography;
}
