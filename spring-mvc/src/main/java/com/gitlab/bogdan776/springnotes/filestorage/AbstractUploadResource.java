package com.gitlab.bogdan776.springnotes.filestorage;

import com.gitlab.bogdan776.springnotes.exception.DetectMediaException;
import com.gitlab.bogdan776.springnotes.exception.ImageProcessingException;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public abstract class AbstractUploadResource implements UploadResourceUseCase {

    private static final String QUERY_RND_PARAM = "rnd";

    public String getExtension(String contentType) {
        TikaConfig config = TikaConfig.getDefaultConfig();
        try {
            MimeType mimeType = config.getMimeRepository().forName(contentType);
            return mimeType.getExtension();
        } catch (MimeTypeException e) {
            throw new ImageProcessingException("Can't get mime type", e);
        }
    }

    public String getContentType(InputStream inputStream) {
        try {
            Tika tika = new Tika();
            return tika.detect(inputStream);
        } catch (IOException e) {
            throw new DetectMediaException(e);
        }
    }

    public URI getUrlWithRnd(ResourceKey resourceKey) {
        UriComponents resourceUri = UriComponentsBuilder
            .fromHttpUrl(resourceKey.getUrl().toString())
            .queryParam(QUERY_RND_PARAM, System.currentTimeMillis())
            .build();
        return resourceUri.toUri();
    }

    public InputStream getInputStream(MultipartFile multipartFile) {
        try {
            return multipartFile.getInputStream();
        } catch (IOException e) {
            throw new ImageProcessingException("Can't get an InputStream", e);
        }
    }
}
