package com.gitlab.bogdan776.springnotes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class WrongPasswordException extends WebException {

    private static final String MESSAGE = "Wrong password. Password length must be between 8 and 60 characters. "
        + "May contain letters, numbers, and the following characters [#, !, %, ^, &, _, -]";

    public WrongPasswordException() {
        super(MESSAGE, HttpStatus.FORBIDDEN);
    }

    public WrongPasswordException(String message) {
        super(message, HttpStatus.FORBIDDEN);
    }
}
