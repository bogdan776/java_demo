package com.gitlab.bogdan776.springnotes.filestorage;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface ResourceKeyMapper {

    @Mapping(target = "properties", expression = "java(resourceKey.properties())")
    ResourceKeyJson mapResourceKey(ResourceKey resourceKey);

    default ResourceKey mapResourceKey(ResourceKeyJson resourceKey) {
        return ResourceKey.builder()
            .url(resourceKey.getUrl())
            .path(resourceKey.getPath())
            .properties(resourceKey.getProperties())
            .build();
    }
}
