package com.gitlab.bogdan776.springnotes.controller.user;

import com.gitlab.bogdan776.springnotes.dto.request.UserInfoRequest;
import com.gitlab.bogdan776.springnotes.dto.response.UserInfoResponse;
import com.gitlab.bogdan776.springnotes.dto.response.UserResponse;
import com.gitlab.bogdan776.springnotes.security.SecurityController;
import com.gitlab.bogdan776.springnotes.usecase.user.EditUserInfoUseCase;
import com.gitlab.bogdan776.springnotes.usecase.user.GetUserProfileUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class EditUserInfoController extends SecurityController implements BaseUserController {

    private final GetUserProfileUseCase getUserProfile;
    private final EditUserInfoUseCase editUserInfo;

    @GetMapping("/profile/edit")
    public String getUserProfile(Model model) {
        UserResponse user = getUserProfile.execute(getUsername());
        UserInfoRequest defaultRequest = setInitialUserInfoRequestParameters(user.getInfo());
        model.addAttribute("user", user);
        model.addAttribute("userInfoRequest", defaultRequest);
        return "user_profile_editor";
    }

    @PostMapping("/profile/edit")
    public String editUserInfo(@ModelAttribute @Validated UserInfoRequest request) {
        editUserInfo.execute(getUserId(), request);
        return "redirect:/users/profile/edit";
    }

    private UserInfoRequest setInitialUserInfoRequestParameters(UserInfoResponse info) {
        UserInfoRequest defaultRequest = new UserInfoRequest();
        defaultRequest.setName(info.getName());
        defaultRequest.setSurname(info.getSurname());
        defaultRequest.setAddress(info.getAddress());
        defaultRequest.setInstagram(info.getInstagram());
        defaultRequest.setFacebook(info.getFacebook());
        defaultRequest.setTwitter(info.getTwitter());
        defaultRequest.setBiography(info.getBiography());
        return defaultRequest;
    }
}
