package com.gitlab.bogdan776.springnotes.dao.user;

import com.gitlab.bogdan776.springnotes.dao.user.repository.UserInfoRepository;
import com.gitlab.bogdan776.springnotes.dao.user.repository.UserRepository;
import com.gitlab.bogdan776.springnotes.entity.user.User;
import com.gitlab.bogdan776.springnotes.entity.user.UserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class DefaultUserDao implements UserDao {

    private final UserRepository userRepository;
    private final UserInfoRepository userInfoRepository;

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public Optional<UserInfo> findInfoByUserId(UUID userId) {
        return userInfoRepository.findByUserId(userId);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void save(UserInfo info) {
        userInfoRepository.save(info);
    }

    @Override
    public void update(UserInfo info) {
        userInfoRepository.update(info);
    }
}
