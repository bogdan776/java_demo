package com.gitlab.bogdan776.springnotes.controller.user;

import com.gitlab.bogdan776.springnotes.security.SecurityController;
import com.gitlab.bogdan776.springnotes.usecase.user.UploadUserAvatarUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequiredArgsConstructor
public class UploadUserAvatarController extends SecurityController implements BaseUserController {

    private final UploadUserAvatarUseCase uploadUserAvatar;

    @PostMapping(value = "/avatar", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String uploadUserAvatar(@RequestParam("avatar") MultipartFile avatar) {
        uploadUserAvatar.execute(getUserId(), avatar);
        return "redirect:/users/profile/edit";
    }
}
