package com.gitlab.bogdan776.springnotes.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gitlab.bogdan776.springnotes.exception.JsonException;

import java.util.TimeZone;

public final class JsonUtils {

    private final static ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.registerModule(new JavaTimeModule());
        MAPPER.setTimeZone(TimeZone.getDefault());
    }

    private JsonUtils() {
    }

    public static <E> String toJson(E object) {
        if (object == null) {
            return null;
        }
        try {
            return MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException ex) {
            throw new JsonException("Cannot write object to json", ex);
        }
    }

    public static <E> E fromJson(String json, Class<E> valueType) {
        if (json == null) {
            return null;
        }
        try {
            return MAPPER.readValue(json, valueType);
        } catch (JsonProcessingException e) {
            throw new JsonException("Cannot parse json", e);
        }
    }
}
