package com.gitlab.bogdan776.springnotes.usecase.auth;

import com.gitlab.bogdan776.springnotes.dto.request.SignUpRequest;

public interface SignUpUseCase {

    void execute(SignUpRequest request);
}
