package com.gitlab.bogdan776.springnotes.filestorage.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.gitlab.bogdan776.springnotes.exception.FileStorageException;
import com.gitlab.bogdan776.springnotes.filestorage.FileStorageService;
import com.gitlab.bogdan776.springnotes.filestorage.ResourceKey;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
@PropertySource("classpath:aws.properties")
public class AmazonS3FileStorageService implements FileStorageService {

    private final AmazonS3 amazonS3;

    @Value("${aws.s3.bucket}")
    private String bucketName;

    @Override
    public ResourceKey upload(String path, InputStream inputStream) {
        return upload(path, inputStream, false);
    }

    @Override
    public ResourceKey upload(String path, InputStream inputStream, boolean authenticatedRead) {
        ResourceKey key = ResourceKey.builder()
            .path(path)
            .property(ResourceKey.AWS_BUCKET_NAME, bucketName)
            .build();
        try {
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, path, inputStream, new ObjectMetadata());
            putObjectRequest.withCannedAcl(
                authenticatedRead
                    ? CannedAccessControlList.AuthenticatedRead
                    : CannedAccessControlList.PublicRead
            );
            amazonS3.putObject(putObjectRequest);
            key.setUrl(amazonS3.getUrl(bucketName, path));
        } catch (RuntimeException e) {
            log.error(e.getMessage(), e);
            throw new FileStorageException(e);
        }
        return key;
    }

    @Override
    public InputStream download(ResourceKey resourceKey) {
        validateResourceKey(resourceKey);
        try {
            String bucket = resourceKey.getProperty(ResourceKey.AWS_BUCKET_NAME);
            S3Object s3Object = amazonS3.getObject(new GetObjectRequest(bucket, resourceKey.getPath()));
            return s3Object.getObjectContent();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new FileStorageException(e);
        }
    }

    @Override
    public void deleteFile(ResourceKey key) {
        validateResourceKey(key);
        try {
            String bucket = key.getProperty(ResourceKey.AWS_BUCKET_NAME);
            amazonS3.deleteObject(bucket, key.getPath());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new FileStorageException(e);
        }
    }

    @Override
    public boolean isFileExist(ResourceKey key) {
        validateResourceKey(key);
        String bucket = key.getProperty(ResourceKey.AWS_BUCKET_NAME);
        return amazonS3.doesObjectExist(bucket, key.getPath());
    }

    private void validateResourceKey(ResourceKey key) {
        Objects.requireNonNull(key.getPath(), "path");
        Objects.requireNonNull(key.getProperty(ResourceKey.AWS_BUCKET_NAME), "bucketName");
    }
}
