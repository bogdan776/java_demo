package com.gitlab.bogdan776.springnotes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(
    code = HttpStatus.BAD_REQUEST,
    reason = "User with this username or email already exists"
)
public class UserAlreadyExistsException extends WebException {

    public UserAlreadyExistsException() {
        super(HttpStatus.BAD_REQUEST);
    }
}
