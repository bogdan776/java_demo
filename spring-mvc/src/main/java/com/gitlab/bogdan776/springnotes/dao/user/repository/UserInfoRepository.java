package com.gitlab.bogdan776.springnotes.dao.user.repository;

import com.gitlab.bogdan776.springnotes.common.util.JsonUtils;
import com.gitlab.bogdan776.springnotes.entity.user.UserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class UserInfoRepository {

    private final JdbcTemplate jdbcTemplate;

    public Optional<UserInfo> findByUserId(UUID userId) {
        return jdbcTemplate.queryForStream(
            "select * from user_info where user_id = ?", new UserInfoRowMapper(), userId
        ).findFirst();
    }

    public void update(UserInfo info) {
        jdbcTemplate.update(
            "update user_info set avatar = ?::jsonb, name = ?, surname = ?, address = ?, "
                + "instagram = ?, facebook = ?, twitter = ?, biography = ? where user_id = ?",
            JsonUtils.toJson(info.getAvatar()), info.getName(), info.getSurname(), info.getAddress(), info.getInstagram(),
            info.getFacebook(), info.getTwitter(), info.getBiography(), info.getUserId()
        );
    }

    public void save(UserInfo info) {
        SqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("user_id", info.getUserId())
            .addValue("avatar", JsonUtils.toJson(info.getAvatar()))
            .addValue("name", info.getName())
            .addValue("surname", info.getSurname())
            .addValue("address", info.getAddress())
            .addValue("instagram", info.getInstagram())
            .addValue("facebook", info.getFacebook())
            .addValue("twitter", info.getTwitter())
            .addValue("biography", info.getBiography());
        new SimpleJdbcInsert(jdbcTemplate)
            .withTableName("user_info")
            .execute(parameters);
    }
}
