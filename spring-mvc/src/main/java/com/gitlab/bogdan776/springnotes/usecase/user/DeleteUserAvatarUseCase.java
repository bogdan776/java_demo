package com.gitlab.bogdan776.springnotes.usecase.user;

import java.util.UUID;

public interface DeleteUserAvatarUseCase {

    void execute(UUID userId);
}
