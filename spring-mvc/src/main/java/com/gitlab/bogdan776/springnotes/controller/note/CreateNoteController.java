package com.gitlab.bogdan776.springnotes.controller.note;

import com.gitlab.bogdan776.springnotes.dto.request.NoteRequest;
import com.gitlab.bogdan776.springnotes.security.SecurityController;
import com.gitlab.bogdan776.springnotes.usecase.note.CreateNoteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class CreateNoteController extends SecurityController implements BaseNoteController {

    private final CreateNoteUseCase createNote;

    @GetMapping("/add")
    public String getForm(Model model) {
        model.addAttribute(new NoteRequest());
        return "note_creator";
    }

    @PostMapping
    public String createNote(@Validated NoteRequest request) {
        createNote.execute(getUserId(), request);
        return "redirect:/notes";
    }
}
