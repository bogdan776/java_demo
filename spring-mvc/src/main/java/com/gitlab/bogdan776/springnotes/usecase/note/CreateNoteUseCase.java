package com.gitlab.bogdan776.springnotes.usecase.note;

import com.gitlab.bogdan776.springnotes.dto.request.NoteRequest;

import java.util.UUID;

public interface CreateNoteUseCase {

    void execute(UUID userId, NoteRequest request);
}
