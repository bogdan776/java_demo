<%@ include file="jspf/header.jspf" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Editor</title>
    <style>
        <%@ include file="css/note_editor.css"%>
    </style>
</head>
<body>
<div class="container">
    <h2 style="color: #2d6f84;">Edit note:</h2>
    <form:form method="post" action="${pageContext.request.contextPath}/notes/${note.id}/edit"
               modelAttribute="noteRequest">
        <form:label path="title">Title:</form:label>
        <form:textarea cssClass="title-input" path="title"/>
        <form:label path="content">Content:</form:label>
        <form:textarea cssClass="text-input" path="content"/>

        <button class="save-button" type="submit">Save</button>
    </form:form>
    <form method="post" action="${pageContext.request.contextPath}/notes/${note.id}/delete">
        <button class="delete-button" type="submit">Delete</button>
    </form>
</div>
</body>
</html>
