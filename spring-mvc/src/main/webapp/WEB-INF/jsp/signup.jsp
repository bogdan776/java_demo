<%@ include file="jspf/header.jspf" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Sign up</title>
    <style>
        <%@ include file="css/auth.css"%>
    </style>
</head>
<body>
<div style="color: #2d6f84; text-align: center;">
    <h1>Hello!</h1>
    <h2>Let's create a new user!</h2>
</div>
<form:form cssClass="auth-form" method="post" action="${pageContext.request.contextPath}/signup"
           modelAttribute="signUpRequest">
    <div class="input-container">
        <form:label path="username">Username:</form:label>
        <form:input required="true" type="text" path="username" placeholder="Username"/>
        <form:label path="email">Email:</form:label>
        <form:input required="true" type="text" path="email" placeholder="Email"/>
        <form:label path="password">Password:</form:label>
        <form:input required="true" type="password" path="password" placeholder="Password"/>
        <form:label path="passwordRepeat">Repeat password:</form:label>
        <form:input required="true" type="password" path="passwordRepeat" placeholder="Repeat password"/>
        <form:button>Sign up</form:button>
        <a href="${pageContext.request.contextPath}/login">Login</a>
    </div>
</form:form>
</body>
</html>
