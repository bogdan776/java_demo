<%@ include file="jspf/header.jspf" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>User profile</title>
    <style>
        <%@ include file="css/user_profile.css"%>
    </style>
</head>
<body>
<div class="marker">
    <h3>Avatar</h3>
</div>
<div class="center_container">
    <c:choose>
        <c:when test="${user.info.avatarUrl == null}">
            <h3>Avatar:)</h3>
        </c:when>
        <c:otherwise>
            <img class="container" src="${user.info.avatarUrl}" alt="Avatar">
        </c:otherwise>
    </c:choose>
</div>
<div class="marker">
    <h3>Info:</h3>
</div>
<div class="center_container">
    <table class="info">
        <tbody>
        <tr>
            <td>Username:</td>
            <td>${user.username}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>${user.email}</td>
        </tr>
        <tr>
            <td>Joined:</td>
            <td>${user.createdAt}</td>
        </tr>
        <c:if test="${user.info.name != null && !user.info.name.blank}">
            <tr>
                <td>Name:</td>
                <td>${user.info.name}</td>
            </tr>
        </c:if>
        <c:if test="${user.info.surname != null && !user.info.surname.blank}">
            <tr>
                <td>Surname:</td>
                <td>${user.info.surname}</td>
            </tr>
        </c:if>
        <c:if test="${user.info.address != null && !user.info.address.blank}">
            <tr>
                <td>Address:</td>
                <td>${user.info.address}</td>
            </tr>
        </c:if>
        <c:if test="${user.info.instagram != null && !user.info.instagram.blank}">
            <tr>
                <td><a href="${user.info.instagram}">Instagram profile</a></td>
            </tr>
        </c:if>
        <c:if test="${user.info.facebook != null && !user.info.facebook.blank}">
            <tr>
                <td><a href="${user.info.facebook}">Facebook profile</a></td>
            </tr>
        </c:if>
        <c:if test="${user.info.twitter != null && !user.info.twitter.blank}">
            <tr>
                <td><a href="${user.info.twitter}">Twitter profile</a></td>
            </tr>
        </c:if>
        <c:if test="${user.info.biography != null && !user.info.biography.blank}">
            <tr>
                <td>${user.info.biography}</td>
            </tr>
        </c:if>
        </tbody>
    </table>
</div>
</body>
</html>
