<%@ include file="jspf/header.jspf" %>
<html>
<head>
    <title>Notes</title>
    <style>
        <%@ include file="css/notes.css"%>
    </style>
</head>
<body>
<div class="container">
    <c:choose>
        <c:when test="${empty notes}">
            <h2 style="color: #2d6f84;">You do not have any notes yet</h2>
        </c:when>
        <c:otherwise>
            <h2 style="color: #2d6f84;">Notes:</h2>
            <table class="note-table">
                <thead>
                <tr>
                    <td style="width: 15%"><i>Title</i></td>
                    <td style="width: 50%"><i>Content</i></td>
                    <td style="width: 25%"><i>Created</i></td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="note" items="${notes}">
                    <tr>
                        <td>${note.title}</td>
                        <td>${note.content}</td>
                        <td>${note.createdAt}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/notes/${note.id}/edit">Edit</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:otherwise>
    </c:choose>
    <a href="${pageContext.request.contextPath}/notes/add"><h3 style="color: #2d6f84;">Add note</h3></a>
</div>
</body>
</html>
