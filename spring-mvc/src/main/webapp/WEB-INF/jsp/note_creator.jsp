<%@ include file="jspf/header.jspf" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Creator</title>
    <style>
        <%@ include file="css/note_editor.css"%>
    </style>
</head>
<body>
<div class="container">
    <h2 style="color: #2d6f84;">Create note:</h2>
    <form:form method="post" action="${pageContext.request.contextPath}/notes" modelAttribute="noteRequest">
        <form:label path="title">Title:</form:label>
        <form:textarea cssClass="title-input" required="true" path="title"/>
        <form:label path="content">Content:</form:label>
        <form:textarea cssClass="text-input" required="true" path="content"/>
        <button class="save-button" type="submit">Save</button>
    </form:form>
</div>
</body>
</html>
