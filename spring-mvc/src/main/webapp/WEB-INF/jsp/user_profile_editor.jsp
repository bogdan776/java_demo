<%@ include file="jspf/header.jspf" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>User profile editor</title>
    <style>
        <%@ include file="css/user_profile_editor.css"%>
    </style>
</head>
<body>
<h2 style="color: #2d6f84; text-align: center;">Profile editor</h2>
<div class="center_container">
    <h3 style="color: #2d6f84;">Avatar</h3>

    <c:choose>
        <c:when test="${user.info.avatarUrl == null}">
            <h3 style="color: #2d6f84; text-align: center;">Avatar:)</h3>
        </c:when>
        <c:otherwise>
            <img class="container" src="${user.info.avatarUrl}" alt="Avatar">
            <form method="post" action="${pageContext.request.contextPath}/users/avatar/delete">
                <button class="delete" type="submit">Delete avatar</button>
            </form>
        </c:otherwise>
    </c:choose>
    <form method="post" action="${pageContext.request.contextPath}/users/avatar" enctype="multipart/form-data">
        <input type="file" name="avatar">
        <button class="save" type="submit">Upload</button>
    </form>
</div>
<div class="center_container">
    <h3 style="color: #2d6f84;">Info:</h3>
    <form:form method="post" action="${pageContext.request.contextPath}/users/profile/edit"
               modelAttribute="userInfoRequest">
        <form:label path="name">Name:</form:label>
        <form:input cssClass="text_input" type="text" path="name"/>

        <form:label path="surname">Surname:</form:label>
        <form:input cssClass="text_input" type="text" path="surname"/>

        <form:label path="address">Address:</form:label>
        <form:input cssClass="text_input" type="text" path="address"/>

        <form:label path="instagram">Instagram:</form:label>
        <form:input cssClass="text_input" type="text" path="instagram"/>

        <form:label path="facebook">Facebook:</form:label>
        <form:input cssClass="text_input" type="text" path="facebook"/>

        <form:label path="twitter">Twitter:</form:label>
        <form:input cssClass="text_input" type="text" path="twitter"/>

        <form:label path="biography">Biography:</form:label>
        <form:textarea cssClass="text_input" path="biography"/>

        <button class="save" type="submit">Save</button>
    </form:form>
</div>
</body>
</html>
