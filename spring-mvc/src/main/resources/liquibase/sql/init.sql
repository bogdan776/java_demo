create table if not exists user_profile
(
    id         uuid      not null,
    username   varchar   not null,
    email      varchar   not null,
    "password" varchar   not null,
    "role"     varchar   not null,
    created_at timestamp not null,
    constraint user_profile_pk primary key (id),
    constraint user_profile_uq_username unique (username)
);

create table if not exists note
(
    id         uuid      not null,
    author_id  uuid      not null,
    title      varchar   not null,
    content    varchar   not null,
    created_at timestamp not null,
    constraint note_pk primary key (id),
    constraint note_fk_author foreign key (author_id) references user_profile
);
