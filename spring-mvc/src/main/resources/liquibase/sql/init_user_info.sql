create table if not exists user_info
(
    user_id   uuid not null,
    avatar    jsonb,
    name      varchar,
    surname   varchar,
    address   varchar,
    instagram varchar,
    facebook  varchar,
    twitter   varchar,
    biography varchar,
    constraint user_info_pk primary key (user_id)
);
