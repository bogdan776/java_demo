package com.gitlab.bogdan776.webflux.dto.response;

import com.gitlab.bogdan776.webflux.entity.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {

    private UUID id;
    private String username;
    private String email;
    private UserRole role;
    private LocalDateTime createdAt;
}
