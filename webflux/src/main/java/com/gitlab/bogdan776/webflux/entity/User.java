package com.gitlab.bogdan776.webflux.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Use {@link Table @Table} if the java class name does not match the table name.
 * Rename the User to UserProfile to remove the {@link Table @Table}.
 * <p>
 * {@link Persistable} needed for the 'insert' sql operation when executing the save and saveAll methods
 * from the interface {@link org.springframework.data.repository.reactive.ReactiveCrudRepository ReactiveCrudRepository}.
 * By default, only 'update' is executed.
 * </p>
 * <p>
 * {@link Column @Column} specifies the name of the column in the database.
 * Used if the column name and field are different.
 * </p>
 */
@Getter
@Setter
@ToString
@Table("user_profile")
public class User implements Persistable<UUID> {

    @Id
    private UUID id;
    private String username;
    private String email;
    private String password;
    private UserRole role;
    @Column("created_at")
    private LocalDateTime createdAt;

    @Override
    public boolean isNew() {
        if (id == null && createdAt == null) {
            id = UUID.randomUUID();
            createdAt = LocalDateTime.now();
            return true;
        }
        return false;
    }
}
