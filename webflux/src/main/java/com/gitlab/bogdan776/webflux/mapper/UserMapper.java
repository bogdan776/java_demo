package com.gitlab.bogdan776.webflux.mapper;

import com.gitlab.bogdan776.webflux.dto.response.UserResponse;
import com.gitlab.bogdan776.webflux.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public UserResponse mapUser(User user) {
        return new UserResponse(
            user.getId(), user.getUsername(), user.getEmail(),
            user.getRole(), user.getCreatedAt()
        );
    }
}
