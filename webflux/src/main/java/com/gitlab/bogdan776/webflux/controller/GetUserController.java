package com.gitlab.bogdan776.webflux.controller;

import com.gitlab.bogdan776.webflux.dto.response.UserResponse;
import com.gitlab.bogdan776.webflux.mapper.UserMapper;
import com.gitlab.bogdan776.webflux.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class GetUserController implements BaseUserController {

    private final UserRepository userRepository;
    private final UserMapper mapper;

    @GetMapping("/{userId}")
    public Mono<UserResponse> findUserById(@PathVariable("userId") UUID userId) {
        return userRepository.findById(userId)
            .map(mapper::mapUser);
    }
}
