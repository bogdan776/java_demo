package com.gitlab.bogdan776.webflux.controller;

import com.gitlab.bogdan776.webflux.entity.User;
import com.gitlab.bogdan776.webflux.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class SaveUsersController implements BaseUserController {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @PostMapping(produces = APPLICATION_JSON_VALUE)
    public Mono<Void> saveUsers(@RequestBody Flux<User> requestFlux) {
        Flux<User> pwChanged = requestFlux.doOnNext(
            user -> user.setPassword(passwordEncoder.encode(user.getPassword()))
        );
        return userRepository.saveAll(pwChanged).then();
    }
}
