package com.gitlab.bogdan776.webflux.controller;

import com.gitlab.bogdan776.webflux.dto.response.UserResponse;
import com.gitlab.bogdan776.webflux.mapper.UserMapper;
import com.gitlab.bogdan776.webflux.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequiredArgsConstructor
public class GetUsersController implements BaseUserController {

    private final UserRepository userRepository;
    private final UserMapper mapper;

    @GetMapping
    public Flux<UserResponse> findAllUsers() {
        return userRepository.findAll()
            .map(mapper::mapUser);
    }
}
