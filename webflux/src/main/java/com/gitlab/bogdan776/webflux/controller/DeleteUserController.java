package com.gitlab.bogdan776.webflux.controller;

import com.gitlab.bogdan776.webflux.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class DeleteUserController implements BaseUserController {

    private final UserRepository userRepository;

    @DeleteMapping("/{userId}")
    public Mono<Void> deleteUser(@PathVariable("userId") UUID userId) {
        return userRepository.deleteById(userId);
    }
}
