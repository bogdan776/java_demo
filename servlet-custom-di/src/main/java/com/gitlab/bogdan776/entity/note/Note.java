package com.gitlab.bogdan776.entity.note;

import com.gitlab.bogdan776.annotation.Column;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class Note {

    private UUID id;
    @Column(name = "author_id")
    private UUID authorId;
    private String title;
    private String text;
    @Column(name = "create_at")
    private LocalDateTime createAt;
}
