package com.gitlab.bogdan776.dao;

import com.gitlab.bogdan776.context.ApplicationProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DefaultConnectionManager implements ConnectionManager {

    /*
    Check the field mappings in the property file (in this project datasource.properties) and those required to form the Property class.
    And you can get rid of extra fields using the method: DriverManager.getConnection(String, Properties).
    Probably need to remove the 'datasource' and maybe something else...
     */
    private final String url;
    private final String username;
    private final String password;
    private final boolean autoCommit;

    public DefaultConnectionManager() {
        ApplicationProperties properties = ApplicationProperties.load("datasource.properties");
        this.url = properties.getProperty("datasource.url");
        this.username = properties.getProperty("datasource.username");
        this.password = properties.getProperty("datasource.password");
        this.autoCommit = Boolean.parseBoolean(properties.getProperty("datasource.auto-commit"));
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(autoCommit);
        return connection;
    }
}
