package com.gitlab.bogdan776.dao.user;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.ConnectionManager;
import com.gitlab.bogdan776.dao.user.repository.UserRepository;
import com.gitlab.bogdan776.entity.user.User;
import com.gitlab.bogdan776.exception.DatabaseException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@Component
@RequiredArgsConstructor
public class DefaultUserDao implements UserDao {

    private final ConnectionManager connectionManager;
    private final UserRepository repository;

    @Override
    public Optional<User> findById(UUID id) {
        try (Connection connection = connectionManager.getConnection()) {
            return repository.findUserById(id, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public Optional<User> findByUsername(String username) {
        try (Connection connection = connectionManager.getConnection()) {
            return repository.findUserByUsername(username, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try (Connection connection = connectionManager.getConnection()) {
            return repository.findUserByEmail(email, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void save(User user) {
        try (Connection connection = connectionManager.getConnection()) {
            repository.save(user, connection);
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void update(User user) {
        try (Connection connection = connectionManager.getConnection()) {
            repository.update(user, connection);
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
