package com.gitlab.bogdan776.dao.user;

import com.gitlab.bogdan776.entity.user.User;

import java.util.Optional;
import java.util.UUID;

public interface UserDao {

    Optional<User> findById(UUID id);

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    void save(User user);

    void update(User user);
}
