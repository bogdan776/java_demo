package com.gitlab.bogdan776.dao.user.repository;

import com.gitlab.bogdan776.common.util.JdbcUtils;
import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.entity.user.User;
import com.gitlab.bogdan776.exception.DatabaseException;
import com.gitlab.bogdan776.mapper.ReflectionResultSetMapper;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@Component
public class UserRepository {

    public Optional<User> findUserById(UUID id, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_user where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                return Optional.ofNullable(ReflectionResultSetMapper.mapResultSet(resultSet, User.class));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public Optional<User> findUserByUsername(String username, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_user where username = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, username);
            try (ResultSet resultSet = statement.executeQuery()) {
                return Optional.ofNullable(ReflectionResultSetMapper.mapResultSet(resultSet, User.class));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public Optional<User> findUserByEmail(String email, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_user where email = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, email);
            try (ResultSet resultSet = statement.executeQuery()) {
                return Optional.ofNullable(ReflectionResultSetMapper.mapResultSet(resultSet, User.class));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void save(User user, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "insert into demo_user (id, username, email, \"password\", \"role\", create_at) values (?, ?, ?, ?, ?, ?)"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                user.getId(), user.getUsername(), user.getEmail(),
                user.getPassword(), user.getRole(), user.getCreateAt());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void update(User user, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "update demo_user set username = ?, email = ?, \"password\" = ?, \"role\" = ? where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                user.getUsername(), user.getEmail(), user.getPassword(),
                user.getRole(), user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
