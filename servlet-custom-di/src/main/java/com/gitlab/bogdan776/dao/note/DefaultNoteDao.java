package com.gitlab.bogdan776.dao.note;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.ConnectionManager;
import com.gitlab.bogdan776.dao.note.repository.NoteRepository;
import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.exception.DatabaseException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@Component
@RequiredArgsConstructor
public class DefaultNoteDao implements NoteDao {

    private final ConnectionManager connectionManager;
    private final NoteRepository noteRepository;

    @Override
    public Optional<Note> findById(UUID id) {
        try (Connection connection = connectionManager.getConnection()) {
            return noteRepository.findById(id, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public Optional<Note> findByIdAndAuthorId(UUID id, UUID authorId) {
        try (Connection connection = connectionManager.getConnection()) {
            return noteRepository.findByIdAndAuthorId(id, authorId, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public List<Note> findAllByAuthor(UUID authorId) {
        try (Connection connection = connectionManager.getConnection()) {
            return noteRepository.findAllByAuthorId(authorId, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void save(Note note) {
        try (Connection connection = connectionManager.getConnection()) {
            noteRepository.save(note, connection);
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void update(Note note) {
        try (Connection connection = connectionManager.getConnection()) {
            noteRepository.update(note, connection);
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(Note note) {
        try (Connection connection = connectionManager.getConnection()) {
            noteRepository.delete(note, connection);
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
