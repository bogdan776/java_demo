package com.gitlab.bogdan776.dao;

import com.gitlab.bogdan776.context.ApplicationProperties;
import com.gitlab.bogdan776.context.Component;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

@Component
public final class HikariConnectionPool implements ConnectionManager {

    private final HikariDataSource dataSource;

    public HikariConnectionPool() {
        ApplicationProperties properties = ApplicationProperties.load("datasource.properties");
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("datasource.url"));
        config.setUsername(properties.getProperty("datasource.username"));
        config.setPassword(properties.getProperty("datasource.password"));
        config.setAutoCommit(Boolean.parseBoolean(properties.getProperty("datasource.auto-commit")));
        config.setDriverClassName(properties.getProperty("datasource.driver-class-name"));
        this.dataSource = new HikariDataSource(config);
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
