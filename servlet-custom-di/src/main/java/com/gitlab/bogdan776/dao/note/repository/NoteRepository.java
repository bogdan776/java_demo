package com.gitlab.bogdan776.dao.note.repository;

import com.gitlab.bogdan776.common.util.JdbcUtils;
import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.exception.DatabaseException;
import com.gitlab.bogdan776.mapper.ReflectionResultSetMapper;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@Component
public class NoteRepository {

    public Optional<Note> findById(UUID id, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_note where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                return Optional.ofNullable(ReflectionResultSetMapper.mapResultSet(resultSet, Note.class));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public Optional<Note> findByIdAndAuthorId(UUID id, UUID authorId, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_note where id = ? and author_id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, id, authorId);
            try (ResultSet resultSet = statement.executeQuery()) {
                return Optional.ofNullable(ReflectionResultSetMapper.mapResultSet(resultSet, Note.class));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public List<Note> findAllByAuthorId(UUID authorId, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_note where author_id = ? order by create_at desc"
        )) {
            JdbcUtils.fillPreparedStatement(statement, authorId);
            try (ResultSet resultSet = statement.executeQuery()) {
                return ReflectionResultSetMapper.mapMultilineResultSet(resultSet, Note.class);
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void save(Note note, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "insert into demo_note (id, author_id, title, \"text\", create_at) values (?, ?, ?, ?, ?)"
        )) {
            JdbcUtils.fillPreparedStatement(
                statement, note.getId(), note.getAuthorId(), note.getTitle(), note.getText(), note.getCreateAt()
            );
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void update(Note note, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "update demo_note set title = ?, text = ? where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, note.getTitle(), note.getText(), note.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void delete(Note note, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "delete from demo_note where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, note.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
