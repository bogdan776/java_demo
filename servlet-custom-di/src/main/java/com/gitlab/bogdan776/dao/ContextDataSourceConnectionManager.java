package com.gitlab.bogdan776.dao;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class ContextDataSourceConnectionManager implements ConnectionManager {

    private final DataSource dataSource;

    public ContextDataSourceConnectionManager() throws NamingException {
        Context context = new InitialContext();
        Context envContext = (Context) context.lookup("java:/comp/env");
        this.dataSource = (DataSource) envContext.lookup("datasource");
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
