package com.gitlab.bogdan776.context;

import java.util.List;

interface Context {

    <T> T get(Class<T> type);

    List<Object> getAllByTypes(Class<?>... types);

    void add(Class<?> type, Object value);

    void add(Object value, Class<?>... types);

    boolean contains(Class<?>... types);
}
