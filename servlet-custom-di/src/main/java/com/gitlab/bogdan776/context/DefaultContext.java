package com.gitlab.bogdan776.context;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class DefaultContext implements Context {

    private final Map<Class<?>, Object> context = new HashMap<>();

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(Class<T> type) {
        return (T) context.get(type);
    }

    @Override
    public List<Object> getAllByTypes(Class<?>... types) {
        return Arrays.stream(types)
            .filter(context::containsKey)
            .map(context::get)
            .collect(Collectors.toList());
    }

    @Override
    public void add(Class<?> type, Object value) {
        context.put(type, value);
    }

    @Override
    public void add(Object value, Class<?>[] types) {
        for (Class<?> type : types) {
            add(type, value);
        }
    }

    @Override
    public boolean contains(Class<?>... types) {
        return Arrays.stream(types)
            .allMatch(context::containsKey);
    }
}
