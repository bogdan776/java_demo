package com.gitlab.bogdan776.context;

import com.gitlab.bogdan776.exception.ContextException;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Arrays;

@Log4j2
class ComponentData {

    private final Class<?> type;
    private final Constructor<?> constructor;

    private final boolean hasDefaultConstructor;
    @Getter
    private final Class<?>[] constructorParams;
    @Getter
    private final int parameterCount;
    @Getter
    private final Class<?>[] superTypes;

    public ComponentData(Class<?> type) {
        this.type = type;
        this.constructor = type.getDeclaredConstructors()[0];
        this.hasDefaultConstructor = (this.parameterCount = constructor.getParameterCount()) == 0;
        this.constructorParams = constructor.getParameterTypes();
        this.superTypes = extractSuperTypes();
    }

    public boolean hasDefaultConstructor() {
        return hasDefaultConstructor;
    }

    public Object create(Object[] initArgs) {
        constructor.setAccessible(true);
        try {
            return constructor.newInstance(initArgs);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            log.error(e.getMessage(), e);
            throw new ContextException(e.getMessage(), e);
        }
    }

    private Class<?>[] extractSuperTypes() {
        Class<?>[] interfaces = type.getInterfaces();
        if (interfaces.length != 0) {
            Class<?>[] interfacesWithCurrentType = Arrays.copyOf(interfaces, interfaces.length + 1);
            interfacesWithCurrentType[interfaces.length] = type;
            return interfacesWithCurrentType;
        }
        return getSuperclass();
    }

    private Class<?>[] getSuperclass() {
        Class<?> superclass = type.getSuperclass();
        return Modifier.isAbstract(superclass.getModifiers()) || superclass == Object.class
            ? new Class[]{type}
            : new Class[]{type, superclass};
    }
}
