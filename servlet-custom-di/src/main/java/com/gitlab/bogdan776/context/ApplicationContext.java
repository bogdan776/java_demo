package com.gitlab.bogdan776.context;

public interface ApplicationContext {

    String ATTRIBUTE_NAME = "applicationContext";

    <T> T getInstance(Class<T> type);
}
