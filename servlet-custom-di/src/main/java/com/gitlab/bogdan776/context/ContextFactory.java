package com.gitlab.bogdan776.context;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

interface ContextFactory {

    void addDependenciesBeforeInitialization(Supplier<Map<Class<?>, Object>> dependencies);

    void initialize(List<Class<?>> classes);

    Context getContext();
}
