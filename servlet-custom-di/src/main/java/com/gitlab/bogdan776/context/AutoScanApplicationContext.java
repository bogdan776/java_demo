package com.gitlab.bogdan776.context;

import com.gitlab.bogdan776.common.util.ClassLoaderUtils;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class AutoScanApplicationContext implements ApplicationContext {

    private final Context context;

    private AutoScanApplicationContext(Context context) {
        this.context = context;
    }

    public static ApplicationContext scanPackage(String packageName, Supplier<Map<Class<?>, Object>> dependencies) {
        ContextFactory contextFactory = new DefaultContextFactory(new DefaultContext());
        contextFactory.addDependenciesBeforeInitialization(dependencies);
        List<Class<?>> classes = ClassLoaderUtils.loadClassesByPackageAndAnnotation(packageName, Component.class);
        contextFactory.initialize(classes);
        return new AutoScanApplicationContext(contextFactory.getContext());
    }

    @Override
    public <T> T getInstance(Class<T> type) {
        return context.get(type);
    }
}
