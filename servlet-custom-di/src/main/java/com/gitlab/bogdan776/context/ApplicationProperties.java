package com.gitlab.bogdan776.context;

import com.gitlab.bogdan776.common.util.ClassLoaderUtils;
import com.gitlab.bogdan776.exception.ContextException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public final class ApplicationProperties extends Properties {

    public ApplicationProperties(String propertiesName) {
        loadProperties(propertiesName);
    }

    public static ApplicationProperties load(String propertiesName) {
        return new ApplicationProperties(propertiesName);
    }

    private void loadProperties(String propertiesName) {
        try (FileReader reader = new FileReader(getPath(propertiesName))) {
            load(reader);
        } catch (IOException e) {
            throw new ContextException("Cannot load properties", e);
        }
    }

    private String getPath(String fileName) {
        return Objects.requireNonNull(
            ClassLoaderUtils.threadContextClassLoader()
                .getResource(fileName)
        ).getPath().replace("%20", " ");
    }
}
