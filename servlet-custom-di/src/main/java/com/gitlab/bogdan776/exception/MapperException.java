package com.gitlab.bogdan776.exception;

public class MapperException extends AppException {

    public MapperException(Throwable cause) {
        super(cause);
    }
}
