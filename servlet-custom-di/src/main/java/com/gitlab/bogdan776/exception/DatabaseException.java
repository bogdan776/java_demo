package com.gitlab.bogdan776.exception;

public class DatabaseException extends AppException {

    public DatabaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
