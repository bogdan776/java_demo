package com.gitlab.bogdan776.exception;

public class ContextException extends AppException {

    public ContextException(String message) {
        super(message);
    }

    public ContextException(Throwable cause) {
        super(cause);
    }

    public ContextException(String message, Throwable cause) {
        super(message, cause);
    }
}
