package com.gitlab.bogdan776.common.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;

public final class JdbcUtils {

    private JdbcUtils() {
    }

    public static void fillPreparedStatement(PreparedStatement statement, Object... values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            statement.setObject(i + 1, convert(values[i]));
        }
    }

    private static Object convert(Object value) {
        if (value instanceof Enum<?> enu) {
            return enu.toString();
        } else if (value instanceof Collection<?> collection) {
            return JsonUtils.toJson(collection);
        } else if (value instanceof LocalDateTime ldt) {
            return Timestamp.valueOf(ldt);
        }
        return value;
    }
}
