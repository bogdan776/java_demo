package com.gitlab.bogdan776.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gitlab.bogdan776.exception.JsonException;
import lombok.extern.log4j.Log4j2;

import java.util.TimeZone;

@Log4j2
public final class JsonUtils {

    private final static ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.registerModule(new JavaTimeModule());
        MAPPER.setTimeZone(TimeZone.getDefault());
    }

    private JsonUtils() {
    }

    public static <E> String toJson(E object) {
        try {
            return MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            throw new JsonException("Cannot write object to json", e);
        }
    }

    public static <E> E fromJson(String json, Class<E> klass) {
        try {
            return MAPPER.readValue(json, klass);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            throw new JsonException("Cannot parse json", e);
        }
    }

    public static <E> E fromJson(String json) {
        try {
            return MAPPER.readValue(json, new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            throw new JsonException("Cannot parse json", e);
        }
    }

    public static boolean isValidJson(String json) {
        try {
            MAPPER.readTree(json);
        } catch (JsonProcessingException e) {
            return false;
        }
        return true;
    }
}
