package com.gitlab.bogdan776.common.constant;

import java.util.regex.Pattern;

public final class Constants {

    public static final Pattern EMAIL_PATTERN = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}");

    public static final Pattern PASSWORD_PATTERN = Pattern.compile("[a-z0-9#?!@$%^&*_-]{8,60}");

    private Constants() {
    }
}
