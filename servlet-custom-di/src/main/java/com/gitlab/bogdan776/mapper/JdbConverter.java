package com.gitlab.bogdan776.mapper;

import com.gitlab.bogdan776.common.util.JsonUtils;
import com.gitlab.bogdan776.entity.user.UserRole;
import org.apache.commons.lang3.EnumUtils;
import org.postgresql.util.PGobject;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.function.Function;

public final class JdbConverter {

    private static final String JSONB = "jsonb";

    private static final Map<Class<?>, Function<Object, Object>> CONVERTERS;

    static {
        CONVERTERS = Map.of(
            Timestamp.class, JdbConverter::timestampToLocalDateTime,
            PGobject.class, JdbConverter::pGobjectToObjectOrString,
            String.class, JdbConverter::stringToEnum
        );
    }

    private JdbConverter() {
    }

    @SuppressWarnings("unchecked")
    public static <T> T convert(Object value) {
        return (T) JdbConverter.CONVERTERS.getOrDefault(
            value.getClass(), object -> object
        ).apply(value);
    }

    private static LocalDateTime timestampToLocalDateTime(Object value) {
        if (value instanceof Timestamp timestamp) {
            return timestamp.toLocalDateTime();
        }
        throw new IllegalArgumentException("Mismatched type");
    }

    private static Object pGobjectToObjectOrString(Object value) {
        if (value instanceof PGobject pGObject) {
            String pGValue = pGObject.getValue();
            if (JSONB.equals(pGObject.getType()) && JsonUtils.isValidJson(pGValue)) {
                return JsonUtils.fromJson(pGValue);
            }
            return pGValue;
        }
        throw new IllegalArgumentException("Mismatched type");
    }

    private static Object stringToEnum(Object value) {
        if (value instanceof String name) {
            if (EnumUtils.isValidEnum(UserRole.class, name)) {
                return EnumUtils.getEnum(UserRole.class, name);
            }
        }
        return value;
    }
}
