package com.gitlab.bogdan776.web.exception;

import jakarta.servlet.http.HttpServletResponse;

public class UserNotFoundException extends WebException {

    private static final String MESSAGE = "User not found";

    public UserNotFoundException() {
        super(MESSAGE, HttpServletResponse.SC_NOT_FOUND);
    }
}
