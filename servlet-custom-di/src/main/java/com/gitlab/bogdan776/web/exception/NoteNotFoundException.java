package com.gitlab.bogdan776.web.exception;

import jakarta.servlet.http.HttpServletResponse;

public class NoteNotFoundException extends WebException {

    private static final String MESSAGE = "Note not found";

    public NoteNotFoundException() {
        super(MESSAGE, HttpServletResponse.SC_NOT_FOUND);
    }
}
