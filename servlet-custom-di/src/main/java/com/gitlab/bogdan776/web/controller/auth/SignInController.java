package com.gitlab.bogdan776.web.controller.auth;

import com.gitlab.bogdan776.context.Autowired;
import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import com.gitlab.bogdan776.web.dto.request.auth.SignInRequest;
import com.gitlab.bogdan776.web.usecase.auth.SignInUseCase;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/sign_in")
public class SignInController extends BaseController {

    @Autowired
    private SignInUseCase signIn;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/sign_in.jsp")
            .include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        UserPayload userPayload = signIn.execute(convertToJson(request, SignInRequest.class));
        setAuthorization(request, userPayload);
        response.sendRedirect("/");
    }
}
