package com.gitlab.bogdan776.web.dto.request.auth;

import lombok.Data;

@Data
public class SignInRequest {

    private String username;
    private String password;
}
