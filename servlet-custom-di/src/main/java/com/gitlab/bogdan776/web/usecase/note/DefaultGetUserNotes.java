package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.note.NoteDao;
import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;
import com.gitlab.bogdan776.web.mapper.NoteMapper;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetUserNotes implements GetUserNotesUseCase {

    private final NoteDao noteDao;
    private final NoteMapper mapper;

    @Override
    public List<NoteResponse> execute(UUID userUuid) {
        List<Note> notes = noteDao.findAllByAuthor(userUuid);
        return mapper.mapNotes(notes);
    }
}
