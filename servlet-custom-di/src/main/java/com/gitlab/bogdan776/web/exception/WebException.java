package com.gitlab.bogdan776.web.exception;

import com.gitlab.bogdan776.exception.AppException;
import lombok.Getter;

public abstract class WebException extends AppException {

    @Getter
    private final int statusCode;

    public WebException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public WebException(String message, Throwable cause, int statusCode) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    public WebException(Throwable cause, int statusCode) {
        super(cause);
        this.statusCode = statusCode;
    }
}
