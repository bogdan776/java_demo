package com.gitlab.bogdan776.web.controller;

import com.gitlab.bogdan776.common.util.JsonUtils;
import com.gitlab.bogdan776.context.ApplicationContext;
import com.gitlab.bogdan776.context.Autowired;
import com.gitlab.bogdan776.exception.ContextException;
import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Log4j2
public abstract class BaseController extends HttpServlet {

    @Override
    public void init(ServletConfig config) {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute(ApplicationContext.ATTRIBUTE_NAME);
        Arrays.stream(getClass().getDeclaredFields())
            .filter(field -> field.isAnnotationPresent(Autowired.class))
            .forEach(field -> setFieldValue(field, applicationContext.getInstance(field.getType())));
    }

    protected void setAuthorization(HttpServletRequest request, UserPayload payload) {
        request.getSession().setAttribute("Authorization", payload);
    }

    protected UserPayload getUserPayload(HttpServletRequest request) {
        return (UserPayload) request.getSession().getAttribute("Authorization");
    }

    protected UUID getUserId(HttpServletRequest request) {
        return getUserPayload(request).getId();
    }

    protected <R> R convertToJson(HttpServletRequest request, Class<R> klass) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        Map<String, String> requestParameters = new HashMap<>();
        parameterMap.forEach((key, value) -> {
            if (value.length > 0) {
                String parameterValue = value.length == 1
                    ? value[0]
                    : JsonUtils.toJson(value);
                requestParameters.put(key, parameterValue);
            }
        });
        return JsonUtils.fromJson(JsonUtils.toJson(requestParameters), klass);
    }

    private <V> void setFieldValue(Field field, V value) {
        field.setAccessible(true);
        try {
            field.set(this, value);
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            throw new ContextException("Cannot set value", e);
        }
    }
}
