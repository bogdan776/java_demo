package com.gitlab.bogdan776.web.dto.request.note;

import lombok.Data;

import java.util.UUID;

@Data
public class NoteRequest {

    private UUID id;
    private String title;
    private String text;
}
