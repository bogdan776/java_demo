package com.gitlab.bogdan776.web.controller.auth;

import com.gitlab.bogdan776.context.Autowired;
import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import com.gitlab.bogdan776.web.dto.request.auth.SignUpRequest;
import com.gitlab.bogdan776.web.usecase.auth.SignUpUseCase;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/sign_up")
public class SignUpController extends BaseController {

    @Autowired
    private SignUpUseCase signUp;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/sign_up.jsp")
            .forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        UserPayload payload = signUp.execute(convertToJson(request, SignUpRequest.class));
        setAuthorization(request, payload);
        response.sendRedirect("/");
    }
}
