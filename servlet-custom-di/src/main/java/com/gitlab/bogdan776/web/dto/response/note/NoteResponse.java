package com.gitlab.bogdan776.web.dto.response.note;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class NoteResponse {

    private UUID id;
    private UUID authorId;
    private String title;
    private String text;
    private LocalDateTime createAt;
}
