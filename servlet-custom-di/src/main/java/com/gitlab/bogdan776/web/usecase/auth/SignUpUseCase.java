package com.gitlab.bogdan776.web.usecase.auth;

import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import com.gitlab.bogdan776.web.dto.request.auth.SignUpRequest;

public interface SignUpUseCase {

    UserPayload execute(SignUpRequest request);
}
