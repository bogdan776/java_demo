package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.note.NoteDao;
import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;
import com.gitlab.bogdan776.web.exception.NoteNotFoundException;
import com.gitlab.bogdan776.web.mapper.NoteMapper;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetUserNote implements GetUserNoteUseCase {

    private final NoteDao noteDao;
    private final NoteMapper mapper;

    @Override
    public NoteResponse execute(UUID userUuid, UUID noteUuid) {
        Note note = noteDao.findByIdAndAuthorId(noteUuid, userUuid)
            .orElseThrow(NoteNotFoundException::new);
        return mapper.mapNote(note);
    }
}
