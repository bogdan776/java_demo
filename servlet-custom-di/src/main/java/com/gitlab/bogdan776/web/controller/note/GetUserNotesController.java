package com.gitlab.bogdan776.web.controller.note;

import com.gitlab.bogdan776.context.Autowired;
import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;
import com.gitlab.bogdan776.web.usecase.note.GetUserNotesUseCase;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet("/notes")
public class GetUserNotesController extends BaseController {

    @Autowired
    private GetUserNotesUseCase getUserNotes;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<NoteResponse> notes = getUserNotes.execute(getUserId(request));
        request.setAttribute("notes", notes);
        request.getRequestDispatcher("/WEB-INF/jsp/notes.jsp")
            .forward(request, response);
    }
}
