package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;

import java.util.List;
import java.util.UUID;

public interface GetUserNotesUseCase {

    List<NoteResponse> execute(UUID userUuid);
}
