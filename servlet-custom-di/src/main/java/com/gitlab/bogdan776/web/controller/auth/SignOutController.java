package com.gitlab.bogdan776.web.controller.auth;

import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.exception.WrongRequestException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/sign_out")
public class SignOutController extends BaseController {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/sign_out.jsp")
            .forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (getUserPayload(request) == null) {
            throw new WrongRequestException("You are not authenticated");
        }
        request.getSession().invalidate();
        response.sendRedirect("/sign_in");
    }
}
