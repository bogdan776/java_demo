package com.gitlab.bogdan776.web.dto.request.auth;

import lombok.Data;

@Data
public class SignUpRequest {

    private String username;
    private String email;
    private String password;
    private String passwordRepeat;
}
