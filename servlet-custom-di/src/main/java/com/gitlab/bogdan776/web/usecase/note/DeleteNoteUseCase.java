package com.gitlab.bogdan776.web.usecase.note;

import java.util.UUID;

public interface DeleteNoteUseCase {

    void execute(UUID userId, UUID noteId);
}
