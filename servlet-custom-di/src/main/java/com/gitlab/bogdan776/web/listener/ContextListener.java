package com.gitlab.bogdan776.web.listener;

import com.gitlab.bogdan776.context.ApplicationContext;
import com.gitlab.bogdan776.context.AutoScanApplicationContext;
import com.gitlab.bogdan776.web.mapper.NoteMapper;
import com.gitlab.bogdan776.web.mapper.UserMapper;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import lombok.extern.log4j.Log4j2;
import org.mapstruct.factory.Mappers;

import java.util.Map;

@Log4j2
@WebListener
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ApplicationContext applicationContext = AutoScanApplicationContext.scanPackage(
            "com.gitlab.bogdan776", () -> Map.of(
                UserMapper.class, Mappers.getMapper(UserMapper.class),
                NoteMapper.class, Mappers.getMapper(NoteMapper.class))
        );
        ServletContext servletContext = event.getServletContext();
        servletContext.setAttribute(ApplicationContext.ATTRIBUTE_NAME, applicationContext);
        log.info("Context initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("Context destroyed");
    }
}
