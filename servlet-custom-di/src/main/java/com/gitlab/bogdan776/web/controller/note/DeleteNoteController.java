package com.gitlab.bogdan776.web.controller.note;

import com.gitlab.bogdan776.context.Autowired;
import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.usecase.note.DeleteNoteUseCase;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.UUID;

@WebServlet("/notes/delete")
public class DeleteNoteController extends BaseController {

    @Autowired
    private DeleteNoteUseCase deleteNote;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        deleteNote.execute(getUserId(request), UUID.fromString(request.getParameter("id")));
        response.sendRedirect("/notes");
    }
}
