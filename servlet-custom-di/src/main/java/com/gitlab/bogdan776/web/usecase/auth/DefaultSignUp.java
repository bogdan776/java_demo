package com.gitlab.bogdan776.web.usecase.auth;

import com.gitlab.bogdan776.common.constant.Constants;
import com.gitlab.bogdan776.common.util.TimeUtils;
import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.user.UserDao;
import com.gitlab.bogdan776.entity.user.User;
import com.gitlab.bogdan776.entity.user.UserRole;
import com.gitlab.bogdan776.security.PasswordEncoder;
import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import com.gitlab.bogdan776.web.dto.request.auth.SignUpRequest;
import com.gitlab.bogdan776.web.exception.WrongRequestException;
import com.gitlab.bogdan776.web.mapper.UserMapper;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultSignUp implements SignUpUseCase {

    private final UserDao userDao;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    @Override
    public UserPayload execute(SignUpRequest request) {
        validateRequest(request);
        userDao.findByUsername(request.getUsername()).ifPresentOrElse(user -> {
            throw new WrongRequestException(
                String.format("User with username %s already exists", request.getUsername())
            );
        }, () -> userDao.findByEmail(request.getEmail()).ifPresent(user -> {
            throw new WrongRequestException(
                String.format("User with email %s already exists", request.getEmail())
            );
        }));
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRole(UserRole.USER);
        user.setCreateAt(TimeUtils.now());
        userDao.save(user);
        return userMapper.mapToPayload(user);
    }

    private void validateRequest(SignUpRequest request) {
        if (!Constants.EMAIL_PATTERN.matcher(request.getEmail()).find()) {
            throw new WrongRequestException("Wrong email");
        }
        if (!Constants.PASSWORD_PATTERN.matcher(request.getPassword()).find()) {
            throw new WrongRequestException("Password is too weak");
        }
        if (!request.getPassword().equals(request.getPasswordRepeat())) {
            throw new WrongRequestException("Passwords are not equal");
        }
    }
}
