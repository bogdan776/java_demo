package com.gitlab.bogdan776.web.dto.payload;

import com.gitlab.bogdan776.entity.user.UserRole;
import lombok.Data;

import java.util.UUID;

@Data
public class UserPayload {

    private UUID id;
    private String username;
    private UserRole role;
}
