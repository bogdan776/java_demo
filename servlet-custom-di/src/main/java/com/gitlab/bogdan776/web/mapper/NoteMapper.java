package com.gitlab.bogdan776.web.mapper;

import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface NoteMapper {

    NoteResponse mapNote(Note note);

    List<NoteResponse> mapNotes(List<Note> notes);
}
