package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.note.NoteDao;
import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.web.dto.request.note.NoteRequest;
import com.gitlab.bogdan776.web.exception.NoteNotFoundException;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultEditNote implements EditNoteUseCase {

    private final NoteDao noteDao;

    @Override
    public void execute(UUID userUuid, NoteRequest request) {
        Note note = noteDao.findByIdAndAuthorId(request.getId(), userUuid)
            .orElseThrow(NoteNotFoundException::new);
        note.setTitle(request.getTitle());
        note.setText(request.getText());
        noteDao.update(note);
    }
}
