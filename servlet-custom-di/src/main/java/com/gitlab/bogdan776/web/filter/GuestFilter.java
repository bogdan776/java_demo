package com.gitlab.bogdan776.web.filter;

import com.gitlab.bogdan776.web.controller.BaseController;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;

import java.io.IOException;

@WebFilter({"/notes/*", "/sign_out"})
public class GuestFilter extends BaseController implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (getUserPayload((HttpServletRequest) request) == null) {
            request.getRequestDispatcher("/sign_in").forward(request, response);
        } else {
            chain.doFilter(request, response);
        }
    }
}
