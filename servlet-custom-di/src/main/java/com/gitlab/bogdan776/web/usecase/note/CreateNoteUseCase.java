package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.web.dto.request.note.NoteRequest;

import java.util.UUID;

public interface CreateNoteUseCase {

    void execute(UUID userId, NoteRequest request);
}
