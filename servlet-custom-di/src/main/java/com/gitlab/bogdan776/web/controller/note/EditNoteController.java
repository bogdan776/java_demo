package com.gitlab.bogdan776.web.controller.note;

import com.gitlab.bogdan776.context.Autowired;
import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.request.note.NoteRequest;
import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;
import com.gitlab.bogdan776.web.usecase.note.EditNoteUseCase;
import com.gitlab.bogdan776.web.usecase.note.GetUserNoteUseCase;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.UUID;

@WebServlet("/notes/edit")
public class EditNoteController extends BaseController {

    @Autowired
    private GetUserNoteUseCase getUserNote;
    @Autowired
    private EditNoteUseCase editNote;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        NoteResponse note = getUserNote.execute(getUserId(request), UUID.fromString(request.getParameter("id")));
        request.setAttribute("note", note);
        request.getRequestDispatcher("/WEB-INF/jsp/note_editor.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        editNote.execute(getUserId(request), convertToJson(request, NoteRequest.class));
        response.sendRedirect("/notes");
    }
}
