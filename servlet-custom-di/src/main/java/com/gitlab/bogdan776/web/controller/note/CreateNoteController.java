package com.gitlab.bogdan776.web.controller.note;

import com.gitlab.bogdan776.context.Autowired;
import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.request.note.NoteRequest;
import com.gitlab.bogdan776.web.usecase.note.CreateNoteUseCase;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/notes/create")
public class CreateNoteController extends BaseController {

    @Autowired
    private CreateNoteUseCase createNote;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/note_editor.jsp")
            .forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        createNote.execute(getUserId(request), convertToJson(request, NoteRequest.class));
        response.sendRedirect("/notes");
    }
}
