package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.common.util.TimeUtils;
import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.note.NoteDao;
import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.web.dto.request.note.NoteRequest;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultCreateNote implements CreateNoteUseCase {

    private final NoteDao noteDao;

    @Override
    public void execute(UUID userId, NoteRequest request) {
        Note note = new Note();
        note.setId(UUID.randomUUID());
        note.setAuthorId(userId);
        note.setTitle(request.getTitle());
        note.setText(request.getText());
        note.setCreateAt(TimeUtils.now());
        noteDao.save(note);
    }
}
