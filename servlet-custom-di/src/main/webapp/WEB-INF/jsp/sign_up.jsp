<%@ include file="jspf/header.jspf" %>
<html>
<head>
    <title>Sign up</title>
    <style>
        <%@ include file="css/auth.css"%>
    </style>
</head>
<body>
<div style="color: #2d6f84; text-align: center;">
    <h1>Hello!</h1>
    <h2>Let's create a new user!</h2>
</div>
<form class="auth-form" method="post" action="${pageContext.request.contextPath}/sign_up">
    <div class="input-container">
        <label for="username">Username</label>
        <input required id="username" name="username" type="text" placeholder="Username">
        <label for="email">Email</label>
        <input required id="email" name="email" type="text" placeholder="Email">
        <label for="password">Password</label>
        <input required id="password" name="password" type="password" placeholder="Password">
        <label for="passwordRepeat">Repeat password</label>
        <input required id="passwordRepeat" name="passwordRepeat" type="password" placeholder="Repeat password">
        <button type="submit">Sign up</button>
    </div>
</form>
</body>
</html>
