<%@ include file="jspf/header.jspf" %>
<html>
<head>
    <title>Sign out</title>
    <style>
        <%@ include file="css/sign_out.css"%>
    </style>
</head>
<body>
<div class="container">
    <h2>Do you really want to leave?</h2>
    <form method="post" action="${pageContext.request.contextPath}/sign_out">
        <button class="sign-out" type="submit">Yes</button>
    </form>
</div>
</body>
</html>
