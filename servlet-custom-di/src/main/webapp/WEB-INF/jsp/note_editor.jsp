<%@ include file="jspf/header.jspf" %>
<html>
<head>
    <title>Note editor</title>
    <style>
        <%@ include file="css/note_editor.css" %>
    </style>
</head>
<body>
<c:choose>
    <c:when test="${not empty requestScope.note}">
        <div class="container">
            <h2 style="color: #2d6f84;">Edit note:</h2>
            <form method="post" action="${pageContext.request.contextPath}/notes/edit">
                <label for="title">Title</label>
                <input class="title-input" required type="text" name="title" id="title"
                       value="${requestScope.note.title}">
                <label for="text">Text</label>
                <textarea class="text-input" name="text" id="text">
                        ${requestScope.note.text}
                </textarea>
                <input hidden="hidden" type="text" name="id" value="${requestScope.note.id}">
                <button class="save-button" type="submit">Save</button>
            </form>
            <form method="post" action="${pageContext.request.contextPath}/notes/delete">
                <input hidden="hidden" type="text" name="id" value="${requestScope.note.id}">
                <button class="delete-button" type="submit">Delete</button>
            </form>
        </div>
    </c:when>
    <c:otherwise>
        <div class="container">
            <h2 style="color: #2d6f84;">Create note:</h2>
            <form method="post" action="${pageContext.request.contextPath}/notes/create">
                <label for="title_new">Title</label>
                <input class="title-input" required type="text" name="title" id="title_new">
                <label for="text_new">Text</label>
                <textarea class="text-input" name="text" id="text_new"></textarea>
                <button class="save-button" type="submit">Save</button>
            </form>
        </div>
    </c:otherwise>
</c:choose>
</body>
</html>
