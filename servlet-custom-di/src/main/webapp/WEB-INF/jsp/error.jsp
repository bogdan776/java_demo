<%@ include file="jspf/header.jspf" %>
<html>
<body>
<div style="color: #2d6f84; text-align: center;">
    <h1>Oops!</h1>
    <h2><c:out value="${requestScope.errorMessage}"/></h2>
</div>
</body>
</html>
