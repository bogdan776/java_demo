<%@ include file="jspf/header.jspf" %>
<html>
<head>
    <title>Notes</title>
    <style>
        <%@ include file="css/notes.css"%>
    </style>
</head>
<body>
<div class="container">
    <c:choose>
        <c:when test="${empty requestScope.notes}">
            <h2 style="color: #2d6f84;">Empty</h2>
        </c:when>
        <c:otherwise>
            <h2 style="color: #2d6f84;">Notes:</h2>
            <table class="note-table">
                <thead>
                <tr>
                    <td style="width: 15%"><i>Title</i></td>
                    <td style="width: 50%"><i>Text</i></td>
                    <td style="width: 25%"><i>Created</i></td>
                    <td style="width: 10%">&nbsp;</td>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="note" items="${requestScope.notes}">
                    <tr>
                        <td><c:out value="${note.title}"/></td>
                        <td><c:out value="${note.text}"/></td>
                        <td><c:out value="${note.createAt}"/></td>
                        <td>
                            <a href="${pageContext.request.contextPath}/notes/edit?id=${note.id}">Edit</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:otherwise>
    </c:choose>
    <a href="notes/create"><h4 style="color: #2d6f84;">Add</h4></a>
</div>
</body>
</html>
