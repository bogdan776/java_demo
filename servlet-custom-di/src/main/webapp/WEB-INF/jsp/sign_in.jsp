<%@ include file="jspf/header.jspf" %>
<html>
<head>
    <title>Sign in</title>
    <style>
        <%@ include file="css/auth.css" %>
    </style>
</head>
<body>
<div style="color: #2d6f84; text-align: center;">
    <h1>Hello!</h1>
    <h2>Enter username and password:</h2>
</div>
<form class="auth-form" action="sign_in" method="post">
    <div class="input-container">
        <label for="username">Username</label>
        <input required id="username" name="username" type="text" placeholder="Username">
        <label for="password">Password</label>
        <input required id="password" name="password" type="password" placeholder="Password">
        <button type="submit">Sign in</button>
        <a style="color: #2d6f84;" href="sign_up">Sign up</a>
    </div>
</form>
</body>
</html>
