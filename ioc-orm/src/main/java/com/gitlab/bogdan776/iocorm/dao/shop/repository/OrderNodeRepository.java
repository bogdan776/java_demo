package com.gitlab.bogdan776.iocorm.dao.shop.repository;

import com.gitlab.bogdan776.iocorm.common.util.JdbcUtils;
import com.gitlab.bogdan776.iocorm.context.Component;
import com.gitlab.bogdan776.iocorm.entity.shop.OrderNode;
import com.gitlab.bogdan776.iocorm.exception.DatabaseException;
import com.gitlab.bogdan776.iocorm.orm.ReflectionResultSetMapper;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Slf4j
@Component
public class OrderNodeRepository {

    public List<OrderNode> findAllByOrderId(UUID orderId, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_order_node where order_id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, orderId);
            try (ResultSet resultSet = statement.executeQuery()) {
                return ReflectionResultSetMapper.mapMultilineResultSet(resultSet, OrderNode.class);
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void save(OrderNode orderNode, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "insert into demo_order_node (id, order_id, product_id, quantity, price, currency) values (?, ?, ?, ?, ?, ?)"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                orderNode.getId(), orderNode.getOrderId(), orderNode.getProductId(),
                orderNode.getQuantity(), orderNode.getPrice(), orderNode.getCurrency());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void update(OrderNode orderNode, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "update demo_order_node set quantity = ?, price = ?, currency = ? where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                orderNode.getQuantity(), orderNode.getPrice(), orderNode.getCurrency(), orderNode.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
