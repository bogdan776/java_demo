package com.gitlab.bogdan776.iocorm.entity.user;

public enum UserRole {
    USER, ADMIN
}
