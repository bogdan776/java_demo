package com.gitlab.bogdan776.iocorm.common.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;

public final class JdbcUtils {

    private JdbcUtils() {
    }

    public static void fillPreparedStatement(PreparedStatement statement, Object value) throws SQLException {
        statement.setObject(1, convertValue(value));
    }

    public static void fillPreparedStatement(PreparedStatement statement, Object... values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            statement.setObject(i + 1, convertValue(values[i]));
        }
    }

    private static Object convertValue(Object value) {
        if (value instanceof Enum<?> enu) {
            return enu.toString();
        } else if (value instanceof Collection<?> || value instanceof Map<?, ?>) {
            return JsonUtils.toJson(value);
        } else if (value instanceof LocalDateTime ldt) {
            return Timestamp.valueOf(ldt);
        }
        return value;
    }
}
