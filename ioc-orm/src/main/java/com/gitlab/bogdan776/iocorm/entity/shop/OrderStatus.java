package com.gitlab.bogdan776.iocorm.entity.shop;

public enum OrderStatus {
    NEW, DONE
}
