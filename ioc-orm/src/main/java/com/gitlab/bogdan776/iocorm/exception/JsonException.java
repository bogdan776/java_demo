package com.gitlab.bogdan776.iocorm.exception;

public class JsonException extends AppException {

    public JsonException(String message, Throwable cause) {
        super(message, cause);
    }
}
