package com.gitlab.bogdan776.iocorm.dao.shop.repository;

import com.gitlab.bogdan776.iocorm.common.util.JdbcUtils;
import com.gitlab.bogdan776.iocorm.context.Component;
import com.gitlab.bogdan776.iocorm.entity.shop.Product;
import com.gitlab.bogdan776.iocorm.exception.DatabaseException;
import com.gitlab.bogdan776.iocorm.orm.ReflectionResultSetMapper;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
public class ProductRepository {

    public Optional<Product> findById(UUID id, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_product where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                return Optional.ofNullable(ReflectionResultSetMapper.mapResultSet(resultSet, Product.class));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public List<Product> findAll(Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement("select * from demo_product");
             ResultSet resultSet = statement.executeQuery()
        ) {
            return ReflectionResultSetMapper.mapMultilineResultSet(resultSet, Product.class);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void save(Product product, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "insert into demo_product (id, \"name\", description, resource_urls, currency, price, quantity) "
                + "values (?, ?, ?, ?::jsonb, ?, ?, ?)"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                product.getId(), product.getName(), product.getDescription(), product.getResourceUrls(),
                product.getCurrency(), product.getPrice(), product.getQuantity());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void update(Product product, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "update demo_product set name = ?, description = ?, resource_urls = ?::jsonb, "
                + "currency = ?, price = ?, quantity = ? where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                product.getName(), product.getDescription(), product.getResourceUrls(),
                product.getCurrency(), product.getPrice(), product.getQuantity(), product.getId()
            );
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
