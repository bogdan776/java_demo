package com.gitlab.bogdan776.iocorm;

import com.gitlab.bogdan776.iocorm.context.ApplicationContext;
import com.gitlab.bogdan776.iocorm.context.AutoScanApplicationContext;

public class IoCDemo {

    public static void main(String[] args) {
        ApplicationContext context = AutoScanApplicationContext.scanPackage("com.gitlab.bogdan776.iocorm");
        System.out.println("-------Context-------");
        context.types().stream()
            .map(Class::getSimpleName)
            .forEach(System.out::println);
        System.out.println("---------------------");
    }
}
