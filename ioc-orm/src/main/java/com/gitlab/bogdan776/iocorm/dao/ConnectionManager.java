package com.gitlab.bogdan776.iocorm.dao;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionManager {

    Connection getConnection() throws SQLException;
}
