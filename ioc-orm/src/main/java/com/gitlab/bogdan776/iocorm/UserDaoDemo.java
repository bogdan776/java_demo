package com.gitlab.bogdan776.iocorm;

import com.gitlab.bogdan776.iocorm.common.constant.Currency;
import com.gitlab.bogdan776.iocorm.common.util.TimeUtils;
import com.gitlab.bogdan776.iocorm.context.ApplicationContext;
import com.gitlab.bogdan776.iocorm.context.AutoScanApplicationContext;
import com.gitlab.bogdan776.iocorm.dao.user.UserDao;
import com.gitlab.bogdan776.iocorm.entity.user.User;
import com.gitlab.bogdan776.iocorm.entity.user.UserRole;
import com.gitlab.bogdan776.iocorm.entity.user.Wallet;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class UserDaoDemo {

    public static final UUID UUID_1 = UUID.fromString("d3ddc30d-2d0a-4876-ae90-75dce1acb417");
    public static final UUID UUID_2 = UUID.fromString("b79e0783-a82c-40dc-a548-16ee623641b1");

    public static void main(String[] args) {
        ApplicationContext context = AutoScanApplicationContext.scanPackage("com.gitlab.bogdan776.iocorm");
        UserDao userDao = context.getInstance(UserDao.class);
        /*
        ...
         */
        findOrCreateUserById(userDao, UUID_1);
    }

    private static void findOrCreateUserById(UserDao userDao, UUID userId) {
        userDao.findById(userId).ifPresentOrElse(
            user -> log.info("User found: {}", user),
            () -> {
                User user = createUser(userId);
                userDao.save(user);
                log.info("New user created and saved");
            }
        );
    }

    private static void findUserByUsername(UserDao userDao, String username) {
        userDao.findByUsername(username).ifPresent(
            user -> log.info("User found: {}", user)
        );
    }

    private static void deleteUser(UserDao userDao, UUID userId) {
        User user = userDao.findById(userId)
            .orElseThrow(() -> new RuntimeException("User not found"));
        log.info("User found: {}", user);
        userDao.delete(user);
        Optional<User> userOptional = userDao.findById(userId);
        log.info("User deleted: {}", userOptional.isEmpty());
    }

    private static void updateUserById(UserDao userDao, UUID userId) {
        User user = userDao.findById(userId)
            .orElseThrow(() -> new RuntimeException("User not found"));
        log.info("User found: {}", user);
        user.setUsername("new_username");
        user.setEmail("new_mail@email.com");
        user.setPassword("qwerty12345");
        userDao.update(user);
        userDao.findById(userId).ifPresent(
            updUser -> log.info("User after update: {}", updUser)
        );
    }

    private static void findOrCreateUserWalletByUserId(UserDao userDao, UUID userId) {
        userDao.findWalletByUser(userId).ifPresentOrElse(
            wallet -> log.info("Wallet found: {}", wallet),
            () -> {
                User user = userDao.findById(userId).orElseThrow();
                Wallet wallet = createWallet(user, UUID_2);
                userDao.save(wallet);
                log.info("Wallet created");
            }
        );
    }

    private static void updateWalletByUserId(UserDao userDao, UUID userId) {
        Wallet wallet = userDao.findWalletByUser(userId)
            .orElseThrow(() -> new RuntimeException("Wallet not found"));
        log.info("Wallet found: {}", wallet);
        wallet.setAmount(BigDecimal.valueOf(15));
        userDao.update(wallet);
        userDao.findWalletByUser(userId)
            .ifPresent(updatedWallet -> log.info("Updated wallet: {}", updatedWallet));
    }

    private static User createUser(UUID userId) {
        User user = new User();
        user.setId(userId);
        user.setUsername("test_user");
        user.setEmail("m@email.com");
        user.setPassword("qwerty");
        user.setRole(UserRole.USER);
        user.setCreateAt(TimeUtils.now());
        return user;
    }

    private static Wallet createWallet(User user, UUID walletId) {
        Wallet wallet = new Wallet();
        wallet.setId(walletId);
        wallet.setUserId(user.getId());
        wallet.setCurrency(Currency.USD);
        wallet.setAmount(BigDecimal.TEN);
        return wallet;
    }
}
