package com.gitlab.bogdan776.iocorm.dao.user.repository;

import com.gitlab.bogdan776.iocorm.common.util.JdbcUtils;
import com.gitlab.bogdan776.iocorm.context.Component;
import com.gitlab.bogdan776.iocorm.entity.user.User;
import com.gitlab.bogdan776.iocorm.exception.DatabaseException;
import com.gitlab.bogdan776.iocorm.orm.ReflectionResultSetMapper;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
public class UserRepository {

    public Optional<User> findUserById(UUID id, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_user where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                return Optional.ofNullable(ReflectionResultSetMapper.mapResultSet(resultSet, User.class));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public Optional<User> findByUsername(String username, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_user where username = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, username);
            try (ResultSet resultSet = statement.executeQuery()) {
                return Optional.ofNullable(ReflectionResultSetMapper.mapResultSet(resultSet, User.class));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void delete(User user, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "delete from demo_user where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void save(User user, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "insert into demo_user (id, username, email, \"password\", \"role\", create_at)"
                + " values (?, ?, ?, ?, ?, ?)"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                user.getId(), user.getUsername(), user.getEmail(),
                user.getPassword(), user.getRole(), user.getCreateAt());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void update(User user, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "update demo_user set username = ?, email = ?, \"password\" = ?, \"role\" = ? where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                user.getUsername(), user.getEmail(), user.getPassword(),
                user.getRole(), user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
