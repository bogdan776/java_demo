package com.gitlab.bogdan776.iocorm.dao.shop.repository;

import com.gitlab.bogdan776.iocorm.common.util.JdbcUtils;
import com.gitlab.bogdan776.iocorm.context.Component;
import com.gitlab.bogdan776.iocorm.entity.shop.Order;
import com.gitlab.bogdan776.iocorm.entity.shop.OrderStatus;
import com.gitlab.bogdan776.iocorm.exception.DatabaseException;
import com.gitlab.bogdan776.iocorm.orm.ReflectionResultSetMapper;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
public class OrderRepository {

    public Optional<Order> findById(UUID id, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_order where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                return Optional.ofNullable(ReflectionResultSetMapper.mapResultSet(resultSet, Order.class));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public List<Order> findAll(Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement("select * from demo_order");
             ResultSet resultSet = statement.executeQuery()
        ) {
            return ReflectionResultSetMapper.mapMultilineResultSet(resultSet, Order.class);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public List<Order> findAllByStatus(OrderStatus status, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_order where status = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, status.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                return ReflectionResultSetMapper.mapMultilineResultSet(resultSet, Order.class);
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public List<Order> findAllByUserId(UUID userId, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_order where user_id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, userId);
            try (ResultSet resultSet = statement.executeQuery()) {
                return ReflectionResultSetMapper.mapMultilineResultSet(resultSet, Order.class);
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public List<Order> findAllByUserIdAndStatus(UUID userId, OrderStatus status, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_order where user_id = ? and status = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, userId, status.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                return ReflectionResultSetMapper.mapMultilineResultSet(resultSet, Order.class);
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void save(Order order, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "insert into demo_order (id, user_id, price, currency, note, status) values (?, ?, ?, ?, ?, ?)"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                order.getId(), order.getUserId(), order.getPrice(),
                order.getCurrency(), order.getNote(), order.getStatus());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void update(Order order, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "update demo_order set price = ?, currency = ?, note = ?, status = ? where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                order.getPrice(), order.getCurrency(), order.getNote(),
                order.getStatus(), order.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
