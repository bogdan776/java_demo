package com.gitlab.bogdan776.iocorm;

import com.gitlab.bogdan776.iocorm.common.constant.Currency;
import com.gitlab.bogdan776.iocorm.context.ApplicationContext;
import com.gitlab.bogdan776.iocorm.context.AutoScanApplicationContext;
import com.gitlab.bogdan776.iocorm.dao.shop.ShopDao;
import com.gitlab.bogdan776.iocorm.entity.shop.Order;
import com.gitlab.bogdan776.iocorm.entity.shop.OrderNode;
import com.gitlab.bogdan776.iocorm.entity.shop.OrderStatus;
import com.gitlab.bogdan776.iocorm.entity.shop.Product;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.UUID;

@Slf4j
public class ShopDaoDemo {

    public static final UUID UUID_1 = UUID.fromString("87bb88d1-0365-4c4b-a8af-30da6a5d6d9a");
    public static final UUID UUID_2 = UUID.fromString("6fd97d23-2f76-4726-8d03-e42a99a7fac0");

    public static void main(String[] args) throws URISyntaxException {
        ApplicationContext context = AutoScanApplicationContext.scanPackage("com.gitlab.bogdan776.iocorm");
        ShopDao shopDao = context.getInstance(ShopDao.class);
        /*
        ...
         */
        findByIdOrCreateProduct(shopDao, UUID_1);
    }

    private static void findByIdOrCreateOrder(ShopDao shopDao, UUID id) {
        shopDao.findOrderById(id).ifPresentOrElse(
            order -> log.info("Order {}", order),
            () -> {
                Order order = createOrder(id, UserDaoDemo.UUID_1);
                shopDao.save(order);
                log.info("Order created");
            }
        );
    }

    private static void updateOrder(ShopDao shopDao, UUID id) {
        Order order = shopDao.findOrderById(id)
            .orElseThrow(() -> new RuntimeException("Order not found"));
        log.info("Order before update: {}", order);
        order.setNote("Status is not updated");
        order.setStatus(OrderStatus.NEW);
        shopDao.update(order);
        shopDao.findOrderById(id)
            .ifPresent(updOrder -> log.info("Updated order: {}", updOrder));
    }

    private static void findByIdOrCreateProduct(ShopDao shopDao, UUID id) {
        shopDao.findProductById(id).ifPresentOrElse(
            product -> log.info("Product {}", product),
            () -> {
                Product product = createProduct(id);
                shopDao.save(product);
                log.info("Product created");
            }
        );
    }

    private static void updateProduct(ShopDao shopDao, UUID id) throws URISyntaxException {
        Product product = shopDao.findProductById(id)
            .orElseThrow(() -> new RuntimeException("Product not found"));
        log.info("Product before update: {}", product);
        product.setDescription("Updated quantity");
        product.setQuantity(10);
        Map<String, URI> resourceUrls = product.getResourceUrls();
        resourceUrls.put("imag", new URI("https://www.google.com.ua/imghp/test"));
        product.setResourceUrls(resourceUrls);
        shopDao.update(product);
        shopDao.findProductById(id)
            .ifPresent(updProduct -> log.info("Updated product: {}", updProduct));
    }

    private static void createOrderNodeByOrderId(ShopDao shopDao, UUID orderId) {
        Order order = shopDao.findOrderById(orderId)
            .orElseThrow(() -> new RuntimeException("Order not found"));
        OrderNode node = createOrderNode(UUID.randomUUID(), order.getId(), UUID_2);
        shopDao.save(node);
        shopDao.findOrderNodesByOrder(order.getId())
            .forEach(orderNode -> log.info("Node: {}", orderNode));
    }

    private static void updateFirstOrderNode(ShopDao shopDao, UUID orderId) {
        Order order = shopDao.findOrderById(orderId)
            .orElseThrow(() -> new RuntimeException("Order not found"));
        OrderNode orderNode = shopDao.findOrderNodesByOrder(order.getId()).stream()
            .findFirst()
            .orElseThrow();
        log.info("Order node before update {}", orderNode);
        orderNode.setQuantity(orderNode.getQuantity() + 1);
        orderNode.setPrice(orderNode.getPrice().add(BigDecimal.valueOf(2)));
        shopDao.update(orderNode);
        shopDao.findOrderNodesByOrder(order.getId())
            .forEach(node -> log.info("Node: {}", node));
    }

    private static OrderNode createOrderNode(UUID id, UUID orderId, UUID productId) {
        OrderNode orderNode = new OrderNode();
        orderNode.setId(id);
        orderNode.setOrderId(orderId);
        orderNode.setProductId(productId);
        orderNode.setQuantity(1);
        orderNode.setPrice(BigDecimal.TWO);
        orderNode.setCurrency(Currency.USD);
        return orderNode;
    }

    private static Product createProduct(UUID id) {
        Product product = new Product();
        product.setId(id);
        product.setName("Product");
        product.setDescription("Demo product");
        try {
            product.setResourceUrls(Map.of("img", new URI("https://www.google.com.ua/imghp")));
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        product.setCurrency(Currency.USD);
        product.setPrice(BigDecimal.ONE);
        product.setQuantity(1);
        return product;
    }

    private static Order createOrder(UUID id, UUID userId) {
        Order order = new Order();
        order.setId(id);
        order.setUserId(userId);
        order.setPrice(BigDecimal.TWO);
        order.setCurrency(Currency.USD);
        order.setNote("New demo order");
        order.setStatus(OrderStatus.NEW);
        return order;
    }
}
