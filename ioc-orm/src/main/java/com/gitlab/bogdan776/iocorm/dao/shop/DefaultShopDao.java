package com.gitlab.bogdan776.iocorm.dao.shop;

import com.gitlab.bogdan776.iocorm.context.Component;
import com.gitlab.bogdan776.iocorm.dao.ConnectionManager;
import com.gitlab.bogdan776.iocorm.dao.shop.repository.OrderNodeRepository;
import com.gitlab.bogdan776.iocorm.dao.shop.repository.OrderRepository;
import com.gitlab.bogdan776.iocorm.dao.shop.repository.ProductRepository;
import com.gitlab.bogdan776.iocorm.entity.shop.Order;
import com.gitlab.bogdan776.iocorm.entity.shop.OrderNode;
import com.gitlab.bogdan776.iocorm.entity.shop.OrderStatus;
import com.gitlab.bogdan776.iocorm.entity.shop.Product;
import com.gitlab.bogdan776.iocorm.exception.DatabaseException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultShopDao implements ShopDao {

    private final ConnectionManager connectionManager;

    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;
    private final OrderNodeRepository orderNodeRepository;

    @Override
    public Optional<Product> findProductById(UUID id) {
        try (Connection connection = connectionManager.getConnection()) {
            return productRepository.findById(id, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public Optional<Order> findOrderById(UUID id) {
        try (Connection connection = connectionManager.getConnection()) {
            return orderRepository.findById(id, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public List<Product> findAllProducts() {
        try (Connection connection = connectionManager.getConnection()) {
            return productRepository.findAll(connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public List<Order> findAllOrders() {
        try (Connection connection = connectionManager.getConnection()) {
            return orderRepository.findAll(connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public List<Order> findAllOrdersByStatus(OrderStatus status) {
        try (Connection connection = connectionManager.getConnection()) {
            return orderRepository.findAllByStatus(status, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public List<Order> findAllOrdersByUser(UUID userId) {
        try (Connection connection = connectionManager.getConnection()) {
            return orderRepository.findAllByUserId(userId, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public List<Order> findAllOrdersByUserAndStatus(UUID userId, OrderStatus status) {
        try (Connection connection = connectionManager.getConnection()) {
            return orderRepository.findAllByUserIdAndStatus(userId, status, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public List<OrderNode> findOrderNodesByOrder(UUID orderId) {
        try (Connection connection = connectionManager.getConnection()) {
            return orderNodeRepository.findAllByOrderId(orderId, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void save(Product product) {
        try (Connection connection = connectionManager.getConnection()) {
            productRepository.save(product, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void save(Order order) {
        try (Connection connection = connectionManager.getConnection()) {
            orderRepository.save(order, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void save(OrderNode orderNode) {
        try (Connection connection = connectionManager.getConnection()) {
            orderNodeRepository.save(orderNode, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void update(Product product) {
        try (Connection connection = connectionManager.getConnection()) {
            productRepository.update(product, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void update(Order order) {
        try (Connection connection = connectionManager.getConnection()) {
            orderRepository.update(order, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void update(OrderNode orderNode) {
        try (Connection connection = connectionManager.getConnection()) {
            orderNodeRepository.update(orderNode, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
