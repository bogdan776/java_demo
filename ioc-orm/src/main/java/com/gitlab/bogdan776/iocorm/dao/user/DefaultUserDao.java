package com.gitlab.bogdan776.iocorm.dao.user;

import com.gitlab.bogdan776.iocorm.context.Component;
import com.gitlab.bogdan776.iocorm.dao.ConnectionManager;
import com.gitlab.bogdan776.iocorm.dao.user.repository.UserRepository;
import com.gitlab.bogdan776.iocorm.dao.user.repository.WalletRepository;
import com.gitlab.bogdan776.iocorm.entity.user.User;
import com.gitlab.bogdan776.iocorm.entity.user.Wallet;
import com.gitlab.bogdan776.iocorm.exception.DatabaseException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultUserDao implements UserDao {

    private final ConnectionManager connectionManager;

    private final UserRepository userRepository;
    private final WalletRepository walletRepository;

    @Override
    public Optional<User> findById(UUID uuid) {
        try (Connection connection = connectionManager.getConnection()) {
            return userRepository.findUserById(uuid, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public Optional<Wallet> findWalletByUser(UUID userId) {
        try (Connection connection = connectionManager.getConnection()) {
            return walletRepository.findByUserId(userId, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public Optional<User> findByUsername(String username) {
        try (Connection connection = connectionManager.getConnection()) {
            return userRepository.findByUsername(username, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void save(User user) {
        try (Connection connection = connectionManager.getConnection()) {
            userRepository.save(user, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void save(Wallet wallet) {
        try (Connection connection = connectionManager.getConnection()) {
            walletRepository.save(wallet, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void update(User user) {
        try (Connection connection = connectionManager.getConnection()) {
            userRepository.update(user, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void update(Wallet wallet) {
        try (Connection connection = connectionManager.getConnection()) {
            walletRepository.update(wallet, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(User user) {
        try (Connection connection = connectionManager.getConnection()) {
            userRepository.delete(user, connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
