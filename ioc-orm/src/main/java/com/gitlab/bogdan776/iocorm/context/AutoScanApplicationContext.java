package com.gitlab.bogdan776.iocorm.context;

import com.gitlab.bogdan776.iocorm.common.util.ClassLoaderUtils;

import java.util.List;
import java.util.Set;

public class AutoScanApplicationContext implements ApplicationContext {

    private final Context context;

    private AutoScanApplicationContext(Context context) {
        this.context = context;
    }

    public static ApplicationContext scanPackage(String packageName) {
        ContextFactory contextFactory = new DefaultContextFactory(new DefaultContext());
        List<Class<?>> classes = ClassLoaderUtils.loadClassesByPackageAndAnnotation(packageName, Component.class);
        contextFactory.initialize(classes);
        return new AutoScanApplicationContext(contextFactory.getContext());
    }

    @Override
    public <T> T getInstance(Class<T> type) {
        return context.get(type);
    }

    @Override
    public Set<Class<?>> types() {
        return context.getAllTypes();
    }
}
