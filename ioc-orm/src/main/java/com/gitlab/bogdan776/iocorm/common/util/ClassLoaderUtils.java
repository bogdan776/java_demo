package com.gitlab.bogdan776.iocorm.common.util;

import com.gitlab.bogdan776.iocorm.exception.ContextException;
import com.google.common.reflect.ClassPath;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
public final class ClassLoaderUtils {

    private ClassLoaderUtils() {
    }

    public static List<Class<?>> loadClassesByPackageAndAnnotation(String packageName,
                                                                   Class<? extends Annotation> annotation) {
        return loadClassesByPackage(
            classPath(systemClassLoader()), packageName,
            defaultPredicate(),
            klass -> klass.isAnnotationPresent(annotation)
        );
    }

    public static List<Class<?>> loadClassesByPackage(String packageName) {
        return loadClassesByPackage(classPath(systemClassLoader()), packageName, defaultPredicate(), defaultPredicate());
    }

    public static List<Class<?>> loadClassesByPackage(ClassPath classPath, String packageName,
                                                      Predicate<ClassPath.ClassInfo> classInfoFilter,
                                                      Predicate<Class<?>> classFilter) {
        return classPath.getTopLevelClassesRecursive(packageName)
            .stream()
            .filter(classInfoFilter)
            .map(ClassPath.ClassInfo::load)
            .filter(classFilter)
            .collect(Collectors.toList());
    }

    public static ClassPath classPath(ClassLoader classLoader) {
        try {
            return ClassPath.from(classLoader);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new ContextException(e.getMessage(), e);
        }
    }

    public static ClassLoader threadContextClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    public static ClassLoader systemClassLoader() {
        return ClassLoader.getSystemClassLoader();
    }

    public static <T> Predicate<T> defaultPredicate() {
        return t -> true;
    }
}
