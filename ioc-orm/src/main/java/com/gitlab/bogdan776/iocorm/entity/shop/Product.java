package com.gitlab.bogdan776.iocorm.entity.shop;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.gitlab.bogdan776.iocorm.common.constant.Currency;
import com.gitlab.bogdan776.iocorm.deserializer.ResourceMapDeserializer;
import com.gitlab.bogdan776.iocorm.orm.Column;
import lombok.Data;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Map;
import java.util.UUID;

@Data
public class Product {

    private UUID id;
    private String name;
    private String description;
    @Column(name = "resource_urls")
    @JsonDeserialize(using = ResourceMapDeserializer.class)
    private Map<String, URI> resourceUrls;
    private Currency currency;
    private BigDecimal price;
    private Integer quantity;
}
