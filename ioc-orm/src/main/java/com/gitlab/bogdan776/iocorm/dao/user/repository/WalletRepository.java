package com.gitlab.bogdan776.iocorm.dao.user.repository;

import com.gitlab.bogdan776.iocorm.common.util.JdbcUtils;
import com.gitlab.bogdan776.iocorm.context.Component;
import com.gitlab.bogdan776.iocorm.entity.user.Wallet;
import com.gitlab.bogdan776.iocorm.exception.DatabaseException;
import com.gitlab.bogdan776.iocorm.orm.ReflectionResultSetMapper;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
public class WalletRepository {

    public Optional<Wallet> findByUserId(UUID userId, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "select * from demo_wallet where user_id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement, userId);
            try (ResultSet resultSet = statement.executeQuery()) {
                return Optional.ofNullable(ReflectionResultSetMapper.mapResultSet(resultSet, Wallet.class));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void save(Wallet wallet, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "insert into demo_wallet (id, user_id, currency, amount) values (?, ?, ?, ?)"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                wallet.getId(), wallet.getUserId(), wallet.getCurrency(), wallet.getAmount());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }

    public void update(Wallet wallet, Connection connection) {
        try (PreparedStatement statement = connection.prepareStatement(
            "update demo_wallet set currency = ?, amount = ? where id = ?"
        )) {
            JdbcUtils.fillPreparedStatement(statement,
                wallet.getCurrency(), wallet.getAmount(), wallet.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new DatabaseException(e.getMessage(), e);
        }
    }
}
