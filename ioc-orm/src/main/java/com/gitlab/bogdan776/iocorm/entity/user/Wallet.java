package com.gitlab.bogdan776.iocorm.entity.user;

import com.gitlab.bogdan776.iocorm.common.constant.Currency;
import com.gitlab.bogdan776.iocorm.orm.Column;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
public class Wallet {

    private UUID id;
    @Column(name = "user_id")
    private UUID userId;
    private Currency currency;
    private BigDecimal amount;
}
