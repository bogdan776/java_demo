package com.gitlab.bogdan776.iocorm.common.constant;

public enum Currency {
    USD, EUR, JPY, UAH
}
