package com.gitlab.bogdan776.iocorm.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.gitlab.bogdan776.iocorm.common.util.JsonUtils;

import java.io.IOException;
import java.util.Map;

public class ResourceMapDeserializer extends StdDeserializer<Map<?, ?>> {

    public ResourceMapDeserializer() {
        this(null);
    }

    public ResourceMapDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Map<?, ?> deserialize(JsonParser jsonParser,
                                 DeserializationContext deserializationContext) throws IOException {
        return JsonUtils.fromJson(jsonParser.getValueAsString());
    }
}
