package com.gitlab.bogdan776.iocorm.dao;

import com.gitlab.bogdan776.iocorm.context.ApplicationProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;

public final class DefaultConnectionManager implements ConnectionManager {

    private final String url, user, password;

    public DefaultConnectionManager() {
        Properties properties = ApplicationProperties.load("datasource.properties");
        this.url = Objects.requireNonNull(properties.getProperty("datasource.url"));
        this.user = Objects.requireNonNull(properties.getProperty("datasource.username"));
        this.password = Objects.requireNonNull(properties.getProperty("datasource.password"));
    }

    @Override
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }
}
