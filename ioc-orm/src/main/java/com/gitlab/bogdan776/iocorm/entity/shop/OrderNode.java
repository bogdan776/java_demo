package com.gitlab.bogdan776.iocorm.entity.shop;

import com.gitlab.bogdan776.iocorm.common.constant.Currency;
import com.gitlab.bogdan776.iocorm.orm.Column;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
public class OrderNode {

    private UUID id;
    @Column(name = "order_id")
    private UUID orderId;
    @Column(name = "product_id")
    private UUID productId;
    private Integer quantity;
    private BigDecimal price;
    private Currency currency;
}
