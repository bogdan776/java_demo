package com.gitlab.bogdan776.iocorm.context;

import java.util.List;

public interface ContextFactory {

    void initialize(List<Class<?>> classes);

    Context getContext();
}
