package com.gitlab.bogdan776.iocorm.dao;

import com.gitlab.bogdan776.iocorm.context.ApplicationProperties;
import com.gitlab.bogdan776.iocorm.context.Component;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

@Component
public final class HikariConnectionPool implements ConnectionManager {

    private final HikariDataSource dataSource;

    public HikariConnectionPool() {
        Properties properties = ApplicationProperties.load("datasource.properties");
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("datasource.url"));
        config.setUsername(properties.getProperty("datasource.username"));
        config.setPassword(properties.getProperty("datasource.password"));
        config.setAutoCommit(Boolean.parseBoolean(properties.getProperty("datasource.auto-commit")));
        this.dataSource = new HikariDataSource(config);
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
