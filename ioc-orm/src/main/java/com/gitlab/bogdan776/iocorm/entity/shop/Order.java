package com.gitlab.bogdan776.iocorm.entity.shop;

import com.gitlab.bogdan776.iocorm.common.constant.Currency;
import com.gitlab.bogdan776.iocorm.orm.Column;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
public class Order {

    private UUID id;
    @Column(name = "user_id")
    private UUID userId;
    private BigDecimal price;
    private Currency currency;
    private String note;
    private OrderStatus status;
}
