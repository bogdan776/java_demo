package com.gitlab.bogdan776.iocorm.context;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

@AllArgsConstructor
public class DefaultContextFactory implements ContextFactory {

    @Getter
    private final Context context;

    @Override
    public void initialize(List<Class<?>> classes) {
        Queue<ComponentData> queue = classes.stream()
            .map(ComponentData::new)
            .sorted(componentDataComparator())
            .collect(Collectors.toCollection(LinkedList::new));
        while (!queue.isEmpty()) {
            ComponentData componentData = queue.poll();
            if (readyToCreate(componentData)) {
                Object object = componentData.create(
                    context.getAllByTypes(componentData.getConstructorParams()).toArray()
                );
                context.add(object, componentData.getSuperTypes());
            } else {
                queue.offer(componentData);
            }
        }
    }

    private Comparator<ComponentData> componentDataComparator() {
        return Comparator.comparing(ComponentData::hasDefaultConstructor)
            .thenComparingInt(ComponentData::getParameterCount);
    }

    private boolean readyToCreate(ComponentData componentData) {
        return componentData.hasDefaultConstructor() || context.contains(componentData.getConstructorParams());
    }
}
