package com.gitlab.bogdan776.iocorm.entity.user;

import com.gitlab.bogdan776.iocorm.orm.Column;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class User {

    private UUID id;
    private String username;
    private String email;
    private String password;
    private UserRole role;
    @Column(name = "create_at")
    private LocalDateTime createAt;
}
