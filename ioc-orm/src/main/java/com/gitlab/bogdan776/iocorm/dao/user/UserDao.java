package com.gitlab.bogdan776.iocorm.dao.user;

import com.gitlab.bogdan776.iocorm.entity.user.User;
import com.gitlab.bogdan776.iocorm.entity.user.Wallet;

import java.util.Optional;
import java.util.UUID;

public interface UserDao {

    Optional<User> findById(UUID id);

    Optional<Wallet> findWalletByUser(UUID userId);

    Optional<User> findByUsername(String username);

    void save(User user);

    void save(Wallet wallet);

    void update(User user);

    void update(Wallet wallet);

    void delete(User user);
}
