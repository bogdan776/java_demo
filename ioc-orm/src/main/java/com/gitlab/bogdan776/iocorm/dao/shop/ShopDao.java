package com.gitlab.bogdan776.iocorm.dao.shop;

import com.gitlab.bogdan776.iocorm.entity.shop.Order;
import com.gitlab.bogdan776.iocorm.entity.shop.OrderNode;
import com.gitlab.bogdan776.iocorm.entity.shop.OrderStatus;
import com.gitlab.bogdan776.iocorm.entity.shop.Product;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ShopDao {

    Optional<Product> findProductById(UUID id);

    Optional<Order> findOrderById(UUID id);

    List<Product> findAllProducts();

    List<Order> findAllOrders();

    List<Order> findAllOrdersByStatus(OrderStatus status);

    List<Order> findAllOrdersByUser(UUID userId);

    List<Order> findAllOrdersByUserAndStatus(UUID userId, OrderStatus status);

    List<OrderNode> findOrderNodesByOrder(UUID orderId);

    void save(Product product);

    void save(Order order);

    void save(OrderNode orderNode);

    void update(Product product);

    void update(Order order);

    void update(OrderNode orderNode);
}
