package com.gitlab.bogdan776.iocorm.orm;

import com.gitlab.bogdan776.iocorm.exception.MapperException;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Stream;

@Slf4j
public final class ReflectionResultSetMapper {

    private ReflectionResultSetMapper() {
    }

    public static <T> List<T> mapMultilineResultSet(ResultSet resultSet, Class<T> klass) throws SQLException {
        List<T> values = Lists.newArrayList();
        while (resultSet.next()) {
            values.add(mapRow(resultSet, klass));
        }
        return values;
    }

    public static <T> T mapResultSet(ResultSet resultSet, Class<T> klass) throws SQLException {
        if (resultSet.next()) {
            return mapRow(resultSet, klass);
        }
        return null;
    }

    public static <T> T mapRow(ResultSet resultSet, Class<T> klass) {
        T instance = createDefaultInstance(klass);
        Stream.of(klass.getDeclaredFields())
            .filter(ReflectionResultSetMapper::fieldIsNotIgnored)
            .peek(field -> field.setAccessible(true))
            .forEach(field -> {
                String columnLabel = extractColumnName(field);
                Object value = getValue(resultSet, columnLabel);
                setFieldValue(field, instance, value);
            });
        return instance;
    }

    private static <T> T createDefaultInstance(Class<T> klass) {
        try {
            Constructor<T> defaultConstructor = klass.getConstructor();
            return defaultConstructor.newInstance();
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException |
                 IllegalAccessException e) {
            throw new MapperException(e);
        }
    }

    private static boolean fieldIsNotIgnored(Field field) {
        return !fieldIsIgnored(field);
    }

    private static boolean fieldIsIgnored(Field field) {
        if (field.isAnnotationPresent(Column.class)) {
            return field.getAnnotation(Column.class).ignore();
        }
        return false;
    }

    private static String extractColumnName(Field field) {
        return field.isAnnotationPresent(Column.class)
            ? field.getAnnotation(Column.class).name()
            : field.getName();
    }

    private static void setFieldValue(Field field, Object instance, Object value) {
        try {
            field.set(instance, value);
        } catch (IllegalAccessException e) {
            throw new MapperException(e);
        }
    }

    private static Object getValue(ResultSet resultSet, String columnLabel) {
        try {
            return JdbConverter.convert(resultSet.getObject(columnLabel));
        } catch (SQLException e) {
            throw new MapperException(e);
        }
    }
}
