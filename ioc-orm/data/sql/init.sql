create table if not exists demo_user
(
    id         uuid      not null,
    username   varchar   not null,
    email      varchar   not null,
    "password" varchar   not null,
    "role"     varchar   not null,
    create_at  timestamp not null,
    constraint demo_user_pk primary key (id),
    constraint demo_user_uq_username unique (username),
    constraint demo_user_uq_email unique (email)
);

create table if not exists demo_wallet
(
    id       uuid    not null,
    user_id  uuid    not null,
    currency varchar not null,
    amount   numeric not null,
    constraint demo_wallet_pk primary key (id),
    constraint demo_wallet_fk_user foreign key (user_id) references demo_user,
    constraint demo_wallet_uq_user_currency unique (user_id, currency)
);

create table if not exists demo_product
(
    id            uuid    not null,
    "name"        varchar not null,
    description   varchar,
    resource_urls jsonb,
    currency      varchar not null,
    price         numeric not null,
    quantity      integer not null,
    constraint demo_product_pk primary key (id)
);

create table if not exists demo_order
(
    id       uuid    not null,
    user_id  uuid    not null,
    price    numeric not null,
    currency varchar not null,
    note     varchar,
    status   varchar not null,
    constraint demo_order_pk primary key (id),
    constraint demo_order_fk_user foreign key (user_id) references demo_user
);

create table if not exists demo_order_node
(

    id         uuid    not null,
    order_id   uuid    not null,
    product_id uuid    not null,
    quantity   integer not null,
    price      numeric not null,
    currency   varchar not null,
    constraint demo_order_node_pk primary key (id),
    constraint demo_order_node_fk_order foreign key (order_id) references demo_order,
    constraint demo_order_node_fk_product foreign key (product_id) references demo_product
);
