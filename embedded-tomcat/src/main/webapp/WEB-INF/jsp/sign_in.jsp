<html>
<head>
    <title>Sign in</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
<form>
    <p>
        Username
        <label for="username">
            <input name="username" id="username" placeholder="Username"/>
        </label>
    </p>
    <p>
        Password
        <label for="password">
            <input name="password" id="password" type="password" placeholder="Password"/>
        </label>

    </p>
    <input type="submit" id="button-username" value="Sign in">
</form>
</body>
<script>
    $(document).ready(function () {
        $('#button-username').click(function (event) {
            event.preventDefault();
            const username = $('#username').val();
            const password = $('#password').val();
            const path = '${pageContext.servletContext.contextPath}/sign_in';
            $.ajax({
                url: path,
                method: 'POST',
                data: '{"username": "' + username + '",\n' + '"password": "' + password + '"}',
                contentType: "application/json; charset=utf-8",
                complete: function (response) {
                    if (response.status === 200) {
                        top.location.href = '/';
                    } else if (response.status === 404) {
                        alert(response.status + '\nIncorrect username or password');
                        top.location.href = path;
                    } else {
                        alert(response.status + ' Bad Request! Try again');
                    }
                }
            });
        });
    });
</script>
</html>
