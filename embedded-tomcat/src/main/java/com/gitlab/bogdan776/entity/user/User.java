package com.gitlab.bogdan776.entity.user;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class User {

    private UUID id;
    private String username;
    private String email;
    private String password;
    private UserRole role;
    @JsonAlias("create_at")
    private LocalDateTime createAt;
}
