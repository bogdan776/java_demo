package com.gitlab.bogdan776.entity.note;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class Note {

    private UUID id;
    @JsonAlias("author_id")
    private UUID authorId;
    private String title;
    private String text;
    @JsonAlias("create_at")
    private LocalDateTime createAt;
}
