package com.gitlab.bogdan776.exception;

public class JsonException extends AppException {

    public JsonException(String message, Throwable cause) {
        super(message, cause);
    }
}
