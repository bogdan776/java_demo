package com.gitlab.bogdan776;

import com.gitlab.bogdan776.context.AnnotationServerContext;
import com.gitlab.bogdan776.context.ApplicationContext;
import com.gitlab.bogdan776.context.ApplicationProperties;
import com.gitlab.bogdan776.context.AutoScanApplicationContext;
import com.gitlab.bogdan776.context.ErrorPageConfig;
import com.gitlab.bogdan776.context.FilterConfig;
import com.gitlab.bogdan776.context.ServerContext;
import com.gitlab.bogdan776.context.ServletConfig;
import com.gitlab.bogdan776.web.mapper.NoteMapper;
import com.gitlab.bogdan776.web.mapper.UserMapper;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.annotation.WebServlet;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;
import org.mapstruct.factory.Mappers;

import java.io.File;
import java.lang.annotation.Annotation;
import java.util.Map;

public class ServletApplication {

    public static void main(String[] args) throws LifecycleException {
        ApplicationContext applicationContext = AutoScanApplicationContext.scanPackage(
            ServletApplication.class.getPackageName(), () -> Map.of(
                UserMapper.class, Mappers.getMapper(UserMapper.class),
                NoteMapper.class, Mappers.getMapper(NoteMapper.class)
            )
        );
        ServerContext<Annotation> serverContext = AnnotationServerContext.of(
            applicationContext, WebServlet.class, WebFilter.class
        );

        ApplicationProperties properties = ApplicationProperties.loadProperties("application.properties");

        Tomcat tomcat = new Tomcat();

        Connector connector = tomcat.getConnector();
        connector.setPort(Integer.parseInt(properties.getProperty("server.port")));

        Context context = tomcat.addWebapp("/", new File("src/main/webapp/").getAbsolutePath());

        ServletConfig.builder()
            .context(context)
            .source(serverContext)
            .build()
            .addAllAnnotatedServlets();
        FilterConfig.builder()
            .context(context)
            .source(serverContext)
            .build()
            .addAllAnnotatedFilters();
        ErrorPageConfig.forContext(context)
            .addErrorPage(Throwable.class, "/exception_handler");

        tomcat.start();
        tomcat.getService().addConnector(connector);
        tomcat.getServer().await();
    }
}
