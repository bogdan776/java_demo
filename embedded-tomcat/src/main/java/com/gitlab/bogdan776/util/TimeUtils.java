package com.gitlab.bogdan776.util;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;

public final class TimeUtils {

    private TimeUtils() {
    }

    public static Instant instant() {
        return Instant.now(Clock.systemUTC());
    }

    public static LocalDateTime now() {
        return LocalDateTime.now(Clock.systemUTC());
    }
}
