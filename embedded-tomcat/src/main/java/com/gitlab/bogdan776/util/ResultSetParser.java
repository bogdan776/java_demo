package com.gitlab.bogdan776.util;

import lombok.extern.slf4j.Slf4j;
import org.postgresql.util.PGobject;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Parser is based on sql column name.
 */
@Slf4j
public final class ResultSetParser {

    private ResultSetParser() {
    }

    public static <T> T parse(ResultSet resultSet, Class<T> klass) throws SQLException {
        if (resultSet.next()) {
            return parseRow(resultSet, klass, extractColumnNames(resultSet));
        }
        return null;
    }

    public static <T> List<T> parseMultiline(ResultSet resultSet, Class<T> klass) throws SQLException {
        List<T> objects = new ArrayList<>();
        while (resultSet.next()) {
            objects.add(parseRow(resultSet, klass, extractColumnNames(resultSet)));
        }
        return objects;
    }

    private static List<String> extractColumnNames(ResultSet resultSet) throws SQLException {
        List<String> columnNames = new ArrayList<>();
        ResultSetMetaData rsmd = resultSet.getMetaData();
        for (int i = 0; i < rsmd.getColumnCount(); i++) {
            columnNames.add(rsmd.getColumnLabel(i + 1));
        }
        return columnNames;
    }

    private static <T> T parseRow(ResultSet resultSet, Class<T> klass, List<String> columnNames) throws SQLException {
        Map<String, Object> row = new LinkedHashMap<>();
        for (String columnName : columnNames) {
            row.put(columnName, getObject(resultSet, columnName));
        }
        return JsonUtils.fromJson(JsonUtils.toJson(row), klass);
    }

    private static Object getObject(ResultSet resultSet, String columnName) throws SQLException {
        return convertValue(resultSet.getObject(columnName));
    }

    private static Object convertValue(Object value) {
        return convertTimestampToLocalDateTime(getValueFromPGobject(value));
    }

    private static Object getValueFromPGobject(Object value) {
        if (value instanceof PGobject pGobject) {
            return pGobject.getValue();
        }
        return value;
    }

    private static Object convertTimestampToLocalDateTime(Object value) {
        if (value instanceof Timestamp timestamp) {
            return timestamp.toLocalDateTime();
        }
        return value;
    }
}
