package com.gitlab.bogdan776.dao.note;

import com.gitlab.bogdan776.entity.note.Note;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface NoteDao {

    Optional<Note> findById(UUID id);

    Optional<Note> findByIdAndAuthorId(UUID id, UUID authorId);

    List<Note> findAllByAuthor(UUID authorId);

    void save(Note note);

    void update(Note note);

    void delete(Note note);
}
