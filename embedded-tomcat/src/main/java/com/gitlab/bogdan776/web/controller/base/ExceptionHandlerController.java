package com.gitlab.bogdan776.web.controller.base;

import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.response.base.ErrorResponse;
import com.gitlab.bogdan776.web.exception.WebException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet("/exception_handler")
public class ExceptionHandlerController extends BaseController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        handleException(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        handleException(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) {
        handleException(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) {
        handleException(req, resp);
    }

    private void handleException(HttpServletRequest req, HttpServletResponse resp) {
        Throwable throwable = (Throwable) req.getAttribute("jakarta.servlet.error.exception");
        if (throwable == null) {
            log.error("Not platform exception");
            return;
        }
        if (throwable instanceof WebException webException) {
            sendResponse(
                resp, webException.getStatusCode(),
                new ErrorResponse(webException.getStatusCode(), webException.getMessage())
            );
            return;
        }
        sendResponse(resp, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, new ErrorResponse(
            HttpServletResponse.SC_INTERNAL_SERVER_ERROR, throwable.getMessage()
        ));
    }
}
