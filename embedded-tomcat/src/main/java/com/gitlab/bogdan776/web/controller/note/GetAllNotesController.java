package com.gitlab.bogdan776.web.controller.note;

import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.usecase.note.GetAllNotesUseCase;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet("/v1/notes/all")
@RequiredArgsConstructor
public class GetAllNotesController extends BaseController {

    private final GetAllNotesUseCase getAllNotes;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        okResponse(resp, getAllNotes.execute(getUserId(req)));
    }
}
