package com.gitlab.bogdan776.web.usecase.auth;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.user.UserDao;
import com.gitlab.bogdan776.entity.user.User;
import com.gitlab.bogdan776.entity.user.UserRole;
import com.gitlab.bogdan776.security.PasswordEncoder;
import com.gitlab.bogdan776.util.TimeUtils;
import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import com.gitlab.bogdan776.web.dto.request.auth.SignUpRequest;
import com.gitlab.bogdan776.web.exception.WrongRequestException;
import com.gitlab.bogdan776.web.mapper.UserMapper;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultSignUp implements SignUpUseCase {

    private final UserDao userDao;
    private final UserMapper mapper;
    private final PasswordEncoder encoder;

    @Override
    public UserPayload execute(SignUpRequest request) {
        validateRequest(request);
        userDao.findByUsername(request.getUsername()).ifPresent(user -> {
            throw new WrongRequestException("User with username '" + user.getUsername() + "' already exists");
        });
        User user = createUser(request);
        userDao.save(user);
        return mapper.mapToPayload(user);
    }

    private User createUser(SignUpRequest request) {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setPassword(encoder.encode(request.getPassword()));
        user.setRole(UserRole.USER);
        user.setCreateAt(TimeUtils.now());
        return user;
    }

    private void validateRequest(SignUpRequest request) {
        if (request.getUsername() == null || request.getUsername().isBlank()) {
            throw new WrongRequestException("Username cannot be empty");
        }
        if (request.getEmail() == null || request.getEmail().isBlank()) {
            throw new WrongRequestException("Email cannot be empty");
        }
        if ((request.getPassword() == null || request.getPassword().isBlank())
            || (request.getPasswordRepeat() == null || request.getPasswordRepeat().isBlank())) {
            throw new WrongRequestException("Password cannot be empty");
        }
        if (!request.getPassword().equals(request.getPasswordRepeat())) {
            throw new WrongRequestException("Passwords should be equal");
        }
    }
}
