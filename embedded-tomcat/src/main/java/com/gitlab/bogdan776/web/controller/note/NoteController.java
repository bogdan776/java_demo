package com.gitlab.bogdan776.web.controller.note;

import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.request.note.NoteRequest;
import com.gitlab.bogdan776.web.exception.WrongRequestException;
import com.gitlab.bogdan776.web.usecase.note.CreateNoteUseCase;
import com.gitlab.bogdan776.web.usecase.note.DeleteNoteUseCase;
import com.gitlab.bogdan776.web.usecase.note.EditNoteUseCase;
import com.gitlab.bogdan776.web.usecase.note.GetNoteUseCase;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
@WebServlet("/v1/notes")
@RequiredArgsConstructor
public class NoteController extends BaseController {

    private final GetNoteUseCase getNote;
    private final CreateNoteUseCase createNote;
    private final EditNoteUseCase editNote;
    private final DeleteNoteUseCase deleteNote;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        okResponse(resp, getNote.execute(getUserId(req), UUID.fromString(getNoteId(req))));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        okResponse(resp, createNote.execute(
            getUserId(req), getRequestBody(req, NoteRequest.class)
        ));
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) {
        okResponse(resp, editNote.execute(
            getUserId(req), UUID.fromString(getNoteId(req)), getRequestBody(req, NoteRequest.class)
        ));
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) {
        String id = getNoteId(req);
        deleteNote.execute(getUserId(req), UUID.fromString(id));
        log.info("Note {} is deleted", id);
        okResponse(resp);
    }

    private String getNoteId(HttpServletRequest request) {
        String id = request.getParameter("id");
        if (id == null) {
            throw new WrongRequestException("Note id cannot be empty");
        }
        return id;
    }
}
