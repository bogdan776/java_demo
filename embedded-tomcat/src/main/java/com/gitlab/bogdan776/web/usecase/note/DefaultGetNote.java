package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.note.NoteDao;
import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;
import com.gitlab.bogdan776.web.exception.NoteNotFoundException;
import com.gitlab.bogdan776.web.mapper.NoteMapper;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetNote implements GetNoteUseCase {

    private final NoteDao noteDao;
    private final NoteMapper mapper;

    @Override
    public NoteResponse execute(UUID userId, UUID noteId) {
        Note note = noteDao.findByIdAndAuthorId(noteId, userId)
            .orElseThrow(NoteNotFoundException::new);
        return mapper.mapNote(note);
    }
}
