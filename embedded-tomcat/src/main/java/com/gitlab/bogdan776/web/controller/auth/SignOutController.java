package com.gitlab.bogdan776.web.controller.auth;

import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.exception.WrongRequestException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/sign_out")
public class SignOutController extends BaseController {

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) {
        if (getUserPayload(req) == null) {
            throw new WrongRequestException("You are not authenticated");
        }
        req.getSession().invalidate();
    }
}
