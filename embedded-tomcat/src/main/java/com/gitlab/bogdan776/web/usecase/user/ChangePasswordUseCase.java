package com.gitlab.bogdan776.web.usecase.user;

import com.gitlab.bogdan776.web.dto.request.user.ChangePasswordRequest;

import java.util.UUID;

public interface ChangePasswordUseCase {

    void execute(UUID userId, ChangePasswordRequest request);
}
