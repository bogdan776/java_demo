package com.gitlab.bogdan776.web.exception;

import jakarta.servlet.http.HttpServletResponse;

public class WrongRequestException extends WebException {

    public WrongRequestException(String message) {
        super(message, HttpServletResponse.SC_BAD_REQUEST);
    }
}
