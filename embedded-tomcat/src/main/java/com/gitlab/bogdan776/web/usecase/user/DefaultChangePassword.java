package com.gitlab.bogdan776.web.usecase.user;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.user.UserDao;
import com.gitlab.bogdan776.entity.user.User;
import com.gitlab.bogdan776.security.PasswordEncoder;
import com.gitlab.bogdan776.web.dto.request.user.ChangePasswordRequest;
import com.gitlab.bogdan776.web.exception.UserNotFoundException;
import com.gitlab.bogdan776.web.exception.WrongRequestException;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultChangePassword implements ChangePasswordUseCase {

    private final UserDao userDao;
    private final PasswordEncoder encoder;

    @Override
    public void execute(UUID userId, ChangePasswordRequest request) {
        validateRequest(request);
        User user = userDao.findById(userId)
            .orElseThrow(UserNotFoundException::new);
        if (!encoder.matches(request.getPassword(), user.getPassword())) {
            throw new WrongRequestException("Password is not correct");
        }
        user.setPassword(encoder.encode(request.getNewPassword()));
        userDao.update(user);
    }

    private void validateRequest(ChangePasswordRequest request) {
        if ((request.getPassword() == null || request.getPassword().isBlank())
            || (request.getPasswordRepeat() == null || request.getPasswordRepeat().isBlank())
            || (request.getNewPassword() == null || request.getNewPassword().isBlank())) {
            throw new WrongRequestException("Password cannot be empty");
        }
        if (!request.getNewPassword().equals(request.getPasswordRepeat())) {
            throw new WrongRequestException("Passwords should be equal");
        }
    }
}
