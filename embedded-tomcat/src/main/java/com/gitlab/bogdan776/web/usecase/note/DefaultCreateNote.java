package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.note.NoteDao;
import com.gitlab.bogdan776.dao.user.UserDao;
import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.entity.user.User;
import com.gitlab.bogdan776.util.TimeUtils;
import com.gitlab.bogdan776.web.dto.request.note.NoteRequest;
import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;
import com.gitlab.bogdan776.web.exception.UserNotFoundException;
import com.gitlab.bogdan776.web.exception.WrongRequestException;
import com.gitlab.bogdan776.web.mapper.NoteMapper;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultCreateNote implements CreateNoteUseCase {

    private final UserDao userDao;
    private final NoteDao noteDao;
    private final NoteMapper mapper;

    @Override
    public NoteResponse execute(UUID userId, NoteRequest request) {
        User user = userDao.findById(userId)
            .orElseThrow(UserNotFoundException::new);
        validateRequest(request);
        Note note = createNote(user, request);
        noteDao.save(note);
        return mapper.mapNote(note);
    }

    private Note createNote(User author, NoteRequest request) {
        Note note = new Note();
        note.setId(UUID.randomUUID());
        note.setAuthorId(author.getId());
        note.setTitle(request.getTitle());
        note.setText(request.getText());
        note.setCreateAt(TimeUtils.now());
        return note;
    }

    private void validateRequest(NoteRequest request) {
        if (request.getTitle() == null || request.getTitle().isBlank()) {
            throw new WrongRequestException("Title cannot be empty");
        }
        if (request.getText() == null || request.getText().isBlank()) {
            throw new WrongRequestException("Text cannot be empty");
        }
    }
}
