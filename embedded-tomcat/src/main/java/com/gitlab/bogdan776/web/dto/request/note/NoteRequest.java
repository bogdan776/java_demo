package com.gitlab.bogdan776.web.dto.request.note;

import lombok.Data;

@Data
public class NoteRequest {

    private String title;
    private String text;
}
