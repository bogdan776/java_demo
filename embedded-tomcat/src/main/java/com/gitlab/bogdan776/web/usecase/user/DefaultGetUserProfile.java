package com.gitlab.bogdan776.web.usecase.user;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.user.UserDao;
import com.gitlab.bogdan776.entity.user.User;
import com.gitlab.bogdan776.web.dto.response.user.UserResponse;
import com.gitlab.bogdan776.web.exception.UserNotFoundException;
import com.gitlab.bogdan776.web.mapper.UserMapper;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetUserProfile implements GetUserProfileUseCase {

    private final UserDao userDao;
    private final UserMapper mapper;

    @Override
    public UserResponse execute(UUID userId) {
        User user = userDao.findById(userId)
            .orElseThrow(UserNotFoundException::new);
        return mapper.mapUser(user);
    }
}
