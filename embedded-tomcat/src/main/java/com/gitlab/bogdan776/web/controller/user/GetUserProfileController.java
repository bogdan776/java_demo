package com.gitlab.bogdan776.web.controller.user;

import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.usecase.user.GetUserProfileUseCase;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

@WebServlet("/")
@RequiredArgsConstructor
public class GetUserProfileController extends BaseController implements UserController {

    private final GetUserProfileUseCase getUserProfile;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        okResponse(resp, getUserProfile.execute(getUserId(req)));
    }
}
