package com.gitlab.bogdan776.web.controller.auth;

import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import com.gitlab.bogdan776.web.dto.request.auth.SignUpRequest;
import com.gitlab.bogdan776.web.usecase.auth.SignUpUseCase;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet("/sign_up")
@RequiredArgsConstructor
public class SignUpController extends BaseController {

    private final SignUpUseCase signUp;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        UserPayload payload = signUp.execute(getRequestBody(req, SignUpRequest.class));
        setAuthorization(req, payload);
        log.info("User created {}", payload);
        okResponse(resp, payload);
    }
}
