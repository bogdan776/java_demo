package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;

import java.util.UUID;

public interface GetNoteUseCase {

    NoteResponse execute(UUID userId, UUID noteId);
}
