package com.gitlab.bogdan776.web.usecase.user;

import com.gitlab.bogdan776.web.dto.response.user.UserResponse;

import java.util.UUID;

public interface GetUserProfileUseCase {

    UserResponse execute(UUID userId);
}
