package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.note.NoteDao;
import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.web.dto.request.note.NoteRequest;
import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;
import com.gitlab.bogdan776.web.exception.NoteNotFoundException;
import com.gitlab.bogdan776.web.mapper.NoteMapper;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultEditNote implements EditNoteUseCase {

    private final NoteDao noteDao;
    private final NoteMapper mapper;

    @Override
    public NoteResponse execute(UUID userId, UUID noteId, NoteRequest request) {
        Note note = noteDao.findByIdAndAuthorId(noteId, userId)
            .orElseThrow(NoteNotFoundException::new);
        note.setTitle(request.getTitle() == null ? note.getTitle() : request.getTitle());
        note.setText(request.getText() == null ? note.getText() : request.getText());
        noteDao.update(note);
        return mapper.mapNote(note);
    }
}
