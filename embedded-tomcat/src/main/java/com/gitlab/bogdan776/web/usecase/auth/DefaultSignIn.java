package com.gitlab.bogdan776.web.usecase.auth;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.user.UserDao;
import com.gitlab.bogdan776.entity.user.User;
import com.gitlab.bogdan776.security.PasswordEncoder;
import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import com.gitlab.bogdan776.web.dto.request.auth.SignInRequest;
import com.gitlab.bogdan776.web.exception.UserNotFoundException;
import com.gitlab.bogdan776.web.exception.WrongRequestException;
import com.gitlab.bogdan776.web.mapper.UserMapper;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class DefaultSignIn implements SignInUseCase {

    private final UserDao userDao;
    private final UserMapper mapper;
    private final PasswordEncoder encoder;

    @Override
    public UserPayload execute(SignInRequest request) {
        User user = userDao.findByUsername(request.getUsername())
            .orElseThrow(UserNotFoundException::new);
        if (!encoder.matches(request.getPassword(), user.getPassword())) {
            throw new WrongRequestException("Wrong password");
        }
        return mapper.mapToPayload(user);
    }
}
