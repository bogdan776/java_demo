package com.gitlab.bogdan776.web.mapper;

import com.gitlab.bogdan776.entity.user.User;
import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import com.gitlab.bogdan776.web.dto.response.user.UserResponse;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {

    UserPayload mapToPayload(User user);

    UserResponse mapUser(User user);
}
