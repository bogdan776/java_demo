package com.gitlab.bogdan776.web.dto.response.user;

import com.gitlab.bogdan776.entity.user.UserRole;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class UserResponse {

    private UUID id;
    private String username;
    private String email;
    private UserRole role;
    private LocalDateTime createAt;
}
