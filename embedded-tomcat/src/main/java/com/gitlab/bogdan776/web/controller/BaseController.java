package com.gitlab.bogdan776.web.controller;

import com.gitlab.bogdan776.exception.JsonException;
import com.gitlab.bogdan776.util.JsonUtils;
import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import com.gitlab.bogdan776.web.exception.WrongRequestException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Slf4j
public abstract class BaseController extends HttpServlet {

    protected <T> T getRequestBody(HttpServletRequest request, Class<T> klass) {
        try (BufferedInputStream inputStream = new BufferedInputStream(request.getInputStream())) {
            String json = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
            return JsonUtils.fromJson(json, klass);
        } catch (IOException | JsonException e) {
            log.error(e.getMessage(), e);
            throw new WrongRequestException("Cannot parse request body");
        }
    }

    protected void setAuthorization(HttpServletRequest request, UserPayload payload) {
        request.getSession().setAttribute("Authorization", payload);
    }

    protected UserPayload getUserPayload(HttpServletRequest request) {
        return (UserPayload) request.getSession().getAttribute("Authorization");
    }

    protected UUID getUserId(HttpServletRequest request) {
        return getUserPayload(request).getId();
    }

    protected void okResponse(HttpServletResponse response) {
        sendResponse(response, HttpServletResponse.SC_OK);
    }

    protected <T> void okResponse(HttpServletResponse response, T body) {
        sendResponse(response, HttpServletResponse.SC_OK, body);
    }

    protected void sendResponse(HttpServletResponse response, int status) {
        sendResponse(response, status, null);
    }

    protected <T> void sendResponse(HttpServletResponse response, int status, T body) {
        response.setContentType("application/json");
        response.setStatus(status);
        if (body == null) {
            return;
        }
        try (ServletOutputStream outputStream = response.getOutputStream()) {
            outputStream.write(JsonUtils.toJson(body).getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new JsonException("Cannot write response body", e);
        }
    }
}
