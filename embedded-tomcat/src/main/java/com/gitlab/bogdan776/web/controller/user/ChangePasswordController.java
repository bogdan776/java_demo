package com.gitlab.bogdan776.web.controller.user;

import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.request.user.ChangePasswordRequest;
import com.gitlab.bogdan776.web.usecase.user.ChangePasswordUseCase;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

@WebServlet("/change_password")
@RequiredArgsConstructor
public class ChangePasswordController extends BaseController implements UserController {

    private final ChangePasswordUseCase changePassword;

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) {
        changePassword.execute(getUserId(req), getRequestBody(req, ChangePasswordRequest.class));
        okResponse(resp);
    }
}
