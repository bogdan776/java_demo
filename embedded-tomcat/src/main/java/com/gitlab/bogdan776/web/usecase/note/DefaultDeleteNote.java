package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.note.NoteDao;
import com.gitlab.bogdan776.entity.note.Note;
import com.gitlab.bogdan776.web.exception.NoteNotFoundException;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultDeleteNote implements DeleteNoteUseCase {

    private final NoteDao noteDao;

    @Override
    public void execute(UUID userId, UUID noteId) {
        Note note = noteDao.findByIdAndAuthorId(noteId, userId)
            .orElseThrow(NoteNotFoundException::new);
        noteDao.delete(note);
    }
}
