package com.gitlab.bogdan776.web.usecase.note;

import com.gitlab.bogdan776.context.Component;
import com.gitlab.bogdan776.dao.note.NoteDao;
import com.gitlab.bogdan776.web.dto.response.note.NoteResponse;
import com.gitlab.bogdan776.web.mapper.NoteMapper;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DefaultGetAllNotes implements GetAllNotesUseCase {

    private final NoteDao noteDao;
    private final NoteMapper mapper;

    @Override
    public List<NoteResponse> execute(UUID userId) {
        return mapper.mapNotes(noteDao.findAllByAuthor(userId));
    }
}
