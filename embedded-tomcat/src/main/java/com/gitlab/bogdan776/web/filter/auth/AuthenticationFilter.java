package com.gitlab.bogdan776.web.filter.auth;

import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.response.base.ErrorResponse;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebFilter(filterName = "AuthenticationFilter", urlPatterns = "/v1/*")
public class AuthenticationFilter extends BaseController implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (getUserPayload((HttpServletRequest) request) == null) {
            sendResponse(
                (HttpServletResponse) response,
                HttpServletResponse.SC_FORBIDDEN,
                new ErrorResponse(HttpServletResponse.SC_FORBIDDEN, "You are not authenticated to view this page")
            );
        } else {
            chain.doFilter(request, response);
        }
    }
}
