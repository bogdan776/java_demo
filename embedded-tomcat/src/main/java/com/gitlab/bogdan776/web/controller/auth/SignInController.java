package com.gitlab.bogdan776.web.controller.auth;

import com.gitlab.bogdan776.web.controller.BaseController;
import com.gitlab.bogdan776.web.dto.payload.UserPayload;
import com.gitlab.bogdan776.web.dto.request.auth.SignInRequest;
import com.gitlab.bogdan776.web.usecase.auth.SignInUseCase;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

import java.io.IOException;

@WebServlet("/sign_in")
@RequiredArgsConstructor
public class SignInController extends BaseController {

    private final SignInUseCase signIn;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        UserPayload payload = signIn.execute(getRequestBody(req, SignInRequest.class));
        setAuthorization(req, payload);
        okResponse(resp, payload);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/jsp/sign_in.jsp").forward(req, resp);
    }
}
