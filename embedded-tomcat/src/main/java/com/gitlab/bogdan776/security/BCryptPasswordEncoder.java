package com.gitlab.bogdan776.security;

import com.gitlab.bogdan776.context.Component;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.regex.Pattern;

@Component
public class BCryptPasswordEncoder implements PasswordEncoder {

    private static final Pattern BCRYPT_PATTERN = Pattern.compile("\\A\\$2(a|y|b)?\\$(\\d\\d)\\$[./0-9A-Za-z]{53}");
    private static final int STRENGTH = 10;

    private final BCryptVersion version;

    public BCryptPasswordEncoder() {
        this(BCryptVersion.$2A);
    }

    public BCryptPasswordEncoder(BCryptVersion version) {
        this.version = version;
    }

    @Override
    public String encode(String rawPassword) {
        if (rawPassword == null) {
            throw new IllegalArgumentException("rawPassword cannot be null");
        }
        return BCrypt.hashPassword(rawPassword, getSalt());
    }

    @Override
    public boolean matches(String rawPassword, String encodedPassword) {
        if (rawPassword == null) {
            throw new IllegalArgumentException("rawPassword cannot be null");
        }
        if (encodedPassword == null || encodedPassword.length() == 0) {
            return false;
        }
        if (!BCRYPT_PATTERN.matcher(encodedPassword).matches()) {
            return false;
        }
        return BCrypt.checkPassword(rawPassword, encodedPassword);
    }

    private String getSalt() {
        return BCrypt.genSalt(version.getVersion(), STRENGTH);
    }

    @AllArgsConstructor
    public enum BCryptVersion {

        $2A("$2a"),
        $2Y("$2y"),
        $2B("$2b");

        @Getter
        private final String version;
    }
}
