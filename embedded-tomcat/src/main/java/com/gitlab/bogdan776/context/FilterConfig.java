package com.gitlab.bogdan776.context;

import com.gitlab.bogdan776.exception.ContextException;
import jakarta.servlet.DispatcherType;
import jakarta.servlet.Filter;
import jakarta.servlet.annotation.WebFilter;
import org.apache.catalina.Context;
import org.apache.tomcat.util.descriptor.web.FilterDef;
import org.apache.tomcat.util.descriptor.web.FilterMap;

import java.lang.annotation.Annotation;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

public class FilterConfig {

    private final Context context;
    private final ServerContext<Annotation> serverContext;

    public FilterConfig(Context context, ServerContext<Annotation> serverContext) {
        this.context = Objects.requireNonNull(context);
        this.serverContext = serverContext;
    }

    public static Builder builder() {
        return new Builder();
    }

    public void addAllAnnotatedFilters() {
        if (serverContext == null) {
            throw new ContextException("ServerContext cannot be null");
        }
        serverContext.getComponents(WebFilter.class)
            .forEach(this::addAnnotatedFilter);
    }

    public void addAllFilters(Supplier<Set<Object>> source) {
        source.get().forEach(this::addAnnotatedFilter);
    }

    public void addAnnotatedFilter(Object webFilter) {
        if (!(webFilter instanceof Filter filter)) {
            throw new ContextException("Type not supported");
        }
        WebFilter annotation = webFilter.getClass().getDeclaredAnnotation(WebFilter.class);
        validate(annotation);
        addFilter(filter, annotation.filterName(), annotation.urlPatterns(), annotation.dispatcherTypes());
    }

    public void addFilter(Filter filter, String filterName, String[] urlPatterns, DispatcherType[] dispatcherTypes) {
        FilterDef filterDefinition = new FilterDef();
        filterDefinition.setFilter(filter);
        filterDefinition.setFilterName(filterName);
        context.addFilterDef(filterDefinition);

        FilterMap filterMap = new FilterMap();
        filterMap.setFilterName(filterName);
        for (String url : urlPatterns) {
            filterMap.addURLPattern(url);
        }
        for (DispatcherType dispatcherType : dispatcherTypes) {
            filterMap.setDispatcher(dispatcherType.name());
        }
        context.addFilterMap(filterMap);
    }

    private void validate(WebFilter webFilter) {
        if (webFilter.filterName().isBlank()) {
            throw new ContextException("filterName cannot be empty");
        }
    }

    public static final class Builder {

        private Context context;
        private ServerContext<Annotation> serverContext;

        public Builder context(Context context) {
            this.context = context;
            return this;
        }

        public Builder source(ServerContext<Annotation> serverContext) {
            this.serverContext = serverContext;
            return this;
        }

        public FilterConfig build() {
            return new FilterConfig(context, serverContext);
        }
    }
}
