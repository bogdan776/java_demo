package com.gitlab.bogdan776.context;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

interface ContextFactory {

    void addBeforeInitialization(Supplier<Map<Class<?>, Object>> objects);

    void initialize(List<Class<?>> classes);

    Context getContext();
}
