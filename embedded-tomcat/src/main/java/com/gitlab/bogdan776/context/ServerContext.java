package com.gitlab.bogdan776.context;

import java.util.Set;

public interface ServerContext<T> {

    Set<Object> getComponents(Class<? extends T> type);
}
