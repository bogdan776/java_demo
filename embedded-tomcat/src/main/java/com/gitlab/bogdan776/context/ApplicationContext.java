package com.gitlab.bogdan776.context;

import java.util.Set;

public interface ApplicationContext {

    <T> T getInstance(Class<T> type);

    Set<Class<?>> types();
}
