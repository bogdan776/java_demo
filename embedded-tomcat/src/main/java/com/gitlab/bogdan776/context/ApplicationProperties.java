package com.gitlab.bogdan776.context;

import com.gitlab.bogdan776.exception.ContextException;
import com.gitlab.bogdan776.util.ClassLoaderUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public final class ApplicationProperties {

    private final Properties properties;

    private ApplicationProperties(Properties properties) {
        this.properties = properties;
    }

    public static ApplicationProperties loadProperties(String propertiesName) {
        Properties properties = new Properties();
        try (FileReader reader = new FileReader(getPath(propertiesName))) {
            properties.load(reader);
            return new ApplicationProperties(properties);
        } catch (IOException e) {
            throw new ContextException("Cannot load properties", e);
        }
    }

    private static String getPath(String name) {
        return Objects.requireNonNull(
            ClassLoaderUtils.threadContextClassLoader()
                .getResource(name)
        ).getPath().replace("%20", " ");
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public String getProperty(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }
}
