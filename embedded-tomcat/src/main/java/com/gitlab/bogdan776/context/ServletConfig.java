package com.gitlab.bogdan776.context;

import com.gitlab.bogdan776.exception.ContextException;
import jakarta.servlet.Servlet;
import jakarta.servlet.annotation.WebServlet;
import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ServletConfig {

    private final Context context;
    private final ServerContext<Annotation> serverContext;

    public ServletConfig(Context context, ServerContext<Annotation> serverContext) {
        this.context = Objects.requireNonNull(context);
        this.serverContext = serverContext;
    }

    public static Builder builder() {
        return new Builder();
    }

    public void addAllAnnotatedServlets() {
        if (serverContext == null) {
            throw new ContextException("ServerContext cannot be null");
        }
        serverContext.getComponents(WebServlet.class)
            .forEach(this::addAnnotatedServlet);
    }

    public void addAllFilters(Supplier<Set<Object>> source) {
        source.get().forEach(this::addAnnotatedServlet);
    }

    public void addAnnotatedServlet(Object httpServlet) {
        if (!(httpServlet instanceof Servlet servlet)) {
            throw new ContextException("Type not supported");
        }
        Class<?> servletClass = httpServlet.getClass();
        WebServlet annotation = servletClass.getDeclaredAnnotation(WebServlet.class);
        validate(annotation);

        String name = servletClass.getSimpleName();
        String inheritedPath = getInheritedPath(servlet);
        String fullPath = inheritedPath.isBlank()
            ? annotation.value()[0]
            : inheritedPath + addSlashToBeginning(removeTrailingSlash(annotation.value()[0]));
        addServlet(name, fullPath, servlet);
    }

    public void addServlet(String name, String path, Servlet servlet) {
        Tomcat.addServlet(context, name, servlet);
        context.addServletMappingDecoded(path, name);
    }

    private void validate(WebServlet webServlet) {
        if (webServlet.value().length == 0) {
            throw new IllegalArgumentException("WebServlet::value cannot be empty");
        }
    }

    private String getInheritedPath(Object controller) {
        return concatPaths(getPaths(controller));
    }

    private List<String> getPaths(Object controller) {
        return Arrays.stream(controller.getClass().getInterfaces())
            .map(klass -> klass.getDeclaredAnnotation(WebServlet.class))
            .map(webServlet -> webServlet.value()[0])
            .collect(Collectors.toCollection(LinkedList::new));
    }

    private String concatPaths(List<String> paths) {
        StringBuilder builder = new StringBuilder();
        paths.forEach(value -> builder.append(addSlashToBeginning(removeTrailingSlash(value))));
        return builder.toString();
    }

    private String addSlashToBeginning(String value) {
        if (!value.startsWith("/")) {
            return "/" + value;
        }
        return value;
    }

    private String removeTrailingSlash(String value) {
        if (value.endsWith("/")) {
            return value.substring(0, value.length() - 1);
        }
        return value;
    }

    public static final class Builder {

        private Context context;
        private ServerContext<Annotation> serverContext;

        public Builder context(Context context) {
            this.context = context;
            return this;
        }

        public Builder source(ServerContext<Annotation> serverContext) {
            this.serverContext = serverContext;
            return this;
        }

        public ServletConfig build() {
            return new ServletConfig(context, serverContext);
        }
    }
}
