package com.gitlab.bogdan776.context;

import lombok.RequiredArgsConstructor;
import org.apache.catalina.Context;
import org.apache.tomcat.util.descriptor.web.ErrorPage;

@RequiredArgsConstructor
public class ErrorPageConfig {

    private final Context context;

    public static ErrorPageConfig forContext(Context context) {
        return new ErrorPageConfig(context);
    }

    public ErrorPageConfig addErrorPage(Class<? extends Throwable> exceptionType, String location) {
        ErrorPage errorPage = new ErrorPage();
        errorPage.setExceptionType(exceptionType.getName());
        errorPage.setLocation(location);
        context.addErrorPage(errorPage);
        return this;
    }
}
