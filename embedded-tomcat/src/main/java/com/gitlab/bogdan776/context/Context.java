package com.gitlab.bogdan776.context;

import java.util.List;
import java.util.Set;

interface Context {

    <T> T get(Class<T> type);

    List<Object> getAllByTypes(Class<?>... types);

    void add(Class<?> type, Object value);

    void add(Object value, Class<?>... types);

    boolean contains(Class<?> type);

    boolean contains(Class<?>... types);

    Set<Class<?>> getAllTypes();
}
