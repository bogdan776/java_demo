package com.gitlab.bogdan776.context;

import lombok.Getter;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class AnnotationServerContext implements ServerContext<Annotation> {

    @Getter
    private final Map<Class<? extends Annotation>, Set<Object>> annotationContext = new HashMap<>();

    private final Set<Class<? extends Annotation>> required = new HashSet<>();
    private final boolean saveAll;

    private AnnotationServerContext(ApplicationContext context, Class<? extends Annotation>[] annotations) {
        if (annotations.length == 0) {
            saveAll = true;
        } else {
            required.addAll(Set.of(annotations));
            saveAll = false;
        }
        load(context);
    }

    @SafeVarargs
    public static ServerContext<Annotation> of(ApplicationContext context, Class<? extends Annotation>... annotations) {
        return new AnnotationServerContext(context, annotations);
    }

    @Override
    public Set<Object> getComponents(Class<? extends Annotation> type) {
        return annotationContext.get(type);
    }

    private void load(ApplicationContext context) {
        for (Class<?> value : context.types()) {
            Annotation[] annotations = value.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                Class<? extends Annotation> annotationType = annotation.annotationType();
                if (!saveAll && !required.contains(annotationType)) {
                    continue;
                }
                if (annotationContext.containsKey(annotationType)) {
                    annotationContext.get(annotationType)
                        .add(context.getInstance(value));
                } else {
                    annotationContext.put(
                        annotationType, new HashSet<>(Collections.singleton(context.getInstance(value))));
                }
            }
        }
    }
}
