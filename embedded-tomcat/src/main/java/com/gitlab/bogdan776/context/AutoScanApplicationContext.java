package com.gitlab.bogdan776.context;

import com.gitlab.bogdan776.util.ClassLoaderUtils;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.annotation.WebServlet;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

public final class AutoScanApplicationContext implements ApplicationContext {

    private final Context context;

    private AutoScanApplicationContext(Context context) {
        this.context = context;
    }

    public static ApplicationContext scanPackage(String packageName, Supplier<Map<Class<?>, Object>> objects) {
        ContextFactory contextFactory = new DefaultContextFactory(new DefaultContext());
        contextFactory.addBeforeInitialization(objects);

        List<Class<?>> classes = ClassLoaderUtils.loadClassesByPackageAndAnnotations(
            packageName, Set.of(Component.class, WebServlet.class, WebFilter.class), true
        );
        contextFactory.initialize(classes);
        return new AutoScanApplicationContext(contextFactory.getContext());
    }

    @Override
    public <T> T getInstance(Class<T> type) {
        return context.get(type);
    }

    @Override
    public Set<Class<?>> types() {
        return context.getAllTypes();
    }
}
