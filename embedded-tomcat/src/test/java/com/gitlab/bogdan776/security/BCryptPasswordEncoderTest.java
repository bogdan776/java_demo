package com.gitlab.bogdan776.security;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class BCryptPasswordEncoderTest {

    private final PasswordEncoder encoder = new BCryptPasswordEncoder();

    @Test
    void shouldBeTrue_whenPasswordMatchesWithEncoded() {
        String encoded = encoder.encode("qwerty123");
        Assertions.assertThat(encoder.matches("qwerty123", encoded)).isTrue();
    }

    @Test
    void shouldBeFalse_whenPasswordDoesNotMatchWithEncoded() {
        String encoded = encoder.encode("Qwerty123");
        Assertions.assertThat(encoder.matches("qwerty123", encoded)).isFalse();
    }

    @Test
    void shouldBeTrue_whenPasswordMatchesWithEncodedUseConstant() {
        String encoded = "$2a$10$QLvMuD9nxzVRopLhjT7NbuG.OpI0gz6Oz22s.8i8fvVauzNZzii8u";
        Assertions.assertThat(encoder.matches("qwerty123", encoded)).isTrue();
    }
}