<html>
<head>
    <title>Sign in</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
<form>
    <p>
        Login
        <label for="login">
            <input name="login" id="login" placeholder="Login"/>
        </label>
    </p>
    <p>
        Password
        <label for="password">
            <input name="password" id="password" type="password" placeholder="Password"/>
        </label>

    </p>
    <input type="submit" id="button-login" value="Sign in">
</form>
</body>
<script>
    $(document).ready(function () {
        $('#button-login').click(function (event) {
            event.preventDefault();
            const login = $('#login').val();
            const password = $('#password').val();
            const path = '${pageContext.servletContext.contextPath}/sign_in';
            $.ajax({
                url: path,
                method: 'POST',
                data: '{"login": "' + login + '",\n' + '"password": "' + password + '"}',
                contentType: "application/json; charset=utf-8",
                complete: function (response) {
                    if (response.status === 200) {
                        top.location.href = '/';
                    } else if (response.status === 404) {
                        alert(response.status + '\nIncorrect login or password');
                        top.location.href = path;
                    } else {
                        alert(response.status + ' Bad Request! Try again');
                    }
                }
            });
        });
    });
</script>
</html>
