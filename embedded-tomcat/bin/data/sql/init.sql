create table if not exists demo_user
(
    id         uuid      not null,
    username   varchar   not null,
    email      varchar   not null,
    "password" varchar   not null,
    "role"     varchar   not null,
    create_at  timestamp not null,
    constraint demo_user_pk primary key (id),
    constraint demo_user_uq_username unique (username)
);

create table if not exists demo_note
(
    id        uuid      not null,
    author_id uuid      not null,
    title     varchar   not null,
    "text"    varchar   not null,
    create_at timestamp not null,
    constraint demo_note_pk primary key (id),
    constraint demo_note_fk_author foreign key (author_id) references demo_user
);
